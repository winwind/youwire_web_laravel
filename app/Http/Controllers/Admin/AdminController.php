<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\config;

class AdminController extends Controller
{
    public function index()
    {
      $confObj = config::all();
      $confArray = array();
      if(!empty($confObj))
      {
        foreach ($confObj as $conf)
        {
          $confArray[$conf->conf_name] = $conf->conf_value;
        }
      }
      return view('admin/admin')->with('configs',$confArray);
    }

    public function control_update(Request $request)
    {
      $items = $request->all();

      foreach ($items as $key => $value) {
        if($key != "_token")
        {
          //<intput> tag name = configs table conf_name
          //get the record that matches the <input> tag name and update
          if(is_numeric($value))
          {
            $sql = "update configs set conf_value=".$value." where conf_name = '".$key."';";
          }
          else
          {
            $sql = "update configs set conf_value='".$value."' where conf_name = '".$key."';";
          }
          DB::statement($sql);
        }
      }
      return redirect()->back();
    }
}
