<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\alertmail_setting;

class AlertmailController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth')->except('index');
  }

  public function alertView()
  {
    $security_group = DB::table('security_groups')
                        ->leftjoin('profile_datas','security_groups.security_group_id', '=', 'profile_datas.security_group_id')
                        ->where('profile_datas.uid','=',auth()->user()->id)
                        ->select('security_groups.*')
                        ->get();
    foreach ($security_group as $sgroup) {
      $data['authority_bulk_download'] = $sgroup->authority_bulk_download;
      $data['authority_delete'] = $sgroup->authority_delete;
      $data['authority_download'] = $sgroup->authority_download;
      $data['authority_protect'] = $sgroup->authority_protect;
      $data['authority_add_tag'] = $sgroup->authority_add_tag;
      $data['authority_edit_tag_category'] = $sgroup->authority_edit_tag_category;
      $data['authority_edit_security_group'] = $sgroup->authority_edit_security_group;
      $data['authority_edit_group'] = $sgroup->authority_edit_group;
      $data['authority_edit_user'] = $sgroup->authority_edit_user;
      $data['authority_edit_alertmail'] = $sgroup->authority_edit_alertmail;
    }
    $ip[0] = $_SERVER['REMOTE_ADDR'];
    $ip[1] = getenv('SERVER_ADDR');

    if($data['authority_edit_alertmail'] == 0) //return back when not authorized
    {
      Session::put('err',"User is not authorized for this operation");
      return redirect()->back();
    }

    $target_id = 0;
    // 初期値
		$flag = 0;
		$limit = 80;
		$mail = '';
		$day = 1;
		$time = 10;

    $modObj = alertmail_setting::where('id','=',$target_id)->orderBy('category_id', 'asc')->orderBy('sub_category_id', 'asc')->get();
    if (!empty($modObj)) {
				foreach ($modObj as $obj) {
					switch ($obj->category_id) {
						case 1:   if ($obj->value)
                      {
								         $flag = 1;
								         $limit = $obj->value;
							        }
							        break;
						case 2:   $mail .= $obj->value . ',';
							        break;
						case 3:		$day = $obj->value;
							        break;
						case 4:		$time = $obj->value;
							        break;
					}
				}
				$mail = substr($mail, 0, -1);
			}
      $request = new \Illuminate\Http\Request();
      $request->alertmail_send = $flag;
      $request->alertmail_send_limit = $limit;
      $request->alertmail_send_mail = $mail;
      $request->alertmail_send_day = $day;
      $request->alertmail_send_time = $time;
      return view('alertmail')->with('old_req',$request)->with('ips',$ip)->with('data',$data);
  }

  public function control_update(Request $request)
  {
    $target_id = 0;
    $flag = $request->input('alertmail_send');

    if ( !is_null($flag) )
    {
      $error_flag = 0;

			if ($flag) {
				// 設定値を取得
				$limit 		= $request->input('alertmail_send_limit');
				$mail 		= $request->input('alertmail_send_mail');
				$mail 		= str_replace(array("\r\n","\n","\r"),"",$mail);
				$mail_array 	= explode(',', $mail);
				$day 			= $request->input('alertmail_send_day');
				$time 		= $request->input('alertmail_send_time');

				if ( $mail == '' ) {
					// メールアドレスが入力されていない場合にはエラー
					$error_flag = 1;
					$err = "Email is required";
          Session::put('err',$err);
				} else {
					foreach ($mail_array as $mail_one) {
						if ( !preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i', $mail_one) ){
							$error_flag = 1;
							$err = "There is an invalid email address";
              Session::put('err',$err);
						}
					}
				}
			}
      else{
        $error_flag = 1;
        $res = alertmail_setting::where('id','=',$target_id)->delete();
        $err = "Alert mail setting is OFF";
        Session::put('err',$err);
      }

      if ( !$error_flag ) {
        $status = 1; //check any error when inserting to DB

        // 一旦、設定データを削除
        $res = alertmail_setting::where('id','=',$target_id)->delete();

				if ( $flag ) {

          $alertSetting = new alertmail_setting;
					// 容量制限値
          $alertSetting->id = 0;
          $alertSetting->category_id = 1;
          $alertSetting->sub_category_id = 0;
          $alertSetting->value = $limit;
          $res = $alertSetting->save();
          if(!$res)$status = 0;
    			// 送信メールアドレス
					$i = 1;
					foreach ($mail_array as $mail_one) {
						if ( $mail_one != '' ) {
              $alertSetting = new alertmail_setting;
              $alertSetting->id = 0;
              $alertSetting->category_id = 2;
              $alertSetting->sub_category_id = $i;
              $alertSetting->value = $mail_one;
              $res = $alertSetting->save();
              if(!$res)$status = 0;
							$i++;
						}
					}

					// 送信曜日
          $alertSetting = new alertmail_setting;
          $alertSetting->id = 0;
          $alertSetting->category_id = 3;
          $alertSetting->sub_category_id = 0;
          $alertSetting->value = $day;
          $res = $alertSetting->save();
          if(!$res)$status = 0;

					// 送信時間 時
          $alertSetting = new alertmail_setting;
          $alertSetting->id = 0;
          $alertSetting->category_id = 4;
          $alertSetting->sub_category_id = 0;
          $alertSetting->value = $time;
          $res = $alertSetting->save();
          if(!$res)$status = 0;
				}

				if ( $status == 1 ) {
            $msg = "Successfully updated";
            Session::put('msg',$msg);
        }
        else{
            $err = "Sorry!! Update failed";
            Session::put('err',$err);
        }
			}
    }
    //return back()->withErrors(['msg' => 'The Message']);
    return redirect()->back();
  }
}
