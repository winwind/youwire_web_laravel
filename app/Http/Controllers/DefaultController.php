<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Group;
use App\TagCatagory;
use App\topic;
use App\recognition;
use App\youwire_default;
use App\config;

use Illuminate\Pagination\LengthAwarePaginator;

class DefaultController extends Controller
{
    var $mPlayer = 0;
    var $mFilePath = "";
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //configuration details should be loaded from a DB table
        $conf = config::all();
        $configuration = array();
        foreach ($conf as $confrow) {
            $configuration[$confrow->conf_name] = $confrow->conf_value;
        }
        $mPlayer = $configuration['player_select'];; //Load from DB
        $mFilePath = $configuration['file_path'];; //load from DB
        $this->middleware('auth')->except('index');

    }

    public function search(Request $request)
    {
        if(empty(auth()->user()->id))
        {
            return redirect()->route('index');
        }
        //return $request->input('startdatetime');
        $group_ids = '';
        $userID = "";
        $item_ary = array();

        $conf = config::all();
        $configuration = array();
        foreach ($conf as $confrow) {
            $configuration[$confrow->conf_name] = $confrow->conf_value;
        }
        $mPlayer = $configuration['player_select'];; //Load from DB
        $mFilePath = $configuration['file_path'];; //load from DB
        //configuration details should be loaded from a DB table
        $mTag_use = $configuration['quicktag'];  //should be loaded from DB
        $mTag_id;
        if($mTag_use == 1)
        {
            $mTag_id = $configuration['quicktag_id']; //should be loaded from DB
        }

        //Other data array
        $data = array();
        $data['tag_use'] = $mTag_use;
        if($mTag_use)
        {
            $qTagCategory = TagCatagory::find($mTag_id);
            foreach ($qTagCategory as $value) {
                $data['qtag_name'] = $value->name;
            }
        }

        $security_group = DB::table('security_groups')
                            ->leftjoin('profile_datas','security_groups.security_group_id', '=', 'profile_datas.security_group_id')
                            ->where('profile_datas.uid','=',auth()->user()->id)
                            ->select('security_groups.*')
                            ->get();

          foreach ($security_group as $sgroup) {
              $data['authority_bulk_download'] = $sgroup->authority_bulk_download;
              $data['authority_delete'] = $sgroup->authority_delete;
              $data['authority_download'] = $sgroup->authority_download;
              $data['authority_protect'] = $sgroup->authority_protect;
              $data['authority_add_tag'] = $sgroup->authority_add_tag;
              $data['authority_edit_tag_category'] = $sgroup->authority_edit_tag_category;
              $data['authority_edit_security_group'] = $sgroup->authority_edit_security_group;
              $data['authority_edit_group'] = $sgroup->authority_edit_group;
              $data['authority_edit_user'] = $sgroup->authority_edit_user;
              $data['authority_edit_alertmail'] = $sgroup->authority_edit_alertmail;

              if ($sgroup->authority_search <> 1) {
                  $group_ids = '-1';
              } else {
                  $group_id_array = array();
                  if ($sgroup->operating_range == 1 || $sgroup->operating_range == 2 || $sgroup->operating_range == 3){
                      $result_group_id = group_users_link::where('uid','=',auth()->user()->id)->select('group_id')->get();
                      $i = 0;
                      foreach ($result_group_id as $value) {
                          $group_id_array[$i] = $value->group_id;
                          $i++;
                      }

                      //group id search
                      if ($group_id_array[0] == 0) {
                          $group_ids = '0';
                      } else {
                          if ($sgroup->operating_range == 3) {
                              $sql_group_node = Group::select('group_node')->distinct()->whereIn('group_id', $group_id_array)->get();
                              $group_id_array = array();
                              foreach ($sql_group_node as $node) {
                                  $sql_group_id_2 = Group::select('group_id')->distinct()->where('group_node','like',$node->group_node.'%')->get();
                                  $j = 0;
                                  foreach ($sql_group_id_2 as $value) {
                                      $group_id_array[$j] = $value->group_id;
                                      $j++;
                                  }
                              }
                          }

                          if ($group_id_array[0] == 0) {
                              $group_ids = '-1';
                          } else {
                              $group_ids = implode(',', $group_id_array);
                          }
                      }
                      if($sgroup->operating_range == 1)
                      {
                          $userID = auth()->user()->id;
                      }
                  }  //end of group id search

                  $group_list = array();
                  if($group_ids <> ''){
                      $grpHandler = Group::whereIn('id',$group_ids)->orderBy('group_sort', 'asc')->get();
                  }
                  else {
                      $grpHandler = Group::orderBy('group_sort', 'asc')->get();
                  }
                  foreach ($grpHandler as $value) {
                      $id = $value->id;
                      foreach ($value->toArray() as $key2 => $v) {
                        if ($key2 == "group_name") {
                            $group_class = substr_count($value->group_node,'.') - 1;
                            $v = str_repeat(">", $group_class - 1) . $v;
                        }
                        $group_list[$id][$key2] = $v;
                      }
                  }

                  if (!is_null($request->input('selected_group')) && $request->input('selected_group') >= 0) {
                      $group_ids = $request->input('selected_group');
                  }
                }// end of old youwire => getGroupIds()
          }//end for foreach of groupIDS

          if(empty($request->input('use_search_sql')))//check whether request is from detailView => if yes then load data useing search_sql
          {
            $sql = "select d.* from defaults as d";
            if($request->input('selected_tag') != 0)
            {
                $sql .= " inner join tags as t on t.rec_id = d.id where t.tag_category_id = ".$request->input('selected_tag');
                $sql .= " and d.uid = " .auth()->user()->id;
            }
            else
            {
                $sql .= " where d.uid = " .auth()->user()->id;
            }
            if($group_ids <> "")
            {
                $sql .= " and d.group_id IN (". $group_ids .")";
            }
            if($request->input('startdatetime') != "")
            {
                $dateandtime = explode(" ",$request->input('startdatetime'));
                if($dateandtime[1] === "00:00")
                {
                  $sql .= " and d.timestamp >= '". $dateandtime[0] ." 00:00:00'";
                }
                else
                {
                  $sql .= " and d.timestamp >= '". $request->input('startdatetime') .":00'";
                }
            }
            if($request->input('enddatetime') != "")
            {
                $dateandtime = explode(" ",$request->input('enddatetime'));
                if($dateandtime[1] === "00:00")
                {
                  $sql .= " and d.timestamp <= '". $dateandtime[0] ." 23:59:59'";
                }
                else {
                  $sql .= " and d.timestamp <= '". $request->input('enddatetime') .":00'";
                }
            }
            if($request->input('startduration') != "")
            {
                $explode_startduration = explode(":", $request->input('startduration'));
                $sec_startduration = intval($explode_startduration[0]) * 60 * 60 + intval($explode_startduration[1]) * 60;
                if(count($explode_startduration) == 3)
                {
                    $sec_startduration = $sec_startduration + intval($explode_startduration[2]);
                }
                if($sec_startduration > 0)
                {
                    $sql .= ' and d.duration >= '. $sec_startduration;
                }
            }
            if($request->input('endduration') != "")
            {
                $explode_startduration = explode(":", $request->input('endduration'));
                $sec_startduration = intval($explode_startduration[0]) * 60 * 60 + intval($explode_startduration[1]) * 60;
                if(count($explode_startduration) == 3)
                {
                    $sec_startduration = $sec_startduration + intval($explode_startduration[2]);
                }
                if($sec_startduration > 0)
                {
                    $sql .= ' and d.duration <= '. $sec_startduration;
                }
            }
            if(($request->input('direction_in') != "" || $request->input('direction_out') != "" || $request->input('direction_vr') != "") && $request->input('direction_all') == "")
            {
                $sql .= " and (";
                if($request->input('direction_in') != "")
                {
                    $sql .= " d.direction = 0";
                }
                if($request->input('direction_out') != "")
                {
                    if($request->input('direction_in') != "")
                    {
                        $sql .= " or d.direction = 1";
                    }
                    else {
                        $sql .= " d.direction = 1";
                    }
                }
                if($request->input('direction_vr') != "")
                {
                    if($request->input('direction_in') != "" || $request->input('direction_out') != "")
                    {
                        $sql .= " or d.direction = 2";
                    }
                    else {
                        $sql .= " d.direction = 2";
                    }
                }
                $sql .= ")";
            }
            if($request->input('download_act') != "" && $request->input('download_non') == "")
            {
                $sql .= " and d.download_datetime IS NOT NULL";
            }
            else if ($request->input('download_act') == "" && $request->input('download_non') != "")
            {
                $sql .= " and d.download_datetime IS NULL";
            }
            if($request->input('localparty') != "")
            {
                $sql .= " and d.localparty like '%". $request->input('localparty') ."%'";
            }
            if($request->input('remoteparty') != "")
            {
                $sql .= " and d.remoteparty like '%". $request->input('remoteparty') ."%'";
            }

            if($request->input('freeword') != "")
            {
                $words = mb_convert_kana($request->input('freeword'), 's');
                $words = explode(' ', $words);
                $result_fr = array();
                $temp_sql = "select record_id from recognition where";
                if($request->input('and_or') == "and")
                {
                    $first_row = 1;
                    foreach ($words as $value) {
                        if($first_row == 1)
                        {
                          $temp_sql .= " text like '%". $value ."%'";
                          $first_row = -9;
                        }
                        else {
                          $temp_sql .= " and text like '%". $value ."%'";
                        }
                    }
                    $temp_sql .= ";";
                    $results = DB::select(DB::raw($temp_sql));
                    $i = 0;
                    foreach ($results as $value) {
                        $result_fr[$i] = $value->record_id;
                    }
                }
                else if ($request->input('and_or') == "or") {
                    $first_row = 1;
                    foreach ($words as $value) {
                        if($first_row == 1)
                        {
                          $temp_sql .= " text like '%". $value ."%'";
                          $first_row = -9;
                        }
                        else {
                          $temp_sql .= " or text like '%". $value ."%'";
                        }
                    }
                    $temp_sql .= ";";
                    $results = DB::select(DB::raw($temp_sql));
                    $i = 0;
                    foreach ($results as $value) {
                        $result_fr[$i] = $value->record_id;
                    }
                }
                $sql .= " and d.id in (". implode(',',$result_fr) .")";
            }

            $sql .= " order by timestamp desc";
            $sql .= ";";
          }
          else
          {
            $sql = $request->input('search_sql');
          }
   //return $sql;
          $data['search_sql'] = $sql;

          $modObj = DB::select($sql);
          $modTopicsObjs = topic::all();
          foreach ($modObj as $key => $obj) {
                  foreach ($modTopicsObjs as $topic) {
                      if($obj->topic_id == $topic->topic_id)
                      {
                          $obj->topic_title = $topic->topic_title;
                      }
                  }
              $obj->filename_org = $obj->filename;
              if(!is_null( $obj->filename))
              {
                  $sYear = substr($obj->timestamp, 0, 4);
                  $sMonth = substr($obj->timestamp, 5, 2);
                  $sDay = substr($obj->timestamp, 8, 2);
                  $obj->filename = $sYear.'/'.$sMonth.$sDay.'/'.$obj->filename;
              }
              $obj->timestamp_date = substr($obj->timestamp, 0, 10);
              $obj->timestamp_time = substr($obj->timestamp, 11, 8);

              if(!is_null($request->input('start')))
              {
                  $obj->start = $request->input('start');
              }

              $obj->duration_sec = $obj->duration;
              $obj->duration = $this->s2h($obj->duration);

              //check quick tag exists
              if($mTag_use == 1){
                  $tagObjCount = tag::where('rec_id','=',$obj->id)->where('tag_category_id','=',$this->$mTag_id)->count();
                  if ($tagObjCount > 0) {
                      $obj->qtag_exist = true;
                  }
                  else {
                      $obj->qtag_exist = false;
                  }
              }

              //group
              $grpObj = Group::find($obj->group_id);
              if (!is_null($grpObj)) {
                    $obj->group_name = $grpObj->group_name;
              } else {
                  $obj->group_name = 'No Entry';
              }

              if(isset($obj->download_datetime))
              {
                $obj->download_datetime_date = substr($obj->download_datetime, 0, 10);
                $obj->download_datetime_time = substr($obj->download_datetime, 11, 8);
              } else {
                $obj->download_datetime_date = '-';
                $obj->download_datetime_time = '';
              }

              $txtObj = recognition::where('record_id','=',$obj->id)->orderBy('start_time', 'asc')->get();
              //return $obj['id'];
              if($txtObj->count()>0)
              {
                  $obj->voicetext1 = null;
                  $obj->voicetext2 = null;
                  foreach ($txtObj as $txt) {
                      $obj->voicetext1 .= $txt->text.':'.$txt->direction.' ';
                      $obj->voicetext2 .= $txt->text.' ';
                  }
              }
              else {
                  $obj->voicetext1 = 'No text';
                  $obj->voicetext2 = 'No text';
              }

              $item_ary[$key] = $obj;
          }

          //create pagination
          if(!empty($request->input('page')))
          {
              $paginateResponse = $this->paginateMe($item_ary, $request->input('selected_page'), $request, $request->input('page'));
          }
          else
          {
              $paginateResponse = $this->paginateMe($item_ary, $request->input('selected_page'), $request, 1);
          }
          $data['selected_page'] = $request->input('selected_page');

          $tag_cat = TagCatagory::all();
          $tag_group = Group::all();
          $ip[0] = $_SERVER['REMOTE_ADDR'];
          $ip[1] = getenv('SERVER_ADDR');
          //Session::put('show_details', 0);

          //config values should be load from database
          $config = array(
            "player_value"  => $configuration['player_select'], //should be loaded from a new DB table called config
            "app_type"      => $configuration['youwire_type'], //should be loaded from a new DB table called config => office = 0 / mobile_app = 1/ mobile_phone = 2/ meeting = 3
            "bulk_dl_limit" => $configuration['bulk_dl_limit'], //should be loaded from a new DB table called config
            "customer" => $configuration['customer'], //default = 0, softbank = 1
            "status_count" => 0, //array(0 => 1),
          );

          return view('welcome')->with('old_req',$request)->with('ips', $ip)->with('tag_cat', $tag_cat)->with('tag_group', $tag_group)->with('config',$config)->with('res',$paginateResponse)->with('data',$data)->with('show_details','0');
          //return redirect()->back()->withInput($request->all)->with('ips', $ip)->with('tag_cat', $tag_cat)->with('tag_group', $tag_group)->with('config',$config)->with('res',$paginateResponse)->with('data',$data);
    }

    public function download_control(Request $request)
    {
        $obj = youwire_default::find($request->id);
        $obj['duration'] = $this->s2h($obj->duration);

        $filename = $obj->filename;
    		$timestamp = $obj->timestamp;
    		$localparty = $obj->localparty;
    		$remoteparty = $obj->remoteparty;
    		$direction = $obj->direction;
    		$timestamp_date = substr( $obj->timestamp, 0, 10);

    		$sYear = substr( $timestamp, 0, 4);
    		$sMonth = substr( $timestamp, 5, 2);
    		$sDay = substr( $timestamp, 8, 2);

        // ファイルパス生成
    		// orekaの場合は、_MD_YOUWIRE_PLAYER_WMP_VALUE
    		if( $this->mPlayer == 1 ){
    			if( $this->mFilePath == "" ){
    				//$this->mFilePath = '/var/log/orkaudio/audio';
            $this->mFilePath = public_path(). "/recordings";
    			}
    			$path_file = $this->mFilePath . '/' . $filename;
    		}else{
    			$path_file = public_path(). '/recordings' . '/' .$localparty. '/' . $sYear . '/' . $sMonth . $sDay . '/' . $filename;
    		}

        // ファイルの存在確認
    		if (!file_exists($path_file)) {
    			die('Error: File('.$path_file.') does not exist');
    			die();
    		}

        // オープンできるか確認
    		if (!($fp = fopen($path_file, 'r'))) {
    			die('Error: Cannot open the file('.$path_file.')');
    			die();
    		}

        // ファイルサイズの確認
    		if (($content_length = filesize($path_file)) == 0) {
    			die('Error: File size is 0.('.$path_file.')');
    			die();
    		}
    		fclose($fp);

        //出力ファイル名の編集
    		$direction_name = '';
    		if ($direction == 0) {
    			$direction_name = '_in';
    		} else if ($direction == 1) {
    			$direction_name = '_out';
    		}

        $remoteparty = ($remoteparty == 'ボイスレコーダー') ? 'VoiceRecorder' : $remoteparty;

        $filename_parts = explode('.',$filename);
		    $extension = $filename_parts[1];


        $dl_filename = $localparty.$direction_name.'_'.$remoteparty.'_'.str_replace(' ','_',$timestamp).'.'.$extension;

        // ダウンロード日時の更新
        $obj = youwire_default::find($request->id);
        $obj->download_datetime = date("Y-m-d H:i:s");
        $obj->save();

        // ダウンロード用のHTTPヘッダ送信
    		if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE")) {
    			header("Pragma: cache;");
    			header("Cache-Control: private");
    		}
    		header("Content-Disposition: attachment; filename=\"".$dl_filename."\"");
    		header("Content-Length: ".$content_length);
    		header("Content-Type: audio/mpeg");

    		// ファイルを読んで出力
    		if (!readfile($path_file)) {
    			die('Cannot read the file('.$path_file.')');
    			die();
    		}

        return redirect()->back()->withInput();
    }

    public function csvoutput_control(Request $request)
    {
        $ids = array();
        $out_all_check = $request->csv_out_all;

        if($out_all_check)
        {
            $sql = $request->search_sql;
            $modObj = DB::select($sql);
            foreach ($modObj as $value) {
                $ids[] = $value->id;
            }
        }
        else {
            $ids = $request->chkselect;
        }

        if(empty($ids)){
            $this->search($request);
        }
        else {
          $sql = 'SELECT ' .
                  'd.id, '.
                  'd.direction, '.
                  'd.duration, '.
                  'd.filename, '.
                  'd.localparty, '.
                  'd.remoteparty, '.
                  'd.uid, '.
                  'd.timestamp, '.
                  'd.latitude, '.
                  'd.longitude, '.
                  'd.location, '.
                  'd.upload_file_size, '.
                  'd.protect, '.
                  'd.localip, '.
                  'd.remoteip, '.
                  'd.portname, '.
                  'd.service_id, '.
                  'd.agent_id, '.
                  'd.nativecall_id, '.
                  'd.group_id, '.
                  'g.group_name, '.
                  't.tag_id, '.
                  't.tag_title, '.
                  't.tag_text, '.
                  'tc.name '.
                  ' FROM defaults AS d LEFT JOIN tags AS t ON (d.id = t.rec_id) LEFT JOIN tag_catagories AS tc ON (t.tag_category_id = tc.id) LEFT JOIN groups AS g ON (d.group_id = g.id) WHERE d.id IN ( ' . implode( ',' , $ids ) . ' );';

                  $result = DB::select($sql);

            			if( preg_match("/firefox/i" , $_SERVER['HTTP_USER_AGENT']) ){
            				header("Content-Type: application/x-csv");
            			}else{
            				header("Pragma: public");
            				header("Content-Type: application/octet-stream");
            			}

                  header("Content-Disposition: attachment ; filename=\"record_data_output.csv\"") ;
            			ob_end_clean();
            			$fp = fopen('php://output','w');

                  // csv_kind : 0 youwire_defaultの同一レコード情報の2件目以降を空値(id以外)で出力 : 1 通常出力
			            $csv_kind = 0;

                  $rec_count = 0;
            			if ( $csv_kind == 0 ) {
            				$rec_id = '';
            			}

            			foreach ($result as  $obj) {
            				if ( $rec_count == 0 ) {
            					$header_array = array(
             							'id',
            							'方向',
            							'通話時間',
            							'ファイル名',
            							'自番号',
            							'相手番号',
            							'UID',
            							'通話開始時間',
            							'緯度',
            							'経度',
            							'住所',
            							'ファイルサイズ(byte)',
            							'保護状態',
            							'自IPアドレス',
            							'相手IPアドレス',
            							'録音ポート番号',
            							'サービスID',
            							'エージェントID',
            							'コールID',
            							'グループID',
            							'グループ名',
            							'タグID',
            							'タグタイトル',
            							'タグテキスト',
            							'タグカテゴリ名'
            					);

            //					$header_array = array_keys($row);
            					foreach($header_array as &$value){
            						$value = mb_convert_encoding($value, 'SJIS-win','UTF-8');
            					}
            					unset($value);

            					fputcsv($fp,$header_array,',','"');
            				}

                    $obj->duration = $this->s2h($obj->duration);
				            $obj->remoteparty = ($obj->remoteparty == "ボイスレコーダー") ? "VoiceRecorder" : $obj->remoteparty;

                    //出力ファイル名の編集
            				if($obj->filename){
            					$direction_name = '';
            					if ($obj->direction == 0) {
            						$direction_name = '_in';
            					} else if ($obj->direction == 1) {
            						$direction_name = '_out';
            					}
            					$filename_parts = explode('.',$obj->filename);
            					$extension = $filename_parts[1];
            					$obj->filename = $obj->localparty.$direction_name.'_'.$obj->remoteparty.'_'.str_replace(':','-',str_replace(' ','_',$obj->timestamp)).'.'.$extension;
            				}
                    if($obj->localip){
            					$obj->localip = long2ip($obj->localip);
            				}
            				if($obj->remoteip){
            					$obj->remoteip = long2ip($obj->remoteip);
            				}

            //        if ( $csv_kind == 0 ) {
            // 					if ( $row['id'] == $rec_id ) {
            // 						for ($i = 0; $i < count($header_array) - 4; $i++) {
            // 							$val = $header_array[$i];
            // 							if ( $i == 0 ) {
            // 								$row_def[$val] = $row['id'];
            // 							} else {
            // 								$row_def[$val] = '';
            // 							}
            // 						}
            // 						$row = $row_def + array_slice( $row , count( $row ) - 4 );
            // 					}
            //				}
                    $row = array();
                    foreach ($obj as $key => $val) {
                        $row[$key] = $val;
                    }

                    foreach($row as &$value){
            					$value = mb_convert_encoding($value, 'SJIS-win','UTF-8');
            				}
            				unset($value);
            				fputcsv($fp,$row,',','"');

            				$rec_count = $rec_count + 1;
            				if ( $csv_kind == 0 ) {
            					$rec_id = $obj->id;
            				}
                  }

                  fclose($fp);
            			exit();

            			//$this->search();
                  return redirect()->back()->withInput();
          }
    }


    public function control_update(Request $request)
    {
      $ids = $request->input('chkselect');
      $protect = $request->input('protect');
      // operation log
  		$action = ( $protect == 1 ) ? "保護" : "保護解除";
      $youwirelog = new youwireLogController();
      $youwirelog->operation("TOP",$action, 'id = '.implode(',', $ids));

      foreach ($ids as $id)
      {
          $record = youwire_default::find($id);
          $record->protect = $protect;
          $record->save();
      }
      return redirect()->back()->withInput();
    }


    public function control_delete(Request $request)
    {
       $ids = $request->input('chkselect');

       //operation log
       $youwirelog = new youwireLogController();
       $youwirelog->operation("TOP","削除", 'id = '.implode(',', $ids));

       foreach ($ids as $id)
       {
          $obj = youwire_default::find($id);
          $uname = $obj['localparty'];
          $timestamp = $obj['timestamp'];
          $uid = $obj['uid'];
          $filename = $obj['fileName'];

          if($obj['protect'] == 0)
          {
            $player = config::where('conf_name','=','player_select')->first();
            $file_Path = config::where('conf_name','=','file_path')->first();

            if($player['conf_value'] === '1')
            {
                $filePath = $file_Path['conf_Value'];
            }
            else
            {
                $sYear = substr( $timestamp, 0, 4);
                $sMonth = substr( $timestamp, 5, 2);
                $sDay = substr( $timestamp, 8, 2);
                $filePath = $file_Path['conf_Value'] . '/'. $uname .'/' . $sYear . '/' . $sMonth . $sDay . '/';
            }

            if(file_exists($filePath.$filename))
            {
              if(!unlink($filePath.$filename))
              {
                return 'ファイルの削除に失敗しました';
              }
            }

            $deletedRecords = youwire_default::where('id','=',$id)->where('uid','=',$uid)->where('protect','=','0')->delete();
            if(!$deletedRecords)
            {
              return 'データベースからレコードを削除できませんでした';
            }

            return '削除に成功しました';
          }
          else
          {
            return 'ファイルは削除保護されています';
          }
       }
    }


    /**
    * Paginate a laravel colletion or array of items.
    *
    * @param  array|Illuminate\Support\Collection $items   array to paginate
    * @param  int $perPage number of pages
    * @return Illuminate\Pagination\LengthAwarePaginator    new LengthAwarePaginator instance
    */
     function paginateMe($itemsList, $perPage, $request, $current_Page){
          if($current_Page == 1)
          {
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
          }
          else {
            $currentPage = $current_Page;
          }
          $itemCollection = collect($itemsList);
          $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
          if($current_Page == 1 ) // to highlight the current page tab in the paginate link set()
          {
            $paginateItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
          }
          else
          {
            $paginateItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage, $current_Page);
          }
         $paginateItems->setPath($request->url());
         return $paginateItems;
     }


    //COnvert seconds to hours
    private function s2h($seconds) {

      		$hours = floor($seconds / 3600);
      		$minutes = floor(($seconds / 60) % 60);
      		$seconds = $seconds % 60;

      		$hms = sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);

      		return $hms;

	   }
}
