<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\youwire_default;
use App\Group;
use App\TagCatagory;
use App\config;
use App\tag;

class DetailController extends Controller
{
  var $mPlayer = 0;
  var $mFilePath = "";
  /**
   * Instantiate a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $mPlayer = 0; //Load from DB
      $mFilePath = ""; //load from DB
      $this->middleware('auth')->except('index');

  }

  public function control_view_detail(Request $request)
  {
    $security_group = DB::table('security_groups')
                        ->leftjoin('profile_datas','security_groups.security_group_id', '=', 'profile_datas.security_group_id')
                        ->where('profile_datas.uid','=',auth()->user()->id)
                        ->select('security_groups.*')
                        ->get();

    foreach ($security_group as $sgroup) {
        $data['authority_bulk_download'] = $sgroup->authority_bulk_download;
        $data['authority_delete'] = $sgroup->authority_delete;
        $data['authority_download'] = $sgroup->authority_download;
        $data['authority_protect'] = $sgroup->authority_protect;
        $data['authority_add_tag'] = $sgroup->authority_add_tag;
        $data['authority_edit_tag_category'] = $sgroup->authority_edit_tag_category;
        $data['authority_edit_security_group'] = $sgroup->authority_edit_security_group;
        $data['authority_edit_group'] = $sgroup->authority_edit_group;
        $data['authority_edit_user'] = $sgroup->authority_edit_user;
        $data['authority_edit_alertmail'] = $sgroup->authority_edit_alertmail;
    }
    $data['search_sql'] = $request->input('search_sql');
    $data['selected_page'] = $request->input('selected_page');
    $data['page'] = $request->input('page');

    $obj = youwire_default::find($request->recid);
    if(!empty($obj))
    {
      $obj['duration'] = $this->s2h($obj['duration']);

      if($obj['group_id'] == 0)
      {
        $obj['group_name'] = '所属なし';
      }
      else
      {
        $grpObj = Group::find($obj['group_id']);
        if(!empty($grpObj))
        {
          $obj['group_name'] = $grpObj['group_name'];
        }
      }

      $obj->filename_org = $obj->filename;
      if(!is_null( $obj->filename))
      {
          $sYear = substr($obj->timestamp, 0, 4);
          $sMonth = substr($obj->timestamp, 5, 2);
          $sDay = substr($obj->timestamp, 8, 2);
          $obj->filename = $sYear.'/'.$sMonth.$sDay.'/'.$obj->filename;
      }

      // レコードIDに対するタグ情報の取得
			$sql = "SELECT t.*, tc.name FROM tags AS t LEFT JOIN tag_catagories AS tc ON (t.tag_category_id = tc.id) WHERE t.rec_id = " . $request->recid . " ORDER BY t.tag_id;";
      $modObj = DB::select($sql);
      $tagListData = array();
      foreach ($modObj as $value)
      {
        $row['tag_catagories_name'] = htmlspecialchars($value->name);
        $row['tag_title'] = htmlspecialchars($value->tag_title);
				$row['tag_text'] = nl2br(htmlspecialchars($value->tag_text));
        $row['tag_id'] = $value->tag_id;
        $row['tag_create_date'] = $value->tag_create_date;
        $row['tag_update_date'] = $value->tag_update_date;
        $tagListData[] = $row;
      }

      $tag_cat = TagCatagory::all();
      $tag_group = Group::all();
      $ip[0] = $_SERVER['REMOTE_ADDR'];
      $ip[1] = getenv('SERVER_ADDR');

      //config values should be load from database
      $config = array(
        "player_value"  => 0,
        "app_type"      => 0, //office = 0 / mobile_app = 1/ mobile_phone = 2/ meeting = 3
        "bulk_dl_limit" => 0,
        "customer" => 0, //default = 0, softbank = 1
        "status_count" => 0, //array(0 => 1),
      );
      //return redirect()->back()->with('show_details','1')->withInput()->with('res',$obj)->with('tagListData', $tagListData);
      return view('detailView')->with('ips', $ip)->with('config',$config)->with('tag_cat', $tag_cat)->with('tag_group', $tag_group)->with('old_req',$request)->with('res',$obj)->with('tagListData', $tagListData)->with('data',$data);
    }
  }


  public function control_detail_update(Request $request)
  {
    $id = $request->input('id');
    $protect = $request->input('protect');

    $action = ( $protect == 1 ) ? "保護" : "保護解除";
    $youwirelog = new youwireLogController();
    $youwirelog->operation("Detail View",$action, 'id = '.$id);

    $record = youwire_default::find($id);
    $record->protect = $protect;
    $record->save();

    return redirect()->back()->withInput();
  }

  public function control_detail_delete(Request $request)
  {
    $id = $request->input('id');
    //operation log
    $youwirelog = new youwireLogController();
    $youwirelog->operation("Detail View","削除", 'id = '.$id);

    $obj = youwire_default::find($id);
    $uname = $obj['localparty'];
    $timestamp = $obj['timestamp'];
    $uid = $obj['uid'];
    $filename = $obj['fileName'];

    if($obj['protect'] == 0)
    {
      $player = config::where('conf_name','=','player_select')->first();
      $file_Path = config::where('conf_name','=','file_path')->first();

      if($player['conf_value'] === '1')
      {
          $filePath = $file_Path['conf_Value'];
      }
      else
      {
          $sYear = substr( $timestamp, 0, 4);
          $sMonth = substr( $timestamp, 5, 2);
          $sDay = substr( $timestamp, 8, 2);
          $filePath = $file_Path['conf_Value'] . '/'. $uname .'/' . $sYear . '/' . $sMonth . $sDay . '/';
      }

      if(file_exists($filePath.$filename))
      {
        if(!unlink($filePath.$filename))
        {
          return 'ファイルの削除に失敗しました';
        }
      }

      $deletedRecords = youwire_default::where('id','=',$id)->where('uid','=',$uid)->where('protect','=','0')->delete();
      if(!$deletedRecords)
      {
        return 'データベースからレコードを削除できませんでした';
      }

    return '削除に成功しました';
    }
    else
    {
      return 'ファイルは削除保護されています';
    }
  }

  public function control_detail_download(Request $request)
  {
    $id = $request->input('id');
    //operation log
    $youwirelog = new youwireLogController();
    $youwirelog->operation("Detail View","削除", 'id = '.$id);

    $obj = youwire_default::find($id);
    $filename = $obj['filename'];
    $timestamp = $obj['timestamp'];
    $localparty = $obj['localparty'];
    $remoteparty = $obj['remoteparty'];
    $direction = $obj['direction'];

    $sYear = substr( $timestamp, 0, 4);
		$sMonth = substr( $timestamp, 5, 2);
		$sDay = substr( $timestamp, 8, 2);

    // ファイルパス生成
    // orekaの場合は、_MD_YOUWIRE_PLAYER_WMP_VALUE
    if( $this->mPlayer == 1 ){
      if( $this->mFilePath == "" ){
        //$this->mFilePath = '/var/log/orkaudio/audio';
        $this->mFilePath = public_path(). "/recordings";
      }
      $path_file = $this->mFilePath . '/' . $filename;
    }else{
      $path_file = public_path(). '\recordings' . '\\' .$localparty. '\\' . $sYear . '\\' . $sMonth . $sDay . '\\' . $filename;
    }

    // ファイルの存在確認
    if (!file_exists($path_file)) {
      die('Error: File('.$path_file.') does not exist');
      die();
    }

    // オープンできるか確認
    if (!($fp = fopen($path_file, 'r'))) {
      die('Error: Cannot open the file('.$path_file.')');
      die();
    }

    // ファイルサイズの確認
    if (($content_length = filesize($path_file)) == 0) {
      die('Error: File size is 0.('.$path_file.')');
      die();
    }
    fclose($fp);

    //出力ファイル名の編集
    $direction_name = '';
    if ($direction == 0) {
      $direction_name = '_in';
    } else if ($direction == 1) {
      $direction_name = '_out';
    }

    $remoteparty = ($remoteparty == 'ボイスレコーダー') ? 'VoiceRecorder' : $remoteparty;

    $filename_parts = explode('.',$filename);
    $extension = $filename_parts[1];
    $dl_filename = $localparty.$direction_name.'_'.$remoteparty.'_'.str_replace(' ','_',$timestamp).'.'.$extension;
    // ダウンロード日時の更新
    $obj = youwire_default::find($request->id);
    $obj->download_datetime = date("Y-m-d H:i:s");
    $obj->save();

    // ダウンロード用のHTTPヘッダ送信
    if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE")) {
      header("Pragma: cache;");
      header("Cache-Control: private");
    }
    header("Content-Disposition: attachment; filename=\"".$dl_filename."\"");
    header("Content-Length: ".$content_length);
    header("Content-Type: audio/mpeg");

    // ファイルを読んで出力
    if (!readfile($path_file)) {
      die('Cannot read the file('.$path_file.')');
      die();
    }

    return redirect()->back()->withInput();
  }

  public function control_tag_delete(Request $request)
  {
    $id = $request->input('id');
    $tagid = $request->input('tag_id');

    //operation log
    $youwirelog = new youwireLogController();
    $youwirelog->operation("Detail View","削除", 'id = '.$id.';tag_id = '.$tagid);

    $deletedTag = tag::where('tag_id','=',$tagid)->delete();
    /*if(!$deletedRecords)
    {
      return 'データベースからレコードを削除できませんでした';
    }*/
    return redirect()->back()->withInput();
  }

  public function control_tag_edit(Request $request)
  {
    $id = $request->input('id');
		$tag_id = $request->input('tag_id');
    $selected_tag_edit = $request->input('selected_tag_edit');
		$tag_title = $request->input('tag_title');
		$tag_text = $request->input('tag_text');
		$edit_type = $request->input('edit');

    $error = array();
    if ( $selected_tag_edit == '0' ) {
			$errMsg[] = 'select tag catagory name';
		}
		if ( strlen(mb_convert_encoding($tag_title,'SJIS', 'UTF-8')) > 64 ) {
			$errMsg[] = 'タイトルは半角64文字以内、全角32文字以内で入力して下さい。';
		}

    if(empty($errMsg))
    {
      $update_uid = auth()->user()->id;
      $update_timestamp = date('Y-m-d H:i:s');

      switch($edit_type)
      {
        case 1: //operation log
                $action = ( $edit_type == 1 ) ? '新規登録' : '更新';
                $youwirelog = new youwireLogController();
                $youwirelog->operation("Detail View","削除", 'id = '.$id);

                $tagObj = new tag;
                $tagObj->rec_id = $id;
                $tagObj->tag_category_id = $selected_tag_edit;
                $tagObj->tag_title = $tag_title;
                $tagObj->tag_text = $tag_text;
                $tagObj->tag_update_uid = $update_uid;
                $tagObj->tag_update_date = $update_timestamp;
                $tagObj->tag_create_uid = $update_uid;
                $tagObj->tag_create_date = $update_timestamp;
                $tagObj->save();
                break;
        case 2: //operation log
                $youwirelog1 = new youwireLogController();
                $youwirelog1->operation("Detail View","更新", 'id = '.$id.';tag_id = '.$tag_id);

                $tagEditObj = tag::find($tag_id);
                $tagEditObj->tag_category_id = $selected_tag_edit;
                $tagEditObj->tag_title = $tag_title;
                $tagEditObj->tag_text = $tag_text;
                $tagEditObj->tag_update_uid = $update_uid;
                $tagEditObj->tag_update_date = $update_timestamp;
                $tagEditObj->save();
                break;
      }
    }
    return redirect()->back()->withInput();
  }

  //COnvert seconds to hours
  private function s2h($seconds) {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;

        $hms = sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
        return $hms;
   }
}//END of class
