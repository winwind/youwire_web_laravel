<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Group;
use App\User;
use App\groups_users_link;
use App\Security_group;

class GroupController extends Controller
{	
	/**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Group table start 2018/08/09 end 2018/08/09
     */
    public function index()
    {
    	$id_user = auth()->user()->id;	
        $get_groups =  group::
				    join('groups_users_links', 'groups.id', '=', 'groups_users_links.group_id')
				    ->where('groups_users_links.uid', $id_user)
				    ->select('*')
				    ->orderBy('id','asc')
				    ->get();

        return view('group.index',[
        	'get_groups' => $get_groups
        ]);
    } 
     /**
     * create New Gruop  start 2018/08/10  end 2018/08/13
     */
    public function create(Request $request)
    {
        $create_group = group::insert([
                        'group_name' => $request->group_name,
                        'group_node' => '.'.$request->group_node.'.',
                        'group_sort' => 99999,
                        'group_status' => 0
                        ]);

        $new_group_id = group::orderBy('id', 'desc')->value('id');
        groups_users_link::insert([
                        'group_id' => $new_group_id,
                        'uid' => auth()->user()->id
        ]);

        return back();
    }
     /**
     * Update Group information start 2018/08/09 end 2018/08/13
     */
    public function update(Request $request, $id)
    {   
        $get_group = group::find($id);
        $get_group->group_name = $request->group_name;
        $get_group->save();

        return redirect()->action('GroupController@index');
    }
     /**
     * Save Group start 2018/08/15 
     */
    public function save(Request $request)
    {
        return redirect()->action('GroupController@index');
    }
     /**
     * Edit Group start 2018/08/09 end 2018/08/13
     */
    public function edit($id)
    {
    	$get_group = group::findOrFail($id); 

        return view('group.edit', ['get_group' => $get_group]); 
    }
     /**
     * Delete Group start 2018/08/09 end 2018/08/09
     */
    public static function destroy($id)
    {
    	group::find($id)->delete();
		groups_users_link::where('group_id', $id)->delete();

        return back();
    }
}
