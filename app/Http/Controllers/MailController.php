<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\alertmail_setting;

class MailController extends Controller
{

    public function send($to)
    {
      Mail::send(['text' => 'mail'], ['name' => 'Youwire Alert System'], function($message){
        $message->to($to, 'To '.$to)->subject(mb_encode_mimeheader("【YouWire】サーバー保存容量のご連絡"););
        $message->from('support@geekfeed.co.jp', '[Geekfeed] Youwire Alert System');
      });
    }

}


  date_default_timezone_set('Asia/Tokyo');	// タイムゾーンの設定
  echo "----- alertmail.php Start. " . date('Y/m/d H:i:s') . " -----\n";

  // URL直叩きは無効
  if ( isset($_SERVER['HTTP_USER_AGENT']) ) {
    echo "----- Error URL直叩きです。 -----\n";
    echo "----- alertmail.php End. " . date('Y/m/d H:i:s') . " -----\n";
    exit(0);

    // ターゲットとなるIDの指定
     $target_id = 0;

     echo "----- これからアラートメールの設定情報を取得します. " . date('Y/m/d H:i:s') . " -----\n";

     $alertObj = alertmail_setting::where('id','=',$target_id)->orderBy('category_id', 'asc')->->orderBy('sub_category_id', 'asc')->get();

     echo "----- アラートメール送信設定情報を取得しました. " . date('Y/m/d H:i:s') . " -----\n";
     echo "----- アラートメール送信設定情報は" . count($alertObj) . "件ありました. " . date('Y/m/d H:i:s') . " -----\n";

     // 設定情報がある場合は以下処理実行
     if(!empty($alertObj))
     {
        // アラートメール情報の取得
        $mails = array();
        foreach ($alertObj as $obj)
        {
          switch($obj->category_id)
          {
            case 1: if(isset($obj->value))
                    {
                      $flag = 1;
                      $limit = $obj->value;
                    }
                    break;
            case 2: $mails[] = $obj->value.",";
                    break;
            case 3: $day = $obj->value;
                    break;
            case 4: $time = $obj->value;
                    break;
          }
        }

        // システム日時の曜日取得
        $sys_week_no = date('w');

        // システム日時の時分と設定時分の10分後の値を取得(バッチ処理が必ず指定時間に開始されるとは限らないため、誤差も考慮)
        $sys_time = date('Hi');
        $target_date = strtotime('2016-01-01 ' . $time . ':00:00');
        $start_time = date('Hi', $target_date);
        $end_time = date('Hi', strtotime('+60 min',$target_date));
        if ( $end_time == 0 ) { $end_time = 2400; }

        // 曜日、時間のチェック
        if ( $sys_week_no == $day && ( $start_time <= $sys_time && $sys_time <= $end_time ) )
        {
          // 容量制限チェック
          $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
          $base = 1024;
          $path = 'C:';

          //全体サイズ
          $total_bytes = disk_total_space($path);
          $class = min((int)log($total_bytes , $base) , count($si_prefix) - 1);
          echo "----- 全体サイズ:" . sprintf('%1.2f' , $total_bytes / pow($base,$class)) . $si_prefix[$class]  . date('Y/m/d H:i:s') . " -----\n";

          //空き容量
          $free_bytes = disk_free_space($path);
          $class = min((int)log($free_bytes , $base) , count($si_prefix) - 1);
          echo "----- 空き容量:" . sprintf('%1.2f' , $free_bytes / pow($base,$class)) . $si_prefix[$class]  . date('Y/m/d H:i:s') . " -----\n";

          //使用容量
          $used_bytes = $total_bytes - $free_bytes;
          $class = min((int)log($used_bytes , $base) , count($si_prefix) - 1);
          echo "----- 使用容量:" . sprintf('%1.2f' , $used_bytes / pow($base,$class)) . $si_prefix[$class]  . date('Y/m/d H:i:s') . " -----\n";

          //使用率
          echo "----- 使用率:" . round($used_bytes / $total_bytes * 100, 2)  . date('Y/m/d H:i:s') . " -----\n";

          if ( $limit <= round($used_bytes / $total_bytes * 100, 2) )
          {
            try{
              foreach ( $mails as $mail_to ){
                // メール送信処理
                //$Mail = new MailController();
                //$Mail->send($mail_to);
                echo $mail_to;
              }

            }catch (PDOException $e){
              print('Error:'.$e->getMessage());
            }catch (Exception $e){
              print('Error:'.$e->getMessage());
            }
          }
        }
     }
  }
