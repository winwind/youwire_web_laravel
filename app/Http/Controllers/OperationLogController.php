<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

class OperationLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      $security_group = DB::table('security_groups')
                          ->leftjoin('profile_datas','security_groups.security_group_id', '=', 'profile_datas.security_group_id')
                          ->where('profile_datas.uid','=',auth()->user()->id)
                          ->select('security_groups.*')
                          ->get();
      foreach ($security_group as $sgroup) {
        $data['authority_bulk_download'] = $sgroup->authority_bulk_download;
        $data['authority_delete'] = $sgroup->authority_delete;
        $data['authority_download'] = $sgroup->authority_download;
        $data['authority_protect'] = $sgroup->authority_protect;
        $data['authority_add_tag'] = $sgroup->authority_add_tag;
        $data['authority_edit_tag_category'] = $sgroup->authority_edit_tag_category;
        $data['authority_edit_security_group'] = $sgroup->authority_edit_security_group;
        $data['authority_edit_group'] = $sgroup->authority_edit_group;
        $data['authority_edit_user'] = $sgroup->authority_edit_user;
        $data['authority_edit_alertmail'] = $sgroup->authority_edit_alertmail;
      }
      $ip[0] = $_SERVER['REMOTE_ADDR'];
      $ip[1] = getenv('SERVER_ADDR');
      return view('operationlog')->with('ips',$ip)->with('data',$data);
    }

    public function search(Request $request)
    {
      $security_group = DB::table('security_groups')
                          ->leftjoin('profile_datas','security_groups.security_group_id', '=', 'profile_datas.security_group_id')
                          ->where('profile_datas.uid','=',auth()->user()->id)
                          ->select('security_groups.*')
                          ->get();
      foreach ($security_group as $sgroup) {
        $data['authority_bulk_download'] = $sgroup->authority_bulk_download;
        $data['authority_delete'] = $sgroup->authority_delete;
        $data['authority_download'] = $sgroup->authority_download;
        $data['authority_protect'] = $sgroup->authority_protect;
        $data['authority_add_tag'] = $sgroup->authority_add_tag;
        $data['authority_edit_tag_category'] = $sgroup->authority_edit_tag_category;
        $data['authority_edit_security_group'] = $sgroup->authority_edit_security_group;
        $data['authority_edit_group'] = $sgroup->authority_edit_group;
        $data['authority_edit_user'] = $sgroup->authority_edit_user;
        $data['authority_edit_alertmail'] = $sgroup->authority_edit_alertmail;
      }
      $ip[0] = $_SERVER['REMOTE_ADDR'];
      $ip[1] = getenv('SERVER_ADDR');

      $sql = "select u.uname as uname, o.* from operationlogs as o left join users as u on (o.uid = u.id) where o.uid = 1";
      $uname = $request->input('uname');
      if($uname != '')
      {
        $sql .= " and u.uname like '%".$uname."%'";
      }

      $startdatetime = $request->input('startdatetime');
      if($startdatetime != '')
      {
        $sql .= " and o.created_at >= '".$startdatetime."'";
      }

      $enddatetime = $request->input('enddatetime');
      if($enddatetime != '')
      {
        $sql .= " and o.created_at <= '".$enddatetime."'";
      }

      $sql .= ";";

      $modObj = DB::select($sql);

      $item_arr = array();
      foreach ($modObj as $key => $obj) {
        $obj->timestamp_date = substr($obj->created_at, 0, 10);
        $obj->timestamp_time = substr($obj->created_at, 11, 8);
        $item_arr[$key] = $obj;
      }

      $paginateResponse = $this->paginateMe($item_arr, 10, $request);
      return view('operationlog')->with('old_req',$request)->with('ips', $ip)->with('logListData',$paginateResponse)->with('data',$data);
    }

    /**
    * Paginate a laravel colletion or array of items.
    *
    * @param  array|Illuminate\Support\Collection $items   array to paginate
    * @param  int $perPage number of pages
    * @return Illuminate\Pagination\LengthAwarePaginator    new LengthAwarePaginator instance
    */
     function paginateMe($itemsList, $perPage, $request){
         $currentPage = LengthAwarePaginator::resolveCurrentPage();
         $itemCollection = collect($itemsList);
         $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
         $paginateItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
         $paginateItems->setPath($request->url());
         return $paginateItems;
     }
}
