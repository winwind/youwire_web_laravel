<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\TagCatagory;
use App\Group;
use App\group_users_link;
use App\youwire_default;
use App\topic;
use App\tag;
use App\recognition;
use App\config;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index()
    {
      if(Auth::guest())
      {
        return view('index');
      }
      else
      {
        return redirect()->route('welcome');
      }
    }

    public function welcome(Request $request)
    {
        if(empty(auth()->user()->id))
        {
            return redirect()->route('index');
        }
        $sql = '';
        $selected_tag = 0;
        $selected_group = -1;
        $group_ids = '';
        $datetime = date("Y/m/d");
        $startdatetime = $datetime . " 00:00:00";
        $request->startdatetime = $startdatetime;
        //$startdatetime = '2018-05-01 19:00:00';
        $item_ary = array();
        $userID = "";

        //configuration details should be loaded from a DB table
        $conf = config::all();
        $configuration = array();
        foreach ($conf as $confrow) {
            $configuration[$confrow->conf_name] = $confrow->conf_value;
        }

        $mTag_use = $configuration['quicktag'];  //should be loaded from DB
        $mTag_id;
        if($mTag_use)
        {
            $mTag_id = $configuration['quicktag_id']; //should be loaded from DB
        }

        //Other data array
        $data = array();
        $data['tag_use'] = $mTag_use;
        if($mTag_use)
        {
            $qTagCategory = TagCatagory::find($mTag_id);
            foreach ($qTagCategory as $value) {
                $data['qtag_name'] = $value->name;
            }
        }


        //load data from database
        $security_group = DB::table('security_groups')
                            ->leftjoin('profile_datas','security_groups.security_group_id', '=', 'profile_datas.security_group_id')
                            ->where('profile_datas.uid','=',auth()->user()->id)
                            ->select('security_groups.*')
                            ->get();
        foreach ($security_group as $sgroup) {

            $data['authority_bulk_download'] = $sgroup->authority_bulk_download;
            $data['authority_delete'] = $sgroup->authority_delete;
            $data['authority_download'] = $sgroup->authority_download;
            $data['authority_protect'] = $sgroup->authority_protect;
            $data['authority_add_tag'] = $sgroup->authority_add_tag;
            $data['authority_edit_tag_category'] = $sgroup->authority_edit_tag_category;
            $data['authority_edit_security_group'] = $sgroup->authority_edit_security_group;
            $data['authority_edit_group'] = $sgroup->authority_edit_group;
            $data['authority_edit_user'] = $sgroup->authority_edit_user;
            $data['authority_edit_alertmail'] = $sgroup->authority_edit_alertmail;


            if ($sgroup->authority_search <> 1) {
                $group_ids = '-1';
            } else {
                $group_id_array = array();
                if ($sgroup->operating_range == 1 || $sgroup->operating_range == 2 || $sgroup->operating_range == 3){
                    $result_group_id = group_users_link::where('uid','=',auth()->user()->id)->select('group_id')->get();
                    $i = 0;
                    foreach ($result_group_id as $value) {
                        $group_id_array[$i] = $value->group_id;
                        $i++;
                    }
///////////////////////////////////////////////////////////////////////
                    if ($group_id_array[0] == 0) {
                        $group_ids = '0';
                    } else {
                        if ($sgroup->operating_range == 3) {
                            $sql_group_node = Group::select('group_node')->distinct()->whereIn('group_id', $group_id_array)->get();
                            $group_id_array = array();
                            foreach ($sql_group_node as $node) {
                                $sql_group_id_2 = Group::select('group_id')->distinct()->where('group_node','like',$node->group_node.'%')->get();
                                $j = 0;
                                foreach ($sql_group_id_2 as $value) {
                                    $group_id_array[$j] = $value->group_id;
                                    $j++;
                                }
                            }
                        }

                        if ($group_id_array[0] == 0) {
                            $group_ids = '-1';
                        } else {
                            $group_ids = implode(',', $group_id_array);
                        }
                    }
                    if($sgroup->operating_range == 1)
                    {
                        $userID = auth()->user()->id;
                    }
                }
///////////////////////////////////////////////////////////////////
                $group_list = array();
                if($group_ids <> ''){
                    $grpHandler = Group::whereIn('id',$group_ids)->orderBy('group_sort', 'asc')->get();
                }
                else {
                    $grpHandler = Group::orderBy('group_sort', 'asc')->get();
                }
                foreach ($grpHandler as $value) {
                    $id = $value->id;
                    foreach ($value->toArray() as $key2 => $v) {
                      if ($key2 == "group_name") {
                          $group_class = substr_count($value->group_node,'.') - 1;
                          $v = str_repeat(">", $group_class - 1) . $v;
                      }
                      $group_list[$id][$key2] = $v;
                    }
                }
                if (!is_null($selected_group) && $selected_group >= 0) {
                    $group_ids = $selected_group;
                }
              }// end of old youwire => getGroupIds()
        }//end for foreach of groupIDS

        //return $group_ids;
        if($group_ids <> '')
        {
            if($userID == ""){
              $modObj = youwire_default::whereIn('group_id', $group_ids)->where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->get();
              $sql = youwire_default::whereIn('group_id', $group_ids)->where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->toSql();
            }
            else {
              $modObj = youwire_default::whereIn('group_id', $group_ids)->where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->where('uid','=',$userID)->get();
              $sql = youwire_default::whereIn('group_id', $group_ids)->where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->where('uid','=',$userID)->toSql();
            }
        }
        else {
            if($userID == ""){
              $modObj = youwire_default::where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->get();
              $sql = youwire_default::where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->toSql();
            }
            else {
              $modObj = youwire_default::where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->where('uid','=',$userID)->get();
              $sql = youwire_default::where('timestamp','>=', $startdatetime)->orderBy('timestamp', 'DESC')->where('uid','=',$userID)->toSql();
            }
        }

        $data['search_sql'] = $sql;

        $modTopicsObjs = topic::all();

        foreach ($modObj as $key => $obj) {
            foreach ($modTopicsObjs as $topic) {
                if($obj['topic_id'] == $topic->topic_id)
                {
                    $obj['topic_title'] = $topic->topic_title;
                }
            }

            $obj['filename_org'] = $obj['filename'];
            if(!is_null( $obj['filename']))
            {
                $sYear = substr($obj['timestamp'], 0, 4);
                $sMonth = substr($obj['timestamp'], 5, 2);
                $sDay = substr($obj['timestamp'], 8, 2);
                $obj['filename'] = $sYear.'/'.$sMonth.$sDay.'/'.$obj['filename'];
            }
            $obj['timestamp_date'] = substr($obj['timestamp'], 0, 10);
            $obj['timestamp_time'] = substr($obj['timestamp'], 11, 8);

            $obj['start'] = 0;

            $obj['duration_sec'] = $obj['duration'];
            $obj['duration'] = $this->s2h($obj['duration']);

            //check quick tag exists
            if($mTag_use == 1){
                $tagObjCount = tag::where('rec_id','=',$obj['id'])->where('tag_category_id','=',$this->$mTag_id)->count();
                if ($tagObjCount > 0) {
                    $obj['qtag_exist'] = true;
                }
                else {
                    $obj['qtag_exist'] = false;
                }
            }

            //group
            $grpObj = Group::find($obj['group_id']);
            if (!is_null($grpObj)) {
                  $obj['group_name'] = $grpObj->group_name;
            } else {
                $obj['group_name'] = 'No Entry';
            }

            if(isset($obj['download_datetime']))
            {
              $obj['download_datetime_date'] = substr($obj['download_datetime'], 0, 10);
              $obj['download_datetime_time'] = substr($obj['download_datetime'], 11, 8);
            } else {
              $obj['download_datetime_date'] = '-';
              $obj['download_datetime_time'] = '';
            }

            $txtObj = recognition::where('record_id','=',$obj['id'])->orderBy('start_time', 'asc')->get();
            //return $obj['id'];
            if($txtObj->count()>0)
            {
                $obj['voicetext1'] = null;
                $obj['voicetext2'] = null;
                foreach ($txtObj as $txt) {
                    $obj['voicetext1'] .= $txt->text.':'.$txt->direction.' ';
                    $obj['voicetext2'] .= $txt->text.' ';
                }
            }
            else {
                $obj['voicetext1'] = 'No text';
                $obj['voicetext2'] = 'No text';
            }

            $item_ary[$key] = $obj;
        }

        //create pagination
        $paginateResponse = $this->paginateMe($item_ary, 20, $request);

        //return $item_ary;
        $ip[0] = $_SERVER['REMOTE_ADDR'];
        $ip[1] = getenv('SERVER_ADDR');
        $tag_cat = TagCatagory::all();
        $tag_group = Group::all();

        //Session::put('show_details', 0);

        //config values should be load from database
        $config = array(
          "player_value"  => $configuration['player_select'], //should be loaded from a new DB table called config
          "app_type"      => $configuration['youwire_type'], //should be loaded from a new DB table called config => office = 0 / mobile_app = 1/ mobile_phone = 2/ meeting = 3
          "bulk_dl_limit" => $configuration['bulk_dl_limit'], //should be loaded from a new DB table called config
          "customer" => $configuration['customer'], //default = 0, softbank = 1
          "status_count" => 0, //array(0 => 1),
        );

        return view('welcome')->with('old_req',$request)->with('ips', $ip)->with('tag_cat', $tag_cat)->with('tag_group', $tag_group)->with('config',$config)->with('res',$paginateResponse)->with('data',$data)->with('show_details','0');
    }


   /**
   * Paginate a laravel colletion or array of items.
   *
   * @param  array|Illuminate\Support\Collection $items   array to paginate
   * @param  int $perPage number of pages
   * @return Illuminate\Pagination\LengthAwarePaginator    new LengthAwarePaginator instance
   */
    function paginateMe($itemsList, $perPage, $request){
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($itemsList);
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginateItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $paginateItems->setPath($request->url());
        return $paginateItems;
    }

    //COnvert seconds to hours
    private function s2h($seconds) {

      		$hours = floor($seconds / 3600);
      		$minutes = floor(($seconds / 60) % 60);
      		$seconds = $seconds % 60;

      		$hms = sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);

      		return $hms;

	   }

}
