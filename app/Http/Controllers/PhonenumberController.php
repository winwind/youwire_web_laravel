<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\phone_number;
use App\profile_data;

class PhonenumberController extends Controller
{	
	/**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * phone_number table start 2018/09/04
     */
    public function index($id)
    {
        $get_phone =  phone_number::where('uid', $id)
				    ->orderBy('phone_number_id','asc')
				    ->get();

        $get_profiles = user::leftJoin('groups_users_links', 'users.id', '=', 'groups_users_links.uid')
                    ->leftJoin('profile_datas', 'users.id', '=', 'profile_datas.uid')
                    ->leftJoin('security_groups', 'profile_datas.security_group_id', '=', 'security_groups.security_group_id')
                    ->leftJoin('groups', 'groups_users_links.group_id', '=', 'groups.id')
                    ->where('users.id', '=', $id)
                    ->whereIn('profile_datas.user_type', [0, 2])
                    ->select('users.id', 'uname', 'name', 'record_control')
                    ->orderBy('users.id','asc')
                    ->get();

                    foreach ($get_profiles as $get_profile) {
                        switch ($get_profile->record_control){
                            case 0:
                                $get_profile->record_control = '録音制御なし';
                                break;
                            case 1:
                                $get_profile->record_control = '識別番号を録音拒否';
                                break;
                            case 2:
                                $get_profile->record_control = '識別番号を録音許可';
                                break;
                        }
                    }

        return view('Phonenumber.index',[
        	'get_phones' => $get_phone,
            'get_profiles' => $get_profiles,
        ]);
    } 
     /**
     * create phone_number start 20180910
     */
    public function create(Request $request)
    {   
        phone_number::insert([
                        'uid' => $request->uid,
                        'phone_number' => $request->phone_number,
                        'phone_number_name' => $request->phone_number_name,
                        ]);

        return back();
    }
     /**
     * Update phone_number start 201809010
     */
    public function update(Request $request, $id)
    {    
        $get_phone = phone_number::find($id);
        $get_phone->phone_number = $request->phone_number;
        $get_phone->phone_number_name = $request->phone_number_name;
        $get_phone->save();

        $id = $request->uid;

        return redirect()->action('PhonenumberController@index', ['id' => $id]);

    }
    /**
     * Update User by phone_number start 201809012
     */
    public function updateuser(Request $request)
    {    
        profile_data::where('uid', $request->uid)
                        ->update(['record_control' => $request->record_control]);

        $id = $request->uid;

        return redirect()->action('PhonenumberController@index', ['id' => $id]);

    }
     /**
     * Edit phone_number start 20180910
     */
    public function edit($id)
    {
    	$get_phone = phone_number::findOrFail($id);

    	return view('Phonenumber.edit', ['get_phones' => $get_phone]); 
    }
     /**
     * Delete phone_number start 20180910
     */
    public static function destroy($id)
    {
		phone_number::where('phone_number_id', $id)->delete();

        return back();
    }
}
