<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Security_group;
use App\config;

class SecuritygroupController extends Controller
{	
	/**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * securitygroup table start 2018/08/15 end 2018/08/16
     */
    public function index()
    {   
        $bulk_dl_limit = config::where('conf_name', '=', 'bulk_dl_limit')->value('conf_value');

        $get_securitygroups = Security_group::orderBy('security_group_id','asc')->get();
        foreach ($get_securitygroups as $get_securitygroup) {
            //convert operating_range
            switch ($get_securitygroup->operating_range){
                case 1:
                    $get_securitygroup->operating_range = '自分のみ';
                    break;
                case 2:
                    $get_securitygroup->operating_range = 'グループ内のみ';
                    break;
                case 3:
                    $get_securitygroup->operating_range = 'グループ配下';
                    break;
                case 4:
                    $get_securitygroup->operating_range = '全体';
                    break;
                case 5:
                    $get_securitygroup->operating_range = 'なし';
                    break;
            }
                //convert authority items
                $get_securitygroup->authority_search ?
                    $get_securitygroup->authority_search = 'O':
                    $get_securitygroup->authority_search = 'X';
                
                $get_securitygroup->authority_delete ?
                    $get_securitygroup->authority_delete = 'O':
                    $get_securitygroup->authority_delete = 'X';

                $get_securitygroup->authority_download ?
                    $get_securitygroup->authority_download = 'O':
                    $get_securitygroup->authority_download = 'X';

                $get_securitygroup->authority_bulk_download ?
                    $get_securitygroup->authority_bulk_download = 'O':
                    $get_securitygroup->authority_bulk_download = 'X';
                
                $get_securitygroup->authority_protect ?
                    $get_securitygroup->authority_protect = 'O':
                    $get_securitygroup->authority_protect = 'X';
                
                $get_securitygroup->authority_add_tag ?
                    $get_securitygroup->authority_add_tag = 'O':
                    $get_securitygroup->authority_add_tag = 'X';

                $get_securitygroup->authority_edit_tag_category ?
                    $get_securitygroup->authority_edit_tag_category = 'O':
                    $get_securitygroup->authority_edit_tag_category = 'X';

                $get_securitygroup->authority_edit_security_group ?
                    $get_securitygroup->authority_edit_security_group = 'O':
                    $get_securitygroup->authority_edit_security_group = 'X';

                $get_securitygroup->authority_edit_group ?
                    $get_securitygroup->authority_edit_group = 'O':
                    $get_securitygroup->authority_edit_group = 'X';

                $get_securitygroup->authority_edit_user ?
                    $get_securitygroup->authority_edit_user = 'O':
                    $get_securitygroup->authority_edit_user = 'X';

                $get_securitygroup->authority_edit_alertmail ?
                    $get_securitygroup->authority_edit_alertmail = 'O':
                    $get_securitygroup->authority_edit_alertmail = 'X';
        }

        return view('securitygroup.index',[
            'get_securitygroups' => $get_securitygroups,
            'bulk_dl_limit' => $bulk_dl_limit
        ]);
    }
     /**
     * create securitygroup  start 2018/09/03  end 2018/09/03
     */
    public function create(Request $request)
    {
        $create_securitygroup = new Security_group; 
        $create_securitygroup->security_group_name = $request->security_group_name;
        $create_securitygroup->operating_range = $request->operating_range;
        if(! isset( $request->authority_search )) { $request->authority_search = '0'; }
        $create_securitygroup->authority_search = $request->authority_search;

        if(! isset( $request->authority_delete )) { $request->authority_delete = '0'; }
        $create_securitygroup->authority_delete = $request->authority_delete;

        if(! isset( $request->authority_download )) { $request->authority_download = '0'; }
        $create_securitygroup->authority_download = $request->authority_download;

        if(! isset( $request->authority_bulk_download )) { $request->authority_bulk_download = '0'; }
        $create_securitygroup->authority_bulk_download = $request->authority_bulk_download;

        if(! isset( $request->authority_protect )) { $request->authority_protect = '0'; }
        $create_securitygroup->authority_protect = $request->authority_protect;

        if(! isset( $request->authority_add_tag )) { $request->authority_add_tag = '0'; }
        $create_securitygroup->authority_add_tag = $request->authority_add_tag;

        if(! isset( $request->authority_edit_tag_category )) { $request->authority_edit_tag_category = '0'; }
        $create_securitygroup->authority_edit_tag_category = $request->authority_edit_tag_category;

        if(! isset( $request->authority_edit_security_group )) { $request->authority_edit_security_group = '0'; }
        $create_securitygroup->authority_edit_security_group = $request->authority_edit_security_group;

        if(! isset( $request->authority_edit_group )) { $request->authority_edit_group = '0'; }
        $create_securitygroup->authority_edit_group = $request->authority_edit_group;

        if(! isset( $request->authority_edit_user )) { $request->authority_edit_user = '0'; }
        $create_securitygroup->authority_edit_user = $request->authority_edit_user;

        if(! isset( $request->authority_edit_alertmail )) { $request->authority_edit_alertmail = '0'; }
        $create_securitygroup->authority_edit_alertmail = $request->authority_edit_alertmail;
        $create_securitygroup->save();
        return back();
    }
    /**
     * Update securitygroup information start 2018/08/15 end 90%
     */
    public function update(Request $request, $id)
    {   
        $get_securitygroup = Security_group::findOrFail($id); 
        $get_securitygroup->security_group_name = $request->security_group_name;
        $get_securitygroup->operating_range = $request->operating_range;
        if(! isset( $request->authority_search )) { $request->authority_search = '0'; }
        $get_securitygroup->authority_search = $request->authority_search;

        if(! isset( $request->authority_delete )) { $request->authority_delete = '0'; }
        $get_securitygroup->authority_delete = $request->authority_delete;

        if(! isset( $request->authority_download )) { $request->authority_download = '0'; }
        $get_securitygroup->authority_download = $request->authority_download;

        if(! isset( $request->authority_bulk_download )) { $request->authority_bulk_download = '0'; }
        $get_securitygroup->authority_bulk_download = $request->authority_bulk_download;

        if(! isset( $request->authority_protect )) { $request->authority_protect = '0'; }
        $get_securitygroup->authority_protect = $request->authority_protect;

        if(! isset( $request->authority_add_tag )) { $request->authority_add_tag = '0'; }
        $get_securitygroup->authority_add_tag = $request->authority_add_tag;

        if(! isset( $request->authority_edit_tag_category )) { $request->authority_edit_tag_category = '0'; }
        $get_securitygroup->authority_edit_tag_category = $request->authority_edit_tag_category;

        if(! isset( $request->authority_edit_security_group )) { $request->authority_edit_security_group = '0'; }
        $get_securitygroup->authority_edit_security_group = $request->authority_edit_security_group;

        if(! isset( $request->authority_edit_group )) { $request->authority_edit_group = '0'; }
        $get_securitygroup->authority_edit_group = $request->authority_edit_group;

        if(! isset( $request->authority_edit_user )) { $request->authority_edit_user = '0'; }
        $get_securitygroup->authority_edit_user = $request->authority_edit_user;

        if(! isset( $request->authority_edit_alertmail )) { $request->authority_edit_alertmail = '0'; }
        $get_securitygroup->authority_edit_alertmail = $request->authority_edit_alertmail;
        $get_securitygroup->save();

        return redirect()->action('SecuritygroupController@index');
    }
     /**
     * Edit securitygroup start 2018/08/15 end 2018/08/16
     */
    public function edit($id)
    {
        $get_securitygroup = Security_group::findOrFail($id); 
        $bulk_dl_limit = config::where('conf_name', '=', 'bulk_dl_limit')->value('conf_value');

        return view('securitygroup.edit', ['get_securitygroup' => $get_securitygroup,
            'bulk_dl_limit' => $bulk_dl_limit]); 
    }
     /**
     * Delete securitygroup start 2018/08/15 end 2018/08/16
     */
    public static function destroy($id)
    {
        Security_group::find($id)->delete();

        return back();
    }
}
