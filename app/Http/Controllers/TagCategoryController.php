<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TagCatagory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TagCategoryController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth')->except('index');
  }

  public function tagCategoryView()
  {
   $security_group = DB::table('security_groups')
                        ->leftjoin('profile_datas','security_groups.security_group_id', '=', 'profile_datas.security_group_id')
                        ->where('profile_datas.uid','=',auth()->user()->id)
                        ->select('security_groups.*')
                        ->get();
    foreach ($security_group as $sgroup) {
      $data['authority_bulk_download'] = $sgroup->authority_bulk_download;
      $data['authority_delete'] = $sgroup->authority_delete;
      $data['authority_download'] = $sgroup->authority_download;
      $data['authority_protect'] = $sgroup->authority_protect;
      $data['authority_add_tag'] = $sgroup->authority_add_tag;
      $data['authority_edit_tag_category'] = $sgroup->authority_edit_tag_category;
      $data['authority_edit_security_group'] = $sgroup->authority_edit_security_group;
      $data['authority_edit_group'] = $sgroup->authority_edit_group;
      $data['authority_edit_user'] = $sgroup->authority_edit_user;
      $data['authority_edit_alertmail'] = $sgroup->authority_edit_alertmail;
    }


    $obj = TagCatagory::orderBy('weight', 'asc')->orderBy('id', 'asc')->get();


    $ip[0] = $_SERVER['REMOTE_ADDR'];
    $ip[1] = getenv('SERVER_ADDR');

    return view('tagcategory')->with('ips', $ip)->with('tagCatListData',$obj)->with('data',$data);
}

  public function control_add(Request $request)
  {
    return $this->execute_edit(1,$request);
  }

  public function control_update(Request $request)
  {
    return $this->execute_edit(2,$request);
  }

  private function execute_edit($edit_type,$request)
  {
    $tag_category_id = $request->input('tag_category_id');
    $tag_category_name = $request->input('tag_category_name');
    $tag_category_weight = $request->input('tag_category_weight');

    $msg = 0;

    if(trim($tag_category_name) == '')
    {
        $msg = 1; //tag cat name is not entered error
    }
    if(strlen(mb_convert_encoding($tag_category_name,'SJIS', 'UTF-8')) > 32 )
    {
        $msg = 2;
    }
    if(trim($tag_category_weight) != '' && !is_numeric($tag_category_weight))
    {
        $msg = 3; // tag weight is not numeric error
    }

    if($msg == 0)
    {
      if(trim($tag_category_weight == ''))
      {
        $tag_category_weight = TagCatagory::max('weight');
        $tag_category_weight ++;
      }
      $res = null;

      //operation log
      $youwirelog = new youwireLogController();

      switch($edit_type)
      {
        case 1: $youwirelog->operation("タグカテゴリ登録","新規登録");
                $tagcat = new TagCatagory;
                $tagcat->name = $tag_category_name;
                $tagcat->weight = $tag_category_weight;
                $res = $tagcat->save();
                if($res)
                {
                  $msg = "4"; // save success message
                }
                else {
                  $msg = "6";
                }
                break;
        case 2: $youwirelog->operation("タグカテゴリ登録","更新",'tag_category_id = '.$tag_category_id);
                $tagcatedit = TagCatagory::find($tag_category_id);
                $tagcatedit->name = $tag_category_name;
                $tagcatedit->weight = $tag_category_weight;
                $res = $tagcatedit->save();
                if($res)
                {
                  $msg = "5"; // update success message
                }
                else {
                  $msg = "7";
                }
                break;
      }
    }
    Session::put('msg',$msg);
    return redirect()->back();
  }

  public function control_delete(Request $request)
  {
      $tag_category_id = $request->input('tag_category_id');

      // operation log
      $youwirelog = new youwireLogController();
      $youwirelog->operation("タグカテゴリ登録","削除", 'tag_category_id = '.$tag_category_id);

      $deletedTagCat = TagCatagory::where('id','=',$tag_category_id)->delete();
      return redirect()->back();
  }
}
