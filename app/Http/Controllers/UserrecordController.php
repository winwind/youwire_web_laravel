<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\groups_users_link;
use App\profile_data;
use App\config;

class UserrecordController extends Controller
{   
    /**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Userrecord table start 2018/09/06
     */
    public function index()
    {
        $is_user_rec_limit = 0;
        $recordings_strage_size = config::where('conf_name', '=', 'recordings_strage_size')->value('conf_value');
        if ( $recordings_strage_size < 0 ) { $is_user_rec_limit = 1; }

        $get_record =  user::leftJoin('groups_users_links', 'users.id', '=', 'groups_users_links.uid')
                    ->leftJoin('profile_datas', 'users.id', '=', 'profile_datas.uid')
                    ->leftJoin('security_groups', 'profile_datas.security_group_id', '=', 'security_groups.security_group_id')
                    ->leftJoin('groups', 'groups_users_links.group_id', '=', 'groups.id')
                    ->where('profile_datas.user_type', '=', 0)
                    ->orWhere('profile_datas.user_type', '=', 2)
                    ->select('users.id', 'uname', 'name', 'security_group_name', 'group_name')
                    ->orderBy('users.id','asc')
                    ->get();

        return view('userrecord.index',[
            'get_records' => $get_record,
            'is_user_rec_limit' => $is_user_rec_limit
        ]);
    } 
     /**
     * Create Userrecord start 20180907
     */
    public function create(Request $request)
    {   

        if ( strlen(mb_convert_encoding($request->name, 'SJIS', 'UTF-8')) > 60 ) {
            return view('name');//errMsg
        }
        if( preg_match("/[^0-9.]/", $request->uname) ) {
            return view('uname');//errMsg
        }
        if ( isset($request->user_type)) {
            $request->user_type = 2;
            
            if (trim($request->password) <> trim($request->vpassword)) {
                return view('vpassword');//errMsg
            }
            if(!preg_match('/^[\x21-\x7e]+$/', $request->password) ) {
                return view('^[\x21-\x7e]+$/, $request->password');//errMsg
            }
        }
            
        if( $request->is_user_rec_limit === 1 ){
            if ( trim($request->file_limit_size) == '' ) {
                return view('file_limit_size not null');//errMsg
            }    
            if ( $request->file_limit_size > 1000 ) {
                return view('file_limit_size > 1000');//errMsg
                }
            if( preg_match("/[^0-9.]/", $request->file_limit_size) ) {
                return view('"/[^0-9.]/", $request->file_limit_size');//errMsg
            }
        }
    
        $u = new user;
        $u->name = $request->name;
        $u->uname = $request->uname;
        if (! isset( $request->password )) { $request->password = ''; }
        $u->password = $request->password;
        $u->email = $request->uname.'@sample.sample';
        $u->umode = 'nest';
        $u->save();

        if(! isset( $request->file_limit_size)) {$request->file_limit_size = 0;}
        if(! isset( $request->security_group_id )) { $request->security_group_id = 1; }
        if(! isset( $request->user_type )) { $request->user_type = 0; }

        $new_user_id = user::orderBy('id', 'desc')->value('id');
        $p = new profile_data;
        $p->uid = $new_user_id;
        $p->disp_type = 0;
        $p->file_limit_size = $request->file_limit_size * 1000000000;
        $p->security_group_id = $request->security_group_id;
        $p->user_type = $request->user_type;
        $p->save();

        if(! isset( $request->group_id )) { $request->group_id = 0; }
        $g = new groups_users_link;
        $g->group_id = $request->group_id;
        $g->uid = $new_user_id;
        $g->save();
        

        return back();
    }
     /**
     * Update Userrecord start 20180907
     */
    public function update(Request $request, $id)
    {    
        
        if ( strlen(mb_convert_encoding($request->name, 'SJIS', 'UTF-8')) > 60 ) {
            return view('name');//errMsg
        }
        if( preg_match("/[^0-9.]/", $request->uname) ) {
            return view('uname');//errMsg
        }
        if ( isset($request->user_type)) {
            $request->user_type = 2;
            if (trim($request->password) <> trim($request->vpassword)) {
                return view('vpassword');//errMsg
            }
            if(!preg_match('/^[\x21-\x7e]+$/', $request->password) ) {
                return view('^[\x21-\x7e]+$/, $request->password');//errMsg
            }
        }
            
        if( $request->is_user_rec_limit === 1 ){
            if ( trim($request->file_limit_size) == '' ) {
                return view('file_limit_size not null');//errMsg
            }    
            if ( $request->file_limit_size > 1000 ) {
                return view('file_limit_size > 1000');//errMsg
                }
            if( preg_match("/[^0-9.]/", $request->file_limit_size) ) {
                return view('"/[^0-9.]/", $request->file_limit_size');//errMsg
            }
        }
    
        $u = user::find($id);
        $u->name = $request->name;
        $u->uname = $request->uname;
        if (! isset( $request->password )) { $request->password = 0; }
        $u->password = $request->password;
        $u->email = $request->uname.'@sample.sample';
        $u->umode = 'nest';
        $u->save();

        if(! isset( $request->file_limit_size)) {$request->file_limit_size = 0;}
        if(! isset( $request->security_group_id )) { $request->security_group_id = 1; }
        if(! isset( $request->user_type )) { $request->user_type = 0; }

        profile_data::where('uid', $id)
                    ->update(['security_group_id' => $request->security_group_id,
                              'security_group_id' => $request->security_group_id,
                                'user_type' => $request->user_type
                            ]);

        if(! isset( $request->group_id )) { $request->group_id = 0; }
        groups_users_link::where('uid', $id)
                        ->update(['group_id' => $request->group_id]);
        
        return redirect()->action('UserrecordController@index');

    }
     /**
     * Edit Userrecord start 20180907 end 20180907
     */
    public function edit($id)
    {
        $is_user_rec_limit = 0;
        $recordings_strage_size = config::where('conf_name', '=', 'recordings_strage_size')->value('conf_value');
        if ( $recordings_strage_size < 0 ) { $is_user_rec_limit = 1; }

        $get_record = user::findOrFail($id);

        return view('userrecord.edit', ['get_records' => $get_record,
                                        'is_user_rec_limit' => $is_user_rec_limit
        ]); 
    }
     /**
     * Delete userrecord start 20180907 end 20180907
     */
    public static function destroy($id)
    {
        user::find($id)->delete();
        profile_data::where('uid', $id)->delete();
        groups_users_link::where('uid', $id)->delete();

        return back();
    }
    public function bygroupid($id)
    {
        $get_record =  user::leftJoin('groups_users_links', 'users.id', '=', 'groups_users_links.uid')
                    ->leftJoin('profile_datas', 'users.id', '=', 'profile_datas.uid')
                    ->leftJoin('security_groups', 'profile_datas.security_group_id', '=', 'security_groups.security_group_id')
                    ->leftJoin('groups', 'groups_users_links.group_id', '=', 'groups.id')
                    ->where('groups.id', '=', $id)
                    ->whereIn('profile_datas.user_type', [0, 2])
                    ->select('users.id', 'uname', 'name', 'security_group_name', 'group_name')
                    ->orderBy('users.id','asc')
                    ->get();

        return view('userrecord.bygroupid',[
            'get_records' => $get_record
        ]);
    } 
}
