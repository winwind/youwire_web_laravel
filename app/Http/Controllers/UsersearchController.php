<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Group;
use App\User;
use App\groups_users_link;
use App\Security_group;
use App\profile_data;

class UsersearchController extends Controller
{	
	/**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * usersearch table start 2018/09/04
     */
    public function index(Request $request)
    {
        $error = [$request->errors];
    	$id_user = auth()->user()->id;
        $get_users =  user::leftJoin('groups_users_links', 'users.id', '=', 'groups_users_links.uid')
				    ->leftJoin('profile_datas', 'users.id', '=', 'profile_datas.uid')
				    ->leftJoin('security_groups', 'profile_datas.security_group_id', '=', 'security_groups.security_group_id')
				    ->leftJoin('groups', 'groups_users_links.group_id', '=', 'groups.id')
                    ->where('profile_datas.user_type', '=', 1)
                    ->orWhere('profile_datas.user_type', '=', 2)
				    ->select('users.id', 'uname', 'name', 'security_group_name', 'group_name')
				    ->orderBy('users.id','asc')
				    ->get();

        return view('usersearch.index',[
        	'get_users' => $get_users,
            'errors' => $error
        ]);
    } 
     /**
     * create usersearch start 20180905
     */
    public function create(Request $request)
    {   
        if(! isset( $request->name )) {  
            $error = '$request->name'; 
            return redirect()->action('UsersearchController@index', ['errors' => $error]);
        }

        if(! isset( $request->uname )) {
            $error = '$request->uname';
            return redirect()->action('UsersearchController@index', ['errors' => $error]);
        }
        if(! isset( $request->password )) {
            $error = '$request->password';
            return redirect()->action('UsersearchController@index', ['errors' => $error]);
        }
        if ( strlen(mb_convert_encoding($request->name, 'SJIS', 'UTF-8')) > 60 ) {
            $error = '$request->name';
            return redirect()->action('UsersearchController@index', ['errors' => $error]);
        }
        if( preg_match("/[^a-zA-Z0-9\_\-]/", $request->uname) ) {
            $error = '$request->uname';
            return redirect()->action('UsersearchController@index', ['errors' => $error]);
        }
    	if (trim($request->password) <> trim($request->vpassword)) {
            $error = 'trim($request->vpassword';
    		return redirect()->action('UsersearchController@index', ['errors' => $error]);
    	}
        if( strlen($request->password) > 0 && !preg_match('/^[\x21-\x7e]+$/', $request->password) ) {
            $error = '!preg_match$request->password';
            return redirect()->action('UsersearchController@index', ['errors' => $error]);
        }
        $u = new user;
        $u->name = $request->name;
        $u->uname = $request->uname;
        $u->password = $request->password;
        $u->email = $request->uname.'@sample.sample';
        $u->umode = 'nest';
        $u->save();
                        
        $new_user_id = user::orderBy('id', 'desc')->value('id');
        
        $p = new profile_data;
        $p->uid = $request->new_user_id;
        $p->file_limit_size = 1200000000;
        $p->disp_type = 0;
        $p->security_group_id = $request->security_group_id;
        $p->user_type = 1;
        $p->save();

        if(! isset( $request->group_id )) { $request->group_id = '0'; }

        $g = new groups_users_link;
        $g->group_id = $request->group_id;
        $g->uid = $new_user_id;
        $g->save();

        return back();
    }
     /**
     * Update usersearch start 20180905
     */
    public function update(Request $request, $id)
    {    
        if ( strlen(mb_convert_encoding($request->name, 'SJIS', 'UTF-8')) > 60 ) {
            return view('errMsg');//errMsg
        }
        if( preg_match("/[^a-zA-Z0-9\_\-]/", $request->uname) ) {
            return view('errMsg');//errMsg
        }
        if (trim($request->password) <> trim($request->vpassword)) {
            return view('$request->vpassword');//errMsg
        }
        if( strlen($request->password) > 0 && !preg_match('/^[\x21-\x7e]+$/', $request->password) ) {
            return view('errMsg');//errMsg
        }
    	$update_users = user::find($id);
    	$update_users->name = $request->name;
        $update_users->uname = $request->uname;
        $update_users->password = $request->password; 
        $update_users->save();
        
        profile_data::where('uid', $id)
        			->update(['security_group_id' => $request->security_group_id]);

    	if (! isset($request->group_id)) { $request->group_id = '0'; }

        groups_users_link::where('uid', $id)
        				->update(['group_id' => $request->group_id]);

        return redirect()->action('UsersearchController@index');

    }
     /**
     * Edit usersearch start 20180905
     */
    public function edit($id)
    {
    	$get_users = user::findOrFail($id);

    	return view('usersearch.edit', ['get_users' => $get_users]); 
    }
     /**
     * Delete usersearch start 20180905
     */
    public static function destroy($id)
    {
    	user::find($id)->delete();
		profile_data::where('uid', $id)->delete();
		groups_users_link::where('uid', $id)->delete();

        return back();
    }
    public function bygroupid($id)
    {
        $get_users =  user::leftJoin('groups_users_links', 'users.id', '=', 'groups_users_links.uid')
                    ->leftJoin('profile_datas', 'users.id', '=', 'profile_datas.uid')
                    ->leftJoin('security_groups', 'profile_datas.security_group_id', '=', 'security_groups.security_group_id')
                    ->leftJoin('groups', 'groups_users_links.group_id', '=', 'groups.id')
                    ->where('groups.id', '=', $id)
                    ->whereIn('profile_datas.user_type', [1, 2])
                    ->select('users.id', 'uname', 'name', 'security_group_name', 'group_name')
                    ->orderBy('users.id','asc')
                    ->get();

        return view('usersearch.bygroupid',[
            'get_users' => $get_users
        ]);
    } 
}
