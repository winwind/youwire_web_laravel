<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Group;
use App\User;
use App\groups_users_link;
use App\Security_group;
use App\profile_data;

class UsersettingController extends Controller
{	
	/**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Usersetting table start 2018/09/18
     */
    public function index()
    {
        $get_users =  user::leftJoin('profile_datas', 'users.id', '=', 'profile_datas.uid')
                    ->where('profile_datas.user_type', '=', 1)
                    ->orWhere('profile_datas.user_type', '=', 2)
                    ->select('users.id', 'uname', 'name', 'users.created_at','last_login', 'file_limit_size')
                    ->orderBy('users.id','asc')
                    ->get();
        return view('usersetting.index',[ 'get_users' => $get_users
        ]);
    } 
    public function create(Request $request)
    {
        if ( strlen(mb_convert_encoding($request->name, 'SJIS', 'UTF-8')) > 60 ) {
            return view('errMsg');//errMsg
        }
        if( preg_match("/[^a-zA-Z0-9\_\-]/", $request->uname) ) {
            return view('errMsg');//errMsg
        }
        if (trim($request->password) <> trim($request->vpassword)) {
            return view('$request->vpassword');//errMsg
        }
        if( strlen($request->password) > 0 && !preg_match('/^[\x21-\x7e]+$/', $request->password) ) {
            return view('errMsg');//errMsg
        }
        $u = new user;
        $u->name = $request->name;
        $u->uname = $request->uname;
        $u->password = $request->password;
        $u->email = $request->email;
        $u->umode = 'nest';
        $u->save();

        $new_user_id = user::orderBy('id', 'desc')->value('id');
        
        $p = new profile_data;
        $p->uid = $new_user_id;
        $p->file_limit_size = $request->file_limit_size;
        $p->disp_type = 0;
        $p->security_group_id = 0;
        $p->user_type = 1;
        $p->save();

        $g = new groups_users_link;
        $g->group_id = $request->group_id;
        $g->uid = $new_user_id;
        $g->save();

        return back();

    }
    public function edit($id)
    {
        $get_users = user::findOrFail($id);

        return view('usersetting.edit', ['get_users' => $get_users]); 
    }

    public function update(Request $request, $id)
    {    
        if ( strlen(mb_convert_encoding($request->name, 'SJIS', 'UTF-8')) > 60 ) {
            return view('errMsg');//errMsg
        }
        if( preg_match("/[^a-zA-Z0-9\_\-]/", $request->uname) ) {
            return view('errMsg');//errMsg
        }
        if (trim($request->password) <> trim($request->vpassword)) {
            return view('$request->vpassword');//errMsg
        }
        if( strlen($request->password) > 0 && !preg_match('/^[\x21-\x7e]+$/', $request->password) ) {
            return view('errMsg');//errMsg
        }
        $update_users = user::find($id);
        $update_users->name = $request->name;
        $update_users->uname = $request->uname;
        $update_users->password = $request->password; 
        $update_users->email = $request->email;
        $update_users->save();
        
        profile_data::where('uid', $id)
                    ->update(['file_limit_size' => $request->file_limit_size]);

        groups_users_link::where('uid', $id)
                        ->update(['group_id' => $request->group_id]);

        return redirect()->action('UsersettingController@index');

    }
    public static function destroy($id)
    {
        user::find($id)->delete();
        profile_data::where('uid', $id)->delete();
        groups_users_link::where('uid', $id)->delete();

        return back();
    }
}
