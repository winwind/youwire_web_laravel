<?php

namespace App\Http\Controllers\util;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\groups_users_link;
use App\youwire_default;
use App\config;
use App\profile_data;

class HelperController extends Controller
{
  public function testSelf()
  {
      //return $this->isRecord('geekfeed','geekfeed');
      $recParam = array(
  			'direction'=>1,
  			'duration'=>8.8,
  			'filename'=>'converted_file_name',
  			'localparty'=>'local_number',
  			'remoteparty'=>'remote_number',
  			'timestamp'=>'2018-09-10 19:37:00',
  			'latitude'=>0,
  			'longitude'=>0,
  			'location'=>'',
  			'topic_id'=>0,
  			'upload_file_size'=>100,
  			'revision'=>'AAA'
		  );
      return $this->addNew($recParam,'geekfeed');
  }

  public function isRecord($localparty, $remoteparty)
  {
    $result_code = 1;
    $user_info = null;

    //Confirmation of the recording number
    $sql = "select distinct u.*,p.* from users as u left join profile_datas as p on u.id=p.uid where u.uname = '".$localparty."';";
    $modObjLocal = DB::selectOne($sql);

    $sql = "select distinct u.*,p.* from users as u left join profile_datas as p on u.id=p.uid where u.uname = '".$remoteparty."';";
    $modObjRemote = DB::selectOne($sql);

    if(!empty($modObjLocal))
    {
      $result_code = 1;
      $user_info = $modObjLocal;
    }
    elseif (!empty($modObjRemote))
    {
      $result_code = 2;
      $tmp = $localparty;
			$localparty = $remoteparty;
			$remoteparty = $tmp;
      $user_info = $modObjRemote;
    }
    else
    {
      return 0;
    }

    if($user_info->record_control == 1 || $user_info->record_control == 2)
    {
      $sql = "select p.*,n.* from phone_numbers as n inner join profile_datas as p on n.uid = p.uid where n.uid = ".$user_info->id." and n.phone_number = '".$remoteparty."';";
      $modObj = DB::selectOne($sql);

      if(!empty($modObj))
      {
        if(($modObj->record_control == 1 || $modObj->record_control == 2))
        {
          $result_code = 0;
        }
      }
    }

    return $result_code;
  }

  public function addNew($recParam,$uname = null)
  {
    if(empty($uname))
    {
      $uid = 1;
    }
    else
    {
      $uid = User::select('id')->where('uname','=',$uname)->first();
      $uid = $uid->id;
    }

    $group_id = groups_users_link::select('group_id')->where('uid','=',$uid)->first();
    $group_id = $group_id->group_id;

    $newObj = new youwire_default;
    $newObj->uid = $uid;
    $newObj->group_id = $group_id;

    foreach ($recParam as $key => $val) {
      $newObj->$key = $val;
    }

    $res = $newObj->save();
    $result = 'false';
    if($res)
    {
        $result = 'true';
        // ファイル保管容量(Byte)
		    $file_limit_size = 0;
        //this variable is used in deleteRecordingFile() method, to identify the methos of calculating used memory space of the stored recording files.
    		$select_option;
        $confObj = config::whereIn('conf_name',['recordings_strage_size','file_path'])->get();
        $mRecordingsStrageSize = $confObj[1]->conf_value;
        $mFilePath = $confObj[0]->conf_value;
    		if ( $mRecordingsStrageSize > 0 ) {
    			// 0より大きい場合、指定容量を制限値とする
    			$file_limit_size = $mRecordingsStrageSize * 1073741824;	// 入力値はGByte指定なのでByteに変換
    			$select_option = 0;
    		} elseif ( $mRecordingsStrageSize == 0 ) {
    			// 0の場合、ストレージの95%を制限値とする
    			$file_limit_size = disk_total_space($mFilePath) * 0.95;
    			$select_option = 1;
    		} else {
    			// 0より小さい場合、ユーザープロフィールの制限値とする
          $profDataObj = profile_data::select('file_limit_size')->where('uid','=',$uid)->first();
          $file_limit_size = $profDataObj->file_limit_size;
    			$select_option = 0;
    		}

        // 録音情報削除処理の実行
    		$this->deleteRecordingFile($file_limit_size, $mRecordingsStrageSize, $uid, $select_option, $mFilePath);
    }
    return $result;
  }

  private function deleteRecordingFile($file_limit_size, $type, $uid, $select_option, $filePath)
  {
    // アップロード済みの録音容量を取得
    $upload_file_size_sum = 0;

    if($select_option == 0)
    {
        $sql_sum = "select sum(upload_file_size) as upload_file_size_sum from defaults";
        if($type < 0)
        {
          $sql_sum .= " where uid =".$uid." group by uid";
        }
        $sql_sum .= ";";
        $sumObj = DB::select($sql_sum);
        $upload_file_size_sum = $sumObj[0]->upload_file_size_sum;
    }
    else
    {
        exec("df -Pk ".$filePath."/ | awk '{print $3}'",$output);
        $upload_file_size_sum = $output[1]*1024; //Output size in KB => it is changed to Byte
    }

    if($file_limit_size <= $upload_file_size_sum)
    {
        // 保存容量超過の場合、削除処理実行
        if($type < 0)
        {
          $row_del_target = youwire_default::where('protect','=',0)->where('uid','=',$uid)->orderBy('timestamp', 'asc')->limit(1)->first();
        }
        else
        {
          $row_del_target = youwire_default::where('protect','=',0)->orderBy('timestamp', 'asc')->limit(1)->first();
        }

        $del_id = $row_del_target->id;
        $del_localparty = $row_del_target->localparty;
        $del_timestamp = $row_del_target->timestamp;
        $del_Year = substr( $del_timestamp, 0, 4);
        $del_Month = substr( $del_timestamp, 5, 2);
        $del_Day = substr( $del_timestamp, 8, 2);
        $del_filename = $row_del_target->filename;

        // DB上の録音情報の削除
        $del_res = youwire_default::where('id','=',$del_id)->delete();
        if($del_res)
        {
            // 実ファイルの削除
			      unlink($filePath . '/'. $del_localparty .'/' . $del_Year . '/' . $del_Month . $del_Day . '/' .  $del_filename);

            $this->deleteRecordingFile($file_limit_size, $mRecordingsStrageSize, $uid, $select_option, $filePath);
        }
    }
    else
    {
      return;
    }
  }
}
