<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\youwire_default;
use App\operationlog;
use Session;

class youwireLogController extends Controller
{
  public function operation($display="",$action="",$operation_info="")
  {
    if((auth()->user()->id) >= 1)
    {

        $operationLog = new operationlog();
        $operationLog->uid = auth()->user()->id;
        $operationLog->access_url = getenv('SERVER_ADDR');
        $operationLog->display = $display;
        $operationLog->action = $action;
        $operationLog->operation_info = $operation_info;
        $operationLog->created_at = date('Y-m-d H:i:s');
        $operationLog->save();
    }
  }
}
