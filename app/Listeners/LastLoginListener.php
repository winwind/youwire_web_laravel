<?php

namespace App\Listeners;

use App\Events\Logined;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\User;

class LastLoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logined  $event
     * @return void
     */
    public function handle(Logined $event)
    {
        $user = Auth::user();

        //get Last login before save new login date
        $user_temp = User::where('id', '=', $user->id)->get();
        foreach ($user_temp as $u) {
          Session::put('last_login', $u->last_login);
        }

        //Store the new login date and time in user->last_login
        $user->last_login = Carbon::Now();
        $user->save();
    }
}
