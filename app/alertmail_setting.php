<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alertmail_setting extends Model
{
    public $timestamps = false;

    /*THis method is in the laravel Model.php.
    and it is overidden to have the abiltiy of composit primary key*/
    /*protected function setKeysForSaveQuery(Builder $query)
    {
        $query
            ->where('id', '=', $this->getAttribute('id'))
            ->where('category_id', '=', $this->getAttribute('category_id'))
            ->where('sub_category_id', '=', $this->getAttribute('sub_category_id'));
        return $query;
    }*/
}
