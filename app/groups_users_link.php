<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class groups_users_link extends Model
{
    protected $primaryKey = 'link_id';
    public $timestamp = false;
    const  CREATED_AT = null;
	const  UPDATED_AT = null;

}
