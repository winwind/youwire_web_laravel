<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile_data extends Model
{
  protected $primaryKey = 'uid';
  public $timestamp = false;
  const  CREATED_AT = null;
  const  UPDATED_AT = null;
  /*public $timestamps = false;*/

}
