<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class security_group extends Model
{
  protected $primaryKey = 'security_group_id';
  public $timestamp = false;
  const  CREATED_AT = null;
  const  UPDATED_AT = null;
/*public $timestamps = false;*/
}
