<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class topic extends Model
{
  protected $primaryKey = 'topic_id';
  public $timestamps = false;
}
