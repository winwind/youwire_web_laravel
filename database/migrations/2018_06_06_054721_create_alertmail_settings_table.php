<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertmailSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alertmail_settings', function (Blueprint $table) {
            /*$table->increments('id');
            $table->timestamps();*/
            $table->mediumInteger('id')->default(null);
            $table->smallInteger('category_id');
            $table->smallInteger('sub_category_id');
            $table->string('value');

            $table->primary(['id','category_id','sub_category_id']);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alertmail_settings');
    }
}
