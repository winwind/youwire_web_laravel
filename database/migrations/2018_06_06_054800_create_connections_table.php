<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->increments('id')->dafault(null);
            $table->string('uuid')->dafault('null');
            $table->integer('timestamp')->dafault(null);
            $table->string('private_ip')->dafault('null');
            $table->integer('start_time')->dafault(null);
            $table->string('description')->nullable()->dafault('null');
            $table->tinyInteger('mailflg')->dafault(0);
            $table->string('mail_address')->nullable()->dafault('null');
            $table->integer('last_mail_alert')->dafault(null);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connections');
    }
}
