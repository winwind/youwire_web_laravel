<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('defaults', function (Blueprint $table) {
            $table->increments('id')->default(null);
            $table->unsignedInteger('direction')->default(0);
            $table->bigInteger('duration')->default(0);
            $table->string('filename')->nullable()->default('null');
            $table->string('localparty')->nullable()->default('null');
            $table->string('remoteparty')->nullable()->default('null');
            $table->unsignedInteger('uid')->default(0);
            $table->dateTime('timestamp')->nullable()->default(null);
            $table->double('latitude',10,8)->nullable()->default(0.00000000);
            $table->double('longitude',10,7)->nullable()->default(0.0000000);
            $table->string('location')->nullable()->default('null');
            $table->unsignedInteger('uTime')->default(0);
            $table->unsignedSmallInteger('topic_id')->default(0);
            $table->integer('upload_file_size')->default(0);
            $table->tinyInteger('protect')->default(0);
            $table->unsignedInteger('group_id')->default(0);
            $table->unsignedInteger('localip')->default(0);
            $table->unsignedInteger('remoteip')->default(0);
            $table->string('portname',10)->nullable()->default('null');
            $table->unsignedInteger('service_id')->default(0);
            $table->string('agent_id',30)->nullable()->default('null');
            $table->string('nativecall_id',30)->nullable()->default('null');
            $table->string('revision',4)->nullable()->default('null');
            $table->dateTime('download_datetime')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('defaults');
    }
}
