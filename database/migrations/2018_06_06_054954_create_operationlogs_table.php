<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operationlogs', function (Blueprint $table) {
            $table->increments('log_id')->default(null);
            $table->mediumInteger('uid')->default(null);
            $table->string('access_url')->default(null);
            $table->string('display')->default(null);
            $table->string('action')->nullable()->default(null);
            $table->string('operation_info')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operationlogs');
    }
}
