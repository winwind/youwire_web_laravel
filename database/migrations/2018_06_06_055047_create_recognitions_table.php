<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecognitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recognitions', function (Blueprint $table) {
            $table->increments('id')->default(null);
            $table->integer('record_id')->default(null);
            $table->float('start_time')->default(null);
            $table->float('end_id')->default(null);
            $table->tinyInteger('direction')->nullable()->default(null);
            $table->string('text')->nullable()->default('null');
            $table->tinyInteger('engine')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recognitions');
    }
}
