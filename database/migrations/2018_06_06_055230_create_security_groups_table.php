<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecurityGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('security_groups', function (Blueprint $table) {
            $table->increments('security_group_id')->default(null);
            $table->string('security_group_name')->nullable()->default('null');
            $table->tinyInteger('operating_range')->default(0);
            $table->tinyInteger('authority_search')->default(0);
            $table->tinyInteger('authority_delete')->default(0);
            $table->tinyInteger('authority_download')->default(0);
            $table->tinyInteger('authority_bulk_download')->default(0);
            $table->tinyInteger('authority_protect')->default(0);
            $table->tinyInteger('authority_add_tag')->default(0);
            $table->tinyInteger('authority_edit_tag_category')->default(0);
            $table->tinyInteger('authority_edit_security_group')->default(0);
            $table->tinyInteger('authority_edit_group')->default(0);
            $table->tinyInteger('authority_edit_user')->default(0);
            $table->tinyInteger('authority_edit_alertmail')->default(0);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('security_groups');
    }
}
