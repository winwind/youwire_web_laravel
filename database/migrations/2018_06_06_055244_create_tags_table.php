<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('tag_id')->default(null);
            $table->unsignedInteger('rec_id')->default(0);
            $table->unsignedInteger('tag_category_id')->default(0);
            $table->string('tag_title')->nullable()->default('null');
            $table->string('tag_text')->nullable()->default('null');
            $table->unsignedInteger('tag_create_uid')->default(0);
            $table->dateTime('tag_create_date')->nullable()->default(null);
            $table->unsignedInteger('tag_update_uid')->default(0);
            $table->dateTime('tag_update_date')->nullable()->default(null);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
