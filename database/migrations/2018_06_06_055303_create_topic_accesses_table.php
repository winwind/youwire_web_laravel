<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topic_accesses', function (Blueprint $table) {
            $table->increments('topic_id');
            $table->mediumInteger('uid')->nullable()->default(null);
            $table->smallInteger('groupid')->nullable()->default(null);
            $table->tinyInteger('can_read')->default(0);
            $table->tinyInteger('can_post')->default(0);
            $table->tinyInteger('can_edit')->default(0);
            $table->tinyInteger('can_delete')->default(0);
            $table->tinyInteger('post_auto_approved')->default(0);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topic_accesses');
    }
}
