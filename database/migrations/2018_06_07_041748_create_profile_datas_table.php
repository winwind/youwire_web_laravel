<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_datas', function (Blueprint $table) {
            $table->increments('uid')->default(null);
            $table->bigInteger('file_limit_size')->default(null);
            $table->string('use_id')->default('null');
            $table->unsignedTinyInteger('disp_type')->default(null);
            $table->integer('security_group_id')->default(null);
            $table->tinyInteger('user_type')->default(null);
            $table->tinyInteger('record_control')->default(0);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_datas');
    }
}
