<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('conf_id');
            $table->unsignedSmallInteger('conf_modid')->default(0);
            $table->unsignedSmallInteger('conf_catid')->default(0);
            $table->string('conf_name');
            $table->string('conf_title');
            $table->string('conf_value');
            $table->string('conf_desc');
            $table->string('conf_formtype');
            $table->string('conf_valuetype');
            $table->unsignedSmallInteger('conf_order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
