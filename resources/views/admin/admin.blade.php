
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel Admin</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
      <div class="container">
        <div class="panel panel-primary">
          <div class="panel-heading">{!! trans('app.admintitle') !!}</div>
          <div class="panel-body">
            <form action="/updatePref" name="admin_frm" method="post">
              {{ csrf_field() }}
            <table class="table" style="text-align:left">
              <tr>
                <th colspan="2">
                  {!! trans('app.pref') !!}
                </th>
              </tr>
              <tr>
                <td width="70%">
                  再生プレーヤー
                  <p>再生に使用するプレーヤーを選択します。</p>
                </td>
                <td width="30%">
                  <select name="player_select">
                    <option value="0" @if (isset($configs)) {{$configs['player_select']=='0'?"selected":""}} @endif>jwplayer</option>
                    <option value="1" @if (isset($configs)) {{$configs['player_select']=='1'?"selected":""}} @endif>WMplayer</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  録音ファイルパス
                  <p>録音ファイルのパスを指定します。</p>
                </td>
                <td>
                  <input type="text" name="file_path" value="@if (isset($configs)) {{$configs['file_path']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  録音ファイルデフォルトカテゴリ
                  <p>録音ファイルの属するカテゴリの既定値を指定します。録音ファイル登録時にカテゴリが取得できなかった場合に使用されます。
                    ※再生プレーヤーが「wmp」の場合のみ有効</p>
                </td>
                <td>
                  <input type="text" name="topic_id_default" value="@if (isset($configs)) {{$configs['topic_id_default']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  クイックタグを有効にする
                  <p>クイックタグを使用する場合、はいを指定します。</p>
                </td>
                <td>
                  <input type="radio" name="quicktag" value="0" @if (isset($configs)) {{$configs['quicktag']=="0"?"checked":""}} @endif/>はい
                  <input type="radio" name="quicktag" value="1" @if (isset($configs)) {{$configs['quicktag']=="1"?"checked":""}} @endif/>いいえ
                </td>
              </tr>
              <tr>
                <td>
                  クイックタグカテゴリＩＤ
                  <p>クイックタグを使用する場合、はいを指定します。</p>
                </td>
                <td>
                  <input type="text" name="quicktag_id" value="@if (isset($configs)) {{$configs['quicktag_id']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  システム運用ユーザー
                  <p>システム運用に使用するユーザーを設定します。</p>
                </td>
                <td>
                  <input type="text" name="system_user" value="@if (isset($configs)) {{$configs['system_user']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  システム運用パスワード
                  <p>システム運用に使用するパスワードを設定します。</p>
                </td>
                <td>
                  <input type="text" name="system_password" value="@if (isset($configs)) {{$configs['system_password']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  YouWire設定
                  <p>YouWireの種類を設定する。</p>
                </td>
                <td>
                  <select name="youwire_type">
                    <option value="0" @if (isset($configs)) {{$configs['youwire_type']=='0'?"selected":""}} @endif>Office</option>
                    <option value="1" @if (isset($configs)) {{$configs['youwire_type']=='1'?"selected":""}} @endif>Mobile App</option>
                    <option value="2" @if (isset($configs)) {{$configs['youwire_type']=='2'?"selected":""}} @endif>Mobile Phone</option>
                    <option value="3" @if (isset($configs)) {{$configs['youwire_type']=='3'?"selected":""}} @endif>Meeting</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>
                  一括ダウンロード最大容量指定(時間:s)
                  <p>一括ダウンロードの最大容量を指定します。指定は通話時間で秒(s)で指定します。
                    0を指定すると一括ダウンロード無効(画面非表示)となります。</p>
                </td>
                <td>
                  <input type="text" name="bulk_dl_limit" value="@if (isset($configs)) {{$configs['bulk_dl_limit']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  録音ファイル保管容量
                  <p>録音ファイル保管容量を設定する。容量を超えると古い通話情報から削除する。単位はGByte・0より大きい場合：指定容量で削除
                    ・0指定：ストレージの95%で削除
                    ・0未満：個々のユーザー単位指定で削除</p>
                </td>
                <td>
                  <input type="text" name="recordings_strage_size" value="@if (isset($configs)) {{$configs['recordings_strage_size']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  アプライアンスネットワーク切断時メール頻度
                  <p>アプライアンスのネットワーク切断時のメール送信頻度を設定します。指定は送信間隔で分(m)で指定します。
                    0を指定すると録音サーバー一覧画面を非表示にします。</p>
                </td>
                <td>
                  <input type="text" name="is_appliance" value="@if (isset($configs)) {{$configs['is_appliance']}} @endif"/>
                </td>
              </tr>
              <tr>
                <td>
                  顧客別対応
                  <p>YouWireの顧客別のカスタマイズ内容の適用</p>
                </td>
                <td>
                  <select name="is_appliance">
                    <option value="0" @if (isset($configs)) {{$configs['is_appliance']=='0'?"selected":""}} @endif>Default</option>
                    <option value="1" @if (isset($configs)) {{$configs['is_appliance']=='1'?"selected":""}} @endif>Softbank</option>
                  </select>
                </td>
              </tr>
            </form>
            </table>
            <div style="text-align:center">
              <input type="submit" class="btn btn-primary" value="送信" />　
              <a href="/welcome" class="btn btn-primary"> TOP </a>
            </div>
          </div>
        </div>
      </div>
    </body>
</html>
