<!DOCTYPE html>
@extends('layouts.header')

@section('ip')
    @foreach ($ips as $ip)
      {{$ip}}
    @endforeach
@endsection

@section('head')
    <meta charset="UTF-8">
    <title>Alert Mail</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
   <link type="text/css" href="css/app.css" rel="stylesheet"/>
   <!-- end of global css -->
   <!-- page level css -->
   <link rel="stylesheet" href="stylesheet.css">
   <link href="vendors/prettycheckable/css/prettyCheckable.css" rel="stylesheet" type="text/css"/>
   <link href="vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" type="text/css" href="css/custom.css">
   <link rel="stylesheet" type="text/css" href="css/form_layouts.css">
   <!--select css-->
    <!--link href="vendors/select2/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="vendors/select2/css/select2-bootstrap.css"/-->
   <!--checkbox&radio-->
    <link href="vendors/prettycheckable/css/prettyCheckable.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="css/custom_css/radio_checkbox.css">
    <link rel="stylesheet" type="text/css" href="vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css">
   <!-- end of page level css -->
 @endsection

   <!--ヘッダーここまで-->
@section('contents')
          <section id="main-content">
              <div class="col-12">
                 <div class="card border-primary">
                    <div class="card-header text-white bg-primary">
                      <h3 class="card-title d-inline"  style="position:relative;">{!! trans('app.alertmailtitle') !!}</h3>
                      <button type="button" id="btn_update" name="btn_update" class="btn btn-primary pull-right d-sm-block submit-group" style="position:absolute; top:3px; right:5px;">
                        <i class="fa fa-fw fa-refresh"></i>{!! trans('app.edit') !!}
                      </button>

                     </div>
                       <div class="card-body">
                         @if(Session::has('msg'))
                           <div style="color:green">{{Session::pull('msg')}}</div>
                         @endif
                         @if(Session::has('err'))
                           <div style="color:red">{{Session::pull('err')}}</div>
                         @endif
                          <form class="col-12" id="list" name="list">
                            {{ csrf_field() }}
                            <div class="form-group row col-12">
                               <label class="col-3 text-left">{!! trans('app.alertmailsend') !!}</label>
                               <div class="py-1">
                                 <div class="form-check abc-radio form-check-inline">
                                   <input class="form-check-input" type="radio" name="alertmail_send" id="alertmail_send_on" value="1" @if (isset($old_req)) {{$old_req->alertmail_send=="1"?"checked":""}} @endif>
                                   <label>{!! trans('app.alertmailon') !!}</label>
                                 </div>
                               </div>
                               <div class="py-1">
                                 <div class="form-check abc-radio form-check-inline">
                                   <input class="form-check-input" type="radio" name="alertmail_send" id="alertmail_send_off" value="0" @if (isset($old_req)) {{$old_req->alertmail_send=="0"?"checked":""}} @endif>
                                   <label>{!! trans('app.alertmailoff') !!}</label>
                                 </div>
                               </div>
                            </div>
                           <div class="form-group row col-12">
                              <label class="col-3 text-left">{!! trans('app.alertmaillimit') !!}</label>
                                <div class="col-9 row">
                                  <div class="py-1">
                                    <div class="form-check abc-radio form-check-inline">
                                      <input class="form-check-input" type="radio" name="alertmail_send_limit" id="alertmail_send_limit_50" value="50" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="50"?"checked":""}} @endif>
                                      <label>50%</label>
                                    </div>
                                  </div>
                                  <div class="py-1">
                                    <div class="form-check abc-radio form-check-inline">
                                      <input class="form-check-input" type="radio" name="alertmail_send_limit" id="alertmail_send_limit_60" value="60" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="60"?"checked":""}} @endif>
                                      <label>60%</label>
                                    </div>
                                  </div>
                                  <div class="py-1">
                                    <div class="form-check abc-radio form-check-inline">
                                      <input class="form-check-input" type="radio" name="alertmail_send_limit" id="alertmail_send_limit_70" value="70" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="70"?"checked":""}} @endif>
                                      <label>70%</label>
                                    </div>
                                  </div>
                                  <div class="py-1">
                                    <div class="form-check abc-radio form-check-inline">
                                      <input class="form-check-input" type="radio" name="alertmail_send_limit" id="alertmail_send_limit_80" value="80" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="80"?"checked":""}} @endif>
                                      <label>80%</label>
                                    </div>
                                  </div>
                                  <div class="py-1">
                                    <div class="form-check abc-radio form-check-inline">
                                      <input class="form-check-input" type="radio" name="alertmail_send_limit" id="alertmail_send_limit_90" value="90" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="90"?"checked":""}} @endif>
                                      <label>90%</label>
                                    </div>
                                  </div>
                              </div>
                            </div>
                           <div class="form-group row col-12">
                               <label for="inputEmail3" class="col-3 text-left">
                                 {!! trans('app.alertmail') !!}</label>
                               <textarea rows="4" class="col-6" name="alertmail_send_mail" id="alertmail_send_mail"></textarea>
                           </div>
                           <div class="form-group row col-12">
                              <label class="col-3 text-left">{!! trans('app.alertday') !!}</label>
                              <div class="col-9 row">
                                 <div class="py-1">
                                   <div class="form-check abc-radio form-check-inline">
                                      <input class="form-check-input" type="radio" name="alertmail_send_day" id="alertmail_send_day_1" value="1" @if (isset($old_req)) {{$old_req->alertmail_send_day=="1"?"checked":""}} @endif>
                                      <label>{!! trans('app.mon') !!}</label>
                                   </div>
                                 </div>
                                 <div class="py-1">
                                   <div class="form-check abc-radio form-check-inline">
                                      <input type="radio" name="alertmail_send_day" id="alertmail_send_day_2" value="2" @if (isset($old_req)) {{$old_req->alertmail_send_day=="2"?"checked":""}} @endif>
                                      <label>{!! trans('app.tue') !!}</label>
                                    </div>
                                  </div>
                                 <div class="py-1">
                                   <div class="form-check abc-radio form-check-inline">
                                      <input type="radio" name="alertmail_send_day" id="alertmail_send_day_3" value="3" @if (isset($old_req)) {{$old_req->alertmail_send_day=="3"?"checked":""}} @endif>
                                      <label>{!! trans('app.wed') !!}</label>
                                    </div>
                                  </div>
                                 <div class="py-1">
                                   <div class="form-check abc-radio form-check-inline">
                                      <input type="radio" name="alertmail_send_day" id="alertmail_send_day_4" value="4" @if (isset($old_req)) {{$old_req->alertmail_send_day=="4"?"checked":""}} @endif>
                                      <label>{!! trans('app.thu') !!}</label>
                                    </div>
                                  </div>
                                 <div class="py-1">
                                   <div class="form-check abc-radio form-check-inline">
                                      <input type="radio" name="alertmail_send_day" id="alertmail_send_day_5" value="5" @if (isset($old_req)) {{$old_req->alertmail_send_day=="5"?"checked":""}} @endif>
                                      <label>{!! trans('app.fri') !!}</label>
                                    </div>
                                  </div>
                                 <div class="py-1">
                                   <div class="form-check abc-radio form-check-inline">
                                      <input type="radio" name="alertmail_send_day" id="alertmail_send_day_6" value="6" @if (isset($old_req)) {{$old_req->alertmail_send_day=="6"?"checked":""}} @endif>
                                      <label>{!! trans('app.sat') !!}</label>
                                    </div>
                                  </div>
                                 <div class="py-1">
                                   <div class="form-check abc-radio form-check-inline">
                                      <input type="radio" name="alertmail_send_day" id="alertmail_send_day_0" value="0" @if (isset($old_req)) {{$old_req->alertmail_send_day=="0"?"checked":""}} @endif>
                                      <label>{!! trans('app.sun') !!}</label>
                                    </div>
                                  </div>
                              </div>
                           </div>
                           <div class="form-group row col-12">
                                 <label class="col-3 text-left">{!! trans('app.alerttime') !!}</label>
                                   <select id="selectize1" class="col-2">
                                      @for($i=0; $i<24; $i++)
                                        <option value="{{$i}}" @if (isset($old_req)) {{$old_req->alertmail_send_time==$i?"checked":""}} @endif>{{$i}}:00</option>
                                      @endfor
                                   </select>
                            </div>

                        </form>
                       </div>
                    </div>
                  </div>

          <!--data table ここまで-->
      </section>
  <!--maincontent-->
@endsection

<!-- ./wrapper -->
 <!-- global js -->
 <script src="js/app.js" type="text/javascript"></script>
 <!-- end of global js-->
 <!-- page level js-->
 <script src="vendors/iCheck/js/icheck.js" type="text/javascript"></script>
 <script src="js/custom_js/form_layouts.js" type="text/javascript"></script>
 <script src="vendors/prettycheckable/js/prettyCheckable.min.js"></script>
  <script type="text/javascript" src="vendors/selectize/js/standalone/selectize.min.js"></script>
  <script type="text/javascript" src="vendors/selectric/js/jquery.selectric.min.js"></script>
  <script type="text/javascript" src="js/custom_js/custom_elements.js"></script>
  <script src="js/custom_js/radio_checkbox.js"></script>
 <!-- end of page level js-->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script>
 $(function(){
 		$('#btn_update').click(function() {
 			if( confirm('{!! trans('app.alertupdateconfirm') !!}') ){
 				$('#list').attr('action', '/control_alert_update');
 				$('#list').submit();
 			}
 			return false;
 		});
 });
 </script>
