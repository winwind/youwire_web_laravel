@extends('layouts.app')

@section('ip')
    @foreach ($ips as $ip)
      {{$ip}}
    @endforeach
@endsection

@section('content')
  <!-- to display error and messages from AlertmailController -->
  @if(Session::has('msg'))
    <div style="color:green">{{Session::pull('msg')}}</div>
  @endif
  @if(Session::has('err'))
    <div style="color:red">{{Session::pull('err')}}</div>
  @endif

  <div class="panel panel-primary">
    <div class="panel-heading">{!! trans('app.alertmailtitle') !!}</div>
    <div class="btn-group">
      <button type="button" id="btn_update" name="btn_update" class="btn btn-primary">{!! trans('app.edit') !!} <span class="glyphicon glyphicon-refresh"></span></button>
    </div>
    <div class="panel-body">
      <form id="list" name="list">
      {{ csrf_field() }}
        <table class="table">
          <tr>
            <th width="25%">
              {!! trans('app.alertmailsend') !!}
            </th>
            <td style="text-align:left" width="75%">
              <input type="radio" name="alertmail_send" id="alertmail_send_on" value="1" @if (isset($old_req)) {{$old_req->alertmail_send=="1"?"checked":""}} @endif>{!! trans('app.alertmailon') !!}
							<input type="radio" name="alertmail_send" id="alertmail_send_off" value="0" @if (isset($old_req)) {{$old_req->alertmail_send=="0"?"checked":""}} @endif>{!! trans('app.alertmailoff') !!}
            </td>
          </tr>
          <tr>
            <th width="25%">
              {!! trans('app.alertmaillimit') !!}
            </th>
            <td style="text-align:left" width="75%">
              <input type="radio" name="alertmail_send_limit" id="alertmail_send_limit_50" value="50" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="50"?"checked":""}} @endif>50%
							<input type="radio" name="alertmail_send_limit" id="alertmail_send_limit_60" value="60" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="60"?"checked":""}} @endif>60%
							<input type="radio" name="alertmail_send_limit" id="alertmail_send_limit_70" value="70" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="70"?"checked":""}} @endif>70%
							<input type="radio" name="alertmail_send_limit" id="alertmail_send_limit_80" value="80" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="80"?"checked":""}} @endif>80%
							<input type="radio" name="alertmail_send_limit" id="alertmail_send_limit_90" value="90" @if (isset($old_req)) {{$old_req->alertmail_send_limit=="90"?"checked":""}} @endif>90%
            </td>
          </tr>
          <tr>
            <th width="25%">
              {!! trans('app.alertmail') !!}
            </th>
            <td style="text-align:left" width="75%">
              <textarea name="alertmail_send_mail" id="alertmail_send_mail" style="width:100%">@if (isset($old_req)) {{$old_req->alertmail_send_mail}} @endif</textarea>
            </td>
          </tr>
          <tr>
            <th width="25%">
              {!! trans('app.alertday') !!}
            </th>
            <td style="text-align:left" width="75%">
              <input type="radio" name="alertmail_send_day" id="alertmail_send_day_1" value="1" @if (isset($old_req)) {{$old_req->alertmail_send_day=="1"?"checked":""}} @endif>{!! trans('app.mon') !!}
							<input type="radio" name="alertmail_send_day" id="alertmail_send_day_2" value="2" @if (isset($old_req)) {{$old_req->alertmail_send_day=="2"?"checked":""}} @endif>{!! trans('app.tue') !!}
							<input type="radio" name="alertmail_send_day" id="alertmail_send_day_3" value="3" @if (isset($old_req)) {{$old_req->alertmail_send_day=="3"?"checked":""}} @endif>{!! trans('app.wed') !!}
							<input type="radio" name="alertmail_send_day" id="alertmail_send_day_4" value="4" @if (isset($old_req)) {{$old_req->alertmail_send_day=="4"?"checked":""}} @endif>{!! trans('app.thu') !!}
							<input type="radio" name="alertmail_send_day" id="alertmail_send_day_5" value="5" @if (isset($old_req)) {{$old_req->alertmail_send_day=="5"?"checked":""}} @endif>{!! trans('app.fri') !!}
							<input type="radio" name="alertmail_send_day" id="alertmail_send_day_6" value="6" @if (isset($old_req)) {{$old_req->alertmail_send_day=="6"?"checked":""}} @endif>{!! trans('app.sat') !!}
							<input type="radio" name="alertmail_send_day" id="alertmail_send_day_0" value="0" @if (isset($old_req)) {{$old_req->alertmail_send_day=="0"?"checked":""}} @endif>{!! trans('app.sun') !!}
            </td>
          </tr>
          <tr>
            <th width="25%">
              {!! trans('app.alerttime') !!}
            </th>
            <td style="text-align:left" width="75%">
              <select name="alertmail_send_time" id="alertmail_send_time">
								@for ($i=0; $i < 24; $i++)
                  <option value="{{$i}}" @if (isset($old_req)) {{$old_req->alertmail_send_time==$i?"checked":""}} @endif>{{$i}}</option>
                @endfor
							</select>
            </td>
          </tr>
        </table>
      </form>
    </div><!--end panel body-->
  </div><!--end panel-->
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(function(){
		$('#btn_update').click(function() {
			if( confirm('{!! trans('app.alertupdateconfirm') !!}') ){
				$('#list').attr('action', '/control_alert_update');
				$('#list').submit();
			}
			return false;
		});
});
</script>
