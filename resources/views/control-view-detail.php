<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Control View Detail</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
   <link type="text/css" href="css/app.css" rel="stylesheet"/>
   <!-- end of global css -->
   <!-- page level css -->
   <link rel="stylesheet" href="stylesheet.css">
   <link href="vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" type="text/css" href="vendors/gridforms/css/gridforms.css">
   <link rel="stylesheet" type="text/css" href="css/custom.css">
   <link rel="stylesheet" type="text/css" href="css/form_layouts.css">

<!--datatables-->
    <link rel="stylesheet" type="text/css" href="vendors/datatables/css/dataTables.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="vendors/datatables/css/buttons.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="vendors/datatables/css/rowReorder.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="vendors/datatables/css/colReorder.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="vendors/datatables/css/scroller.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <link rel="stylesheet" type="text/css" href="css/custom_css/datatables_custom.css">

   <!--checkbox&radio-->
    <link href="vendors/prettycheckable/css/prettyCheckable.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="css/custom_css/radio_checkbox.css">
    <link rel="stylesheet" type="text/css" href="vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css">
   <!-- end of page level css -->
 </head>
  <boby class="skin-coreplus">
    <!--ヘッダー-->
      <header>
        <div style="margin:20px">
          <img class="youwire-logo" src="images/logo.png" style="width:350px;">
        </div>
        <div class="text-white" style="float:right; padding:20px;">
          ログインユーザー：<?php echo 'ユーザ名'?> || 前回ログイン日時：<?php echo '2018/9/4 12:00'?>
        </div>
        <section class="content-header">
          <div class="card-header text-white bg-primary">
               <div class="input-group-append dropdown">
                   <a href="welcome.php" class="btn menu-button btn-primary card-title"><i class="fa fa-fw fa-home"></i>TOP</a>

                    <button class="btn menu-button btn-primary dropdown-toggle dropdown" type="button" data-toggle="dropdown">
                      管理者メニュー
                    </button>
                      <ul class="dropdown-menu">
                        <li class="dropdown-item">
                            <a href="tag-category.php">タグカテゴリ登録</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="group.php">グループ登録</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="user-record.php">録音番号登録</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="user-search.php">検索ユーザー登録</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="securty-group.php">操作権限登録</a>
                        </li>
                    </ul>

                    <a href="userinfo.php" class="btn menu-button btn-primary card-title">パスワード変更</a>
                    <a href="login.php" class="btn menu-button btn-primary card-title">ログアウト</a>
                    <a href="welcome.php" class="btn menu-button btn-primary card-title">管理画面</a>

                </div>
          </div>
        </section>
      </header>
   <!--ヘッダーここまで-->

  <div class="wrapper">
    <section id="sidebar-search" style="width:300px;">
      <!--検索タブ-->

        <div class="col-12">
          <div class="search-tab">
            <div class="card border-primary ">
              <div class="card-header bg-primary text-white">
                  <h3 class="card-title d-inline">
                      <i class="fa fa-fw fa-search"></i>検索
                  </h3>
                  <span class="pull-right hidden-xs">
                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                    <i class="fa fa-fw fa-times removepanel"></i>
                  </span>
              </div>
              <div class="card card-body">
                  <form action="#" class="">

                      <!--div class="form-body box-body"-->
                      <div class="box-body" style="margin-left:5px;">
                             <div class="form-group row">
                                    <label class="col-12 col-form-label control-label text-left">
                                        通話時間：
                                    </label>
                                  <div class="col-12">
                                    <div class="input-group input-group-prepend">
                                        <div class="input-group-text border-right-0 rounded-0">
                                            <i class="fa fa-fw fa-clock-o"></i>
                                        </div>
                                        <input type="text" class="form-control" id="reservationtime"
                                               placeholder="MM/DD/YYYY HH:MM-MM/DD/YYYY HH:MM">
                                    </div>
                                </div>
                              </div>

                            <div class="form-group row">
                                <label for=rightinputnumber class="col-12 col-form-label text-left">
                                    自番号：
                                </label>
                                <div class="col-12">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text border-right-0 rounded-0">
                                        <i class="fa fa-fw fa-phone"></i>
                                    </span>
                                        <input type="text" placeholder="Phone Number"
                                               id="inputnumber" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputnumber" class="col-12 col-form-label text-left">
                                  相手番号：
                                </label>
                                  <div class="col-12">
                                    <div class="input-group input-group-prepend">
                                        <span class="input-group-text border-right-0 rounded-0">
                                        <i class="fa fa-fw fa-phone"></i>
                                    </span>
                                        <input type="text" placeholder="Phone Number"
                                               id="inputnumber2" class="form-control"/>
                                    </div>
                                  </div>

                            </div>
                            <div class="form-group row">
                                  <label style="padding-right:100px">
                                    　方向：
                                  </label>
                                       <div class="col-6 py-1">

                                              <div class="form-check abc-checkbox form-check-inline">
                                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                  <label for="inlineCheckbox1">着信 </label>
                                              </div>
                                          </div>
                                        <div class="col-6 py-1">

                                              <div class="form-check abc-checkbox form-check-inline">
                                                  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option1">
                                                  <label for="inlineCheckbox2">発信 </label>
                                              </div>
                                          </div>
                             </div>

                            <div class="form-group row">
                              <label style="padding-right:40px">
                                　ダウンロード：
                              </label>
                                 <div class="col-6 py-1">
                                   <div class="form-check abc-checkbox form-check-inline">
                                       <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                       <label for="inlineCheckbox1">あり</label>
                                   </div>
                                 </div>
                                 <div class="col-6 py-1">
                                    <div class="form-check abc-checkbox form-check-inline">
                                       <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option1">
                                       <label for="inlineCheckbox2">なし</label>
                                    </div>
                                   </div>
                             </div>

                             <div class="form-group row">
                                    <label class="col-12 col-form-label text-left">
                                      タグ:
                                    </label>
                                    <div class="col-12 ">
                                      <select id="selectize1" class="form-control">
                                          <option value="">--選択--</option>
                                          <option value="claim">クレーム</option>
                                          <option value="contact">問い合わせ</option>
                                          <option value="others">その他</option>
                                      </select>
                                    </div>
                              </div>
                              <div class="form-group row">
                                     <label class="col-12 col-form-label text-left">
                                       グループ:
                                     </label>
                                     <div class="col-12">
                                       <select id="selectize1" class="form-control">
                                           <option value="">--選択--</option>
                                           <option value="claim">クレーム</option>
                                           <option value="contact">問い合わせ</option>
                                           <option value="others">その他</option>
                                       </select>
                                     </div>
                               </div>
                               <div class="form-group row">
                                      <label class="col-12 col-form-label text-left">
                                        表示件数:
                                      </label>
                                      <div class="col-12 ">
                                        <select id="selectize1" class="form-control">
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>

                                        </select>
                                      </div>
                                </div>

                            <div>
                                <div class="row">
                                    <div class="col-12">

                                        <button type="button" class="btn btn-primary">
                                          検索
                                        </button>
                                        &nbsp;
                                        <button type="reset" class="btn btn-default bttn_reset">
                                          クリア
                                        </button>

                                  </div>
                                </div>
                            </div>
                        </div>
                    </form>
                 </div>
                </div>
              </div>
             </div>
            <!--検索タブ終わり-->

          </section>
        <!--sidebar-->

          <section id="main-content" class="col-8">
           <!--基本情報-->
               <div class="col-12">
                     <div class="card border-primary">
                         <div class="card-header text-white bg-primary" style="position:relative;">
                             <h3 class="card-title d-inline" >基本情報</h3>
                          </div>
                          <div class="card-body">
                            <div class="col-12 row">
                              <div class="col-6 row">
                                <label class="col-5 text-left">ID：</label>
                                <div class="col-6">353</div>
                              </div>
                              <div class="col-6 row">
                                <label class="col-5 text-left">自番号：</label>
                                <div class="col-6">08012345678</div>
                              </div>
                            </div>
                            <div class="col-12 row">
                              <div class="col-6 row">
                                <label class="col-5 text-left">通話開始：</label>
                                <div class="col-6">2018-08-07 14:40:20</div>
                              </div>
                              <div class="col-6 row">
                                <label class="col-5 text-left">相手番号：</label>
                                <div class="col-6">0801231231</div>
                              </div>
                            </div>
                            <div class="col-12 row">
                              <div class="col-6 row">
                                <label class="col-5 text-left">通話時間：</label>
                                <div class="col-6">00:01:49</div>
                              </div>
                              <div class="col-6 row">
                                <label class="col-5 text-left">方向：</label>
                                <div class="col-6">発信</div>
                              </div>
                            </div>
                            <div class="col-12 row">
                              <div class="col-6 row">
                                <label class="col-5 text-left">グループ名：</label>
                                <div class="col-6">デフォルト</div>
                              </div>
                              <div class="col-6 row">
                                <label class="col-5 text-left">自IPアドレス：</label>
                                <div class="col-6">0</div>
                              </div>
                            </div>
                            <div class="col-12 row">
                              <div class="col-6 row">
                                <label class="col-5 text-left">保護状態：</label>
                                <div class="col-6">解除</div>
                              </div>
                              <div class="col-6 row">
                                <label class="col-5 text-left">相手IPアドレス：</label>
                                <div class="col-6">0</div>
                              </div>
                            </div>
                            <div class="col-12 row">
                              <div class="col-6 row">
                                <label class="col-5 text-left">最終DL日時：</label>
                                <div class="col-6"></div>
                              </div>
                              <div class="col-6 row">
                              </div>
                            </div>
                          </div>
              <!--基本情報ここまで-->
                          <div class="card-header text-white bg-primary" style="position:relative;">
                              <p class="card-title d-inline" >
                                <i class="fa fa-fw fa-play-circle"></i>再生
                              </p>
                          </div>
                          <div class="card-body">


                          </div>
                    </div>
              </div>


             <!--tag-->
             <div class="col-12">
               <div class="card border-primary">
                 <div class="card-header text-white bg-primary">
                     <h3 class="card-title d-inline">
                         <i class="fa fa-fw fa-list-alt"></i>タグ一覧
                     </h3>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">
                         <table id="mytable" class="table table-bordred table-striped">
                             <thead>
                             <tr>
                                 <th>タグID</th>
                                 <th>カテゴリ名</th>
                                 <th>タイトル</th>
                                 <th>内容</th>
                                 <th>編集</th>
                                 <th>削除</th>
                             </tr>
                             </thead>
                             <tbody>
                             <tr>
                                 <td>1</td>
                                 <td>問い合わせ</td>
                                 <td>タイトル</td>
                                 <td>これは問い合わせです。</td>
                                 <td>

                                     <button class="btn btn-primary btn-xs" data-toggle="modal"
                                             data-target="#edit" data-placement="top"><span
                                             class="fa fa-pencil"></span></button>

                                 </td>
                                 <td>

                                     <button class="btn btn-danger btn-xs" data-toggle="modal"
                                             data-target="#delete" data-placement="top"><span
                                             class="fa fa-trash"></span></button>

                                 </td>
                             </tr>
                           </tbody>
                         </table>
                     </div>
                 </div>
             </div>
           </div>
              <div class="col-12">
                 <div class="card border-primary">
                    <div class="card-header text-white bg-primary">
                      <h3 class="card-title d-inline"  style="position:relative;">タグ新規登録/更新</h3>
                      <button class="btn btn-primary pull-right d-sm-block submit-group" style="position:absolute; top:3px; right:5px;">
                        <i class="fa fa-fw fa-upload"></i>登録
                      </button>

                     </div>
                       <div class="card-body">
                          <form class="col-12">
                           <div class="form-group row">
                              <label class="col-3 text-left">タグID：</label>
                              <div class="col-3">123</div>
                           </div>
                           <div class="form-group row">
                                 <label class="col-3 text-left">カテゴリ名：</label>
                                   <select id="selectize1" class="col-3">
                                       <option value="1">問い合わせ</option>
                                       <option value="claim">クレーム</option>
                                       <option value="contact">その他</option>
                                   </select>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-3 text-left">タイトル：</label>
                                <input type="text" class="col-3" id="inputGroupId"
                                       value="">
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-3 text-left">内容：</label>
                                <textarea rows="4" class="col-6" id="inputGroupId" value=""></textarea>
                            </div>
                        </form>
                       </div>
                    </div>
                  </div>

          <!--data table ここまで-->
      </section>
  <!--maincontent-->

  </div>
<!-- ./wrapper -->
 <!-- global js -->
 <script src="js/app.js" type="text/javascript"></script>
 <!-- end of global js-->
 <!-- page level js-->
 <script src="vendors/iCheck/js/icheck.js" type="text/javascript"></script>
 <script src="js/custom_js/form_layouts.js" type="text/javascript"></script>

  <!--script type="text/javascript" src="vendors/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
  <script type="text/javascript" src="vendors/select2/js/select2.js"></script>
  <script type="text/javascript" src="vendors/select2/js/select2.full.js"></script-->

  <script type="text/javascript" src="vendors/selectize/js/standalone/selectize.min.js"></script>
  <script type="text/javascript" src="vendors/selectric/js/jquery.selectric.min.js"></script>
  <script type="text/javascript" src="js/custom_js/custom_elements.js"></script>

  <script src="js/custom_js/radio_checkbox.js"></script>


  <script src="js/custom_js/radio_checkbox.js"></script>

<!--datatables-->
<script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="vendors/datatables/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript" src="vendors/datatables/js/dataTables.rowReorder.js"></script>
<script type="text/javascript" src="vendors/datatables/js/dataTables.scroller.js"></script>

<script type="text/javascript" src="js/custom_js/datatables_custom.js"></script>
 <!-- end of page level js-->
  </body>
</html>
