@extends('layouts.app')

@section('ip')
    @foreach ($ips as $ip)
      {{$ip}}
    @endforeach
@endsection

@section('leftpanel')
  @include('inc.searchbar')
@endsection

@section('content')
<form id="list" name="list">
{{ csrf_field() }}
  @if (!empty($res))
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <div>{!! trans('app.info') !!}
                    <div class="btn-group">
                        <button type="button" name="btn_back" id="btn_back" class="btn btn-primary">{!! trans('app.back') !!} <span class="glyphicon glyphicon-arrow-left"></span></button>
                        <button type="button" name="button_play" id="button_play" class="btn btn-primary" onclick="@if ($config['player_value'] == 1) wmp_play('{!! asset('recordings') !!}/{{$res->filename_org}}'
                        @else loadjwplayer('{!! asset('recordings') !!}/{{$res->localparty}}/{{$res->filename}}'@endif,'recording_id_{{$res->id}}');">
                              <img src="{!! asset('images/button_blue_play_26.png') !!}"/>
                        </button>
                        @if ($data['authority_protect'] == 1)
                            <button type="button" id="btn_detail_lock" name="btn_detail_lock" class="btn btn-primary">{!! trans('app.lock') !!} <span class="glyphicon glyphicon-lock"></span></button>
                            <button type="button" id="btn_detail_unlock" name="btn_detail_unlock" class="btn btn-primary">{!! trans('app.unlock') !!} <span class="glyphicon glyphicon-remove"></span></button>
                        @endif
                        @if ($data['authority_delete'] == 1)
                            <button type="button" id="btn_detail_delete" name="btn_detail_delete" class="btn btn-primary">{!! trans('app.delete') !!} <span class="glyphicon glyphicon-trash"></span></button>
                        @endif
                        @if ($data['authority_download'] == 1)
                            <button type="button" id="btn_detail_download" name="btn_detail_dl" class="btn btn-primary">{!! trans('app.download') !!} <span class="glyphicon glyphicon-floppy-save"></span></button>
                        @endif
                    </div>
                </div>
              </div>
              <div class="panel-body">
                <table class="table">
                  <tr>
                    <th>
                      {!! trans('app.id') !!}
                    </th>
                    <td>
                      {{$res->id}}
                      @if ($res->protect === 1)
                          <span class="glyphicon glyphicon-remove-circle"></span>
                      @endif
                    </td>
                    <th>
                      {!! trans('app.localparty') !!}
                    </th>
                    <td>
                      {{$res->localparty}}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      {!! trans('app.callstart') !!}
                    </th>
                    <td>
                      {{$res->timestamp}}
                    </td>
                    <th>
                      {!! trans('app.remoteparty') !!}
                    </th>
                    <td>
                      {{ $res->remoteparty}}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      {!! trans('app.duration') !!}
                    </th>
                    <td>
                      {{$res->duration}}
                    </td>
                    <th>
                      {!! trans('app.direction') !!}
                    </th>
                    <td>
                      @if ($res->direction == 0)
                        {!! trans('app.incoming') !!}
                      @elseif ($res->direction == 1)
                        {!! trans('app.outgoing') !!}
                      @elseif ($res->direction == 2)
                        {!! trans('app.voicerecorder') !!}
                      @endif
                    </td>
                  </tr>
                  <tr>
                    <th>
                      {!! trans('app.group') !!}
                    </th>
                    <td>
                      {{$res->group_name}}
                    </td>
                    <th>
                      {!! trans('app.localip') !!}
                    </th>
                    <td>
                      {{ $res->remoteip}}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      {!! trans('app.protectstate') !!}
                    </th>
                    <td>
                      @if ($res->protect == 0)
                        {!! trans('app.release') !!}
                      @else
                        {!! trans('app.protect') !!}
                      @endif
                    </td>
                    <th>
                      {!! trans('app.remoteip') !!}
                    </th>
                    <td>
                      {{ $res->localip}}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      {!! trans('app.dldatetime') !!}
                    </th>
                    <td>
                      {{$res->download_datetime}}
                    </td>
                    <th>
                      {!! trans('app.agentid') !!}
                    </th>
                    <td>
                      Agent ID
                    </td>
                  </tr>
                </table>
              </div>

                <!--player -->
                <div class="panel-body">
                   @if ($config["player_value"] == 1) <!-- 1 => Windows Media Player -->
                      <object
                            id="wmp2"
                            width="98%" height="45"
                            classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
                            codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715"
                            standby="Loading MicrosoftR WindowsR Media Player components..."
                            type="application/x-oleobject">

                            <!-- <param name="FileName" value="./media/meta_sample.asx" /> --><!-- Filename属性は絶対・相対パスどちらでもOK -->
                            <param name="ShowControls" value="true" /><!-- コントロール表示 -->
                            <param name="AutoStart" value="true" /><!-- 自動再生しない -->

                            <!-- コントロール表示（showcontrols="1"）、自動再生しない（autostart="0"） -->
                            <embed
                              name="wmp2"
                              type="application/x-mplayer2"
                              pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"
                              width="98%" height="45"
                              showcontrols="1"><!-- src属性は絶対パス指定 -->
                            </embed>
                      </object>
                    @else
                      <div id="player_container" style="margin:0 auto;"></div>
                      <p id="message"></p>
                      ​<script src="{!! asset('js/jwplayer.js') !!}" ></script>
                      <script type="text/javascript">
                            jwplayer("player_container").setup({
                            flashplayer: "{!! asset('player/player.swf') !!}",
                            controlbar: "bottom",
                            //width: 800,
                            width:"99%",height: 24,
                            stretching: 'exactfit'
                            });
                    </script>
                  @endif
              </div>
            </div>

            <!--speech data recognition -->
            <div class="panel panel-primary">
              <div class="panel-heading">{!! trans('app.speechrecognitiondata') !!}</div>
              <div class="panel-body">
                <div id="recognition">
                  <div name="youwire_recognition" id="youwire_recognition" style="text-align: left;"></div>
                </div>
              </div>
            </div>

            <!-- Tag list display -->
            <div class="panel panel-primary">
              <div class="panel-heading">{!! trans('app.taglist') !!}</div>
              <div class="panel-body">
                <div id="taglist">
                  <table class="table">
                    <tr>
                			<th width="45">{!! trans('app.tagid') !!}</th>
                			<th width="70">{!! trans('app.tagcatname') !!}</th>
                			<th width="130">{!! trans('app.tagtitle') !!}</th>
                			<th>{!! trans('app.tagtext') !!}</th>
                			<th width="80">{!! trans('app.tagcreatetime') !!}</th>
                			<th width="80">{!! trans('app.tagupdatetime') !!}</th>
                			@if ($data['authority_add_tag'] == 1)
                        <th width="155">{!! trans('app.tagcontrol') !!}</th>
                      @endif
                		</tr>
                    @foreach ($tagListData as $tagData)
                      <tr id="tag_id_{{$tagData['tag_id']}}">
                        <td style="text-align:center" >{{$tagData['tag_id']}}</td>
                  			<td style="text-align:left" id="tag_catagories_name{{$tagData['tag_id']}}">{{$tagData['tag_catagories_name']}}</td>
                  			<td style="text-align:left" id="tag_title{{$tagData['tag_id']}}">{{$tagData['tag_title']}}</td>
                  			<td style="text-align:left" id="tag_text{{$tagData['tag_id']}}">{{$tagData['tag_text']}}</td>
                  			<td style="text-align:center" >{{$tagData['tag_create_date']}}</td>
                  			<td style="text-align:center" >{{$tagData['tag_update_date']}}</td>
                        @if($data['authority_add_tag'] == 1)
                    			<td style="text-align:center">
                    				   <a href="{{$tagData['tag_id']}}" name="button_dl{{$tagData['tag_id']}}" id="button_dl{{$tagData['tag_id']}}" value="{{$tagData['tag_id']}}" class="btn btn-primary btn_tag_edit_update">
                                  <span class="glyphicon glyphicon-edit"></span> {!! trans('app.tagedit') !!}
                               </a>
                    					 <a href="{{$tagData['tag_id']}}" name="button_dl{{$tagData['tag_id']}}" id="button_dl{{$tagData['tag_id']}}" value="{{$tagData['tag_id']}}" class="btn btn-primary btn_tag_del">
                                  <span class="glyphicon glyphicon-trash"></span> {!! trans('app.tagdelete') !!}
                               </a>
                    			</td>
                  			@endif
                      </tr>
                    @endforeach
                  </table>
                </div>
              </div>
            </div>

            <!--Tag Add / Edit / Update  -->
            @if($data['authority_add_tag'] == 1)
              <div class="panel panel-primary">
                <div class="panel-heading">{!! trans('app.tagedittitle') !!}
                  <div class="btn-group">
                    <button type="button" id="button_tag_add" name="button_tag_add" class="btn btn-primary">{!! trans('app.new') !!} <span class="glyphicon glyphicon-plus"></span></button>
                    <button type="button" id="button_tag_update" name="button_tag_update" class="btn btn-primary">{!! trans('app.edit') !!} <span class="glyphicon glyphicon-refresh"></span></button>
                  </div>
                </div>
                <div class="panel-body">
                  <table class="table">
                    <tr>
                			<th width="20%">
                				{!! trans('app.tagid') !!}
                			</th>
                			<td style="text-align:left" width="80%"  id="tag_edit_id">
                				{!! trans('app.newtagidmsg') !!}
                			</td>
                		</tr>
                    <tr>
                			<th>
                				{!! trans('app.tagcatname') !!}
                			</th>
                			<td style="text-align:left" >
                				<select name="selected_tag_edit" id="selected_tag_edit">
                					<option value=0>---</option>
                          @foreach ($tag_cat as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                          @endforeach
                				</select>
                			</td>
                		</tr>
                    <tr>
                			<th>
                				{!! trans('app.tagtitle') !!}
                			</th>
                			<td style="text-align:left" >
                				<input type="text" name="tag_title" id="tag_title" value="" onKeyPress="return submitStop(event);">
                			</td>
                		</tr>
                    <tr>
                			<th>
                				{!! trans('app.tagtext') !!}
                			</th>
                			<td style="text-align:left" >
                				<TEXTAREA name="tag_text" id="tag_text" value="" style="width:100%"></TEXTAREA>
                			</td>
                		</tr>
                  </table>
                </div>
              </div>
            @endif
    <input type="hidden" name="id" id="id" value="{{$res->id}}"/>
    @endif
    <input type="hidden" name="tag_id" id="tag_id" value="-1">
    <input type="hidden" name="search_sql" id="search_sql" value="{{$data['search_sql']}}"/>
    <input type="hidden" name="selected_page" id="selected_page" value="{{$data['selected_page']}}"/>
    <input type="hidden" name="use_search_sql" id="use_search_sql" value="1"/> <!--this is used to inform defaultController=>search() that request is sent from detialView-->
    <input type="hidden" name="protect" id="protect" value="0"/>
    <input type="hidden" name="edit" id="edit" value="0"/>
    <input type="hidden" name="page" id="page" value="{{$data['page']}}"/>
</form>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
function submitStop(e){
    if(!e) var e = window.event;

    if(e.keyCode == 13)
        return false;
}


$(function(){
  $('#btn_back').click(function() {
    $('#list').attr('action', '/search');
    $('#list').attr('method', 'GET');
    $('#list').submit();
    return false;
  });
  $('#btn_detail_lock').click(function() {
		$('#list').attr('action', '/control_detail_update');
    $('#protect').attr('value', '1');
		$('#list').submit();
		return false;
	});
  $('#btn_detail_unlock').click(function() {
		$('#list').attr('action', '/control_detail_update');
    $('#protect').attr('value', '0');
		$('#list').submit();
		return false;
	});
  $('#btn_detail_delete').click(function() {
		if( confirm('{!! trans('app.deleteconfirm') !!}') ){
			$('#list').attr('action', '/control_detail_delete');
			$('#list').submit();
		}
		return false;
	});
  $('#btn_detail_download').click(function() {
		$('#list').attr('action', '/control_detail_download');
		$('#list').submit();
		return false;
	});
  $('.btn_tag_edit_update').click(function() {
		/*$('#tag_id').val($(this).attr("href"));
		$('#list').attr('action', '/control_view_tag_edit');
		$('#list').submit();*/
    var tagid = $(this).attr('href');
    var tagcatname = $('#tag_catagories_name'+tagid).html();
    var tagtitle = $('#tag_title'+tagid).html();
    var tagtext = $('#tag_text'+tagid).html();

    $('#tag_edit_id').html(tagid);
    $('#selected_tag_edit option').each(function(){
      if($(this).html() == tagcatname)
      {
        $(this).attr('selected','selected');
      }
    });
    $('#tag_title').val(tagtitle);
    $('#tag_text').val(tagtext);
		return false;
	});
  $('.btn_tag_del').click(function() {
		if( confirm('{!! trans('app.tagdeleteconfirm') !!}') ){
			$('#tag_id').val($(this).attr("href"));
			$('#list').attr('action', '/control_tag_delete');
			$('#list').submit();
		}
		return false;
	});
  $('#button_tag_add').click(function() {
		if( confirm('{!! trans('app.tagaddconfirm') !!}') ){
      $('#tag_id').val($('#tag_edit_id').html());
      $('#edit').attr('value','1');
			$('#list').attr('action', '/control_tag_edit');
			$('#list').submit();
		}
		return false;
	});
  $('#button_tag_update').click(function() {
		if( $('#tag_edit_id').html() == '' || isNaN($('#tag_edit_id').html())){
			alert( '{!! trans('app.updatecheck') !!}' );
		} else {
			if( confirm('{!! trans('app.updateconfirm') !!}') ){
        $('#tag_id').val($('#tag_edit_id').html());
        $('#edit').attr('value','2');
				$('#list').attr('action', '/control_tag_edit');
				$('#list').submit();
			}
		}
		return false;
	});
});


@if ($config['player_value'] == 1)
    function wmp_play(mediapath,reco_id,id){
        //alert(mediapath);
        document.wmp2.filename = mediapath;
        document.wmp2.play();
              /*if ( id ) {
                      var voiceText = document.getElementById('voicetext_'+id).innerHTML;
                      var dispArea = document.getElementById('youwire_recognition');
                      dispArea.value = voiceText;
              }*/
    }
@else
    function loadjwplayer(mediapath,reco_id,id){
        //alert(mediapath);
        jwplayer().load({file: mediapath});
        jwplayer().play();
        /*if ( id ) {
        var voiceText1 = document.getElementById('voicetext_'+id).innerHTML;
        var voiceText2 = document.getElementById('voicetext_'+id+'_html').value;
        var dispArea = document.getElementById('youwire_recognition');
        voiceHtml = voiceText2.split(' ');
        var html = '';
        for (var i = 0; i < voiceHtml.length; i++) {
          if ( voiceHtml[i].indexOf(':0') != -1 ) {
            var str = voiceHtml[i].replace( /:0/g , '' );
            var telnum = document.getElementById('localparty_'+id).innerHTML;
            html += '<div class="dir_0"><span class="telnum">' + telnum + '：</span><span class="text">' + str + '</span></div>';
          } else if ( voiceHtml[i].indexOf(':1') != -1 ) {
            var str = voiceHtml[i].replace( /:1/g , '' );
            var telnum = document.getElementById('remoteparty_'+id).innerHTML;
            html += '<div class="dir_1"><span class="telnum">' + telnum + '：</span><span class="text">' + str + '</span></div>';
          }
        }
        dispArea.value = voiceText1;
        dispArea.innerHTML = html;
      }*/
    }

@endif
</script>
