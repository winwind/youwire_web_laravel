<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>groups</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="stylesheet" href="vendors/chartist/css/chartist.min.css">
    <link href="vendors/nvd3/css/nv.d3.min.css" rel="stylesheet" type="text/css">
    <link href="vendors/morrisjs/morris.css" rel="stylesheet" type="text/css">
    <link href="vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="vendors/awesomebootstrapcheckbox/css/build.css">
        <!-- global css -->
    <link type="text/css" href="css/app.css" rel="stylesheet">
        <!-- end of global css -->
  　<link rel="stylesheet" type="text/css" href="css/formelements.css">
    <link href="css/buttons_sass.css" rel="stylesheet">
        <!--page level css -->
    <link href="css/nestable.css" rel="stylesheet"/>
    <link href="css/datatable.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link href="vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
  　<link rel="stylesheet" type="text/css" href="vendors/gridforms/css/gridforms.css">
    <link rel="stylesheet" type="text/css" href="css/form_layouts.css">
  </head>
 <body class="skin-coreplus">
  <!--ヘッダー-->
  <header>
    <div style="margin:20px">
      <img class="youwire-logo" src="images/logo.png" style="width:350px;">
    </div>
    <div class="text-white" style="float:right; padding:20px;">
      ログインユーザー：<?php echo 'ユーザ名'?> || 前回ログイン日時：<?php echo '2018/9/4 12:00'?>
    </div>
    <section class="content-header">
      <div class="card-header text-white bg-primary">
           <div class="input-group-append dropdown">
               <a href="welcome.php" class="btn menu-button btn-primary card-title"><i class="fa fa-fw fa-home"></i>TOP</a>

                <button class="btn menu-button btn-primary dropdown-toggle dropdown" type="button" data-toggle="dropdown">
                  管理者メニュー
                </button>
                  <ul class="dropdown-menu">
                    <li class="dropdown-item">
                        <a href="tag-category.php">タグカテゴリ登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="group.php">グループ登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="user-record.php">録音番号登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="user-search.php">検索ユーザー登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="securty-group.php">操作権限登録</a>
                    </li>
                </ul>

                <a href="userinfo.php" class="btn menu-button btn-primary card-title">パスワード変更</a>
                <a href="login.php" class="btn menu-button btn-primary card-title">ログアウト</a>
                <a href="welcome.php" class="btn menu-button btn-primary card-title">管理画面</a>

            </div>
      </div>
    </section>
  </header>
 <!--ヘッダーここまで-->
    <div class="wrapper">
      <section class="content">
        <div class="row contents">

        <!------------------------------------------------------------------------------------->
            <div class="col-lg-4  col-12">
                        <div class="card border-primary">
                                <div class="card-header text-white bg-primary" style="position:relative;">
                                    <h3 class="card-title d-inline">
                                        グループ登録
                                    </h3>
                                    <!--span class="pull-right"-->
                                          <button class="btn btn-primary pull-right d-sm-block submit-group" style="position:absolute; top:3px; right:5px;">
                                            <i class="fa fa-fw fa-upload"></i>登録
                                          </button>
                                    <!--/span-->
                                </div>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="inputEmail3">グループID</label>
                                            <input type="text" class="form-control" id="inputGroupId"
                                                   placeholder="ID" disabled="disabled">
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword1">グループ名</label>
                                            <input type="text" class="form-control" id="inputGroupName"
                                                   placeholder="グループ名">
                                        </div>
                                    </form>
                                </div>
                            </div>
                          </div>
                  <!--/div-->

        <!------------------------------------------------------------------------------------->
            <div class="col-lg-8 col-12">
                <div class="card card-danger">
                    <div class="card-header text-white bg-primary" style="position:relative;">
                        <h3 class="card-title d-inline" >
                          グループ一覧
                        </h3>
                        <button class="btn btn-primary pull-right d-sm-block" style="position:absolute; top:3px; right:5px;">
                          <i class="fa fa-fw fa-sitemap submit-groupmap"></i>グループ構成保存
                        </button>
                    </div>
                              <div class="card-body ">
                                  <div class="dd" id="nestable_list_1">
                                      <ol class="dd-list">
                                          <li class="dd-item" data-id="1">
                                              <div class="dd-handle">デフォルト</div>
                                                <div class="content-buttons">
                                              <!--処理選択ボタン-->
                                                    <span class="testBtn">
                                                      <button class="btn dropdown-toggle input-group-append" type="button" aria-expanded="true" data-toggle="dropdown">
                                                        処理選択
                                                      </button>
                                                        <ul class="dropdown-menu pull-right action-dropdownmenu actiondropdown">
                                                          <li class="dropdown-item">
                                                              <a href="#">編集</a>
                                                          </li>
                                                          <li class="dropdown-item">
                                                              <a href="#">削除</a>
                                                          </li>
                                                          <li class="dropdown-divider"></li>
                                                          <li class="dropdown-item">
                                                              <a href="#">録音番号一覧へ</a>
                                                          </li>
                                                          <li class="dropdown-item">
                                                              <a href="#">検索ユーザー一覧へ</a>
                                                          </li>
                                                      </ul>
                                                    </span>
                                              <!--処理選択ここまで-->
                                              </div>

                                          </li>
                                          <li class="dd-item" data-id="2">
                                              <div class="dd-handle">東京本社</div>
                                                <div class="content-buttons">
                                              <!--処理選択ボタン-->
                                                    <span class="testBtn">
                                                      <button class="btn dropdown-toggle input-group-append" type="button" aria-expanded="true" data-toggle="dropdown">
                                                        処理選択
                                                      </button>
                                                        <ul class="dropdown-menu pull-right action-dropdownmenu actiondropdown">
                                                          <li class="dropdown-item">
                                                              <a href="#">編集</a>
                                                          </li>
                                                          <li class="dropdown-item">
                                                              <a href="#">削除</a>
                                                          </li>
                                                          <li class="dropdown-divider"></li>
                                                          <li class="dropdown-item">
                                                              <a href="#">録音番号一覧へ</a>
                                                          </li>
                                                          <li class="dropdown-item">
                                                              <a href="#">検索ユーザー一覧へ</a>
                                                          </li>
                                                      </ul>
                                                    </span>
                                              <!--処理選択ここまで-->
                                                </div>

                                              <ol class="dd-list">
                                                  <li class="dd-item" data-id="3">
                                                      <div class="dd-handle">【本社】営業部門</div>
                                                         <div class="content-buttons">
                                                      <!--処理選択ボタン-->
                                                            <span class="testBtn">
                                                              <button class="btn dropdown-toggle input-group-append" type="button" aria-expanded="true" data-toggle="dropdown">
                                                                処理選択
                                                              </button>
                                                                <ul class="dropdown-menu pull-right action-dropdownmenu actiondropdown">
                                                                  <li class="dropdown-item">
                                                                      <a href="#">編集</a>
                                                                  </li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">削除</a>
                                                                  </li>
                                                                  <li class="dropdown-divider"></li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">録音番号一覧へ</a>
                                                                  </li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">検索ユーザー一覧へ</a>
                                                                  </li>
                                                              </ul>
                                                            </span>
                                                      <!--処理選択ここまで-->
                                                       </div>

                                                  </li>
                                                  <li class="dd-item" data-id="4">
                                                      <div class="dd-handle">【本社】システム部門</div>
                                                        <div class="content-buttons">
                                                      <!--処理選択ボタン-->
                                                            <span class="testBtn">
                                                              <button class="btn dropdown-toggle input-group-append" type="button" aria-expanded="true" data-toggle="dropdown">
                                                                処理選択
                                                              </button>
                                                                <ul class="dropdown-menu pull-right action-dropdownmenu actiondropdown">
                                                                  <li class="dropdown-item">
                                                                      <a href="#">編集</a>
                                                                  </li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">削除</a>
                                                                  </li>
                                                                  <li class="dropdown-divider"></li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">録音番号一覧へ</a>
                                                                  </li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">検索ユーザー一覧へ</a>
                                                                  </li>
                                                              </ul>
                                                            </span>
                                                      <!--処理選択ここまで-->
                                                      </div>

                                                  </li>
                                                  <li class="dd-item" data-id="5">
                                                      <div class="dd-handle">Item 5</div>
                                                        <div class="content-buttons">
                                                      <!--処理選択ボタン-->
                                                            <span class="testBtn">
                                                              <button class="btn dropdown-toggle input-group-append" type="button" aria-expanded="true" data-toggle="dropdown">
                                                                処理選択
                                                              </button>
                                                                <ul class="dropdown-menu pull-right action-dropdownmenu actiondropdown">
                                                                  <li class="dropdown-item">
                                                                      <a href="#">編集</a>
                                                                  </li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">削除</a>
                                                                  </li>
                                                                  <li class="dropdown-divider"></li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">録音番号一覧へ</a>
                                                                  </li>
                                                                  <li class="dropdown-item">
                                                                      <a href="#">検索ユーザー一覧へ</a>
                                                                  </li>
                                                              </ul>
                                                            </span>
                                                      <!--処理選択ここまで-->
                                                       </div>

                                                      <ol class="dd-list">
                                                          <li class="dd-item" data-id="6">
                                                              <div class="dd-handle">Item 6</div>
                                                                <div class="content-buttons">
                                                              <!--処理選択ボタン-->
                                                                    <span class="testBtn">
                                                                      <button class="btn dropdown-toggle input-group-append" type="button" aria-expanded="true" data-toggle="dropdown">
                                                                        処理選択
                                                                      </button>
                                                                        <ul class="dropdown-menu pull-right action-dropdownmenu actiondropdown">
                                                                          <li class="dropdown-item">
                                                                              <a href="#">編集</a>
                                                                          </li>
                                                                          <li class="dropdown-item">
                                                                              <a href="#">削除</a>
                                                                          </li>
                                                                          <li class="dropdown-divider"></li>
                                                                          <li class="dropdown-item">
                                                                              <a href="#">録音番号一覧へ</a>
                                                                          </li>
                                                                          <li class="dropdown-item">
                                                                              <a href="#">検索ユーザー一覧へ</a>
                                                                          </li>
                                                                      </ul>
                                                                    </span>
                                                              <!--処理選択ここまで-->
                                                              </div>

                                                          </li>
                                                          <li class="dd-item" data-id="7">
                                                              <div class="dd-handle">Item 7</div>
                                                                <div class="content-buttons">
                                                              <!--処理選択ボタン-->
                                                                    <!--span class="testBtn">
                                                                      <button class="btn dropdown-toggle input-group-append" type="button" aria-expanded="true" data-toggle="dropdown">
                                                                        処理選択
                                                                      </button>
                                                                        <ul class="dropdown-menu pull-right action-dropdownmenu actiondropdown">
                                                                          <li class="dropdown-item">
                                                                              <a href="#">編集</a>
                                                                          </li>
                                                                          <li class="dropdown-item">
                                                                              <a href="#">削除</a>
                                                                          </li>
                                                                          <li class="dropdown-divider"></li>
                                                                          <li class="dropdown-item">
                                                                              <a href="#">録音番号一覧へ</a>
                                                                          </li>
                                                                          <li class="dropdown-item">
                                                                              <a href="#">検索ユーザー一覧へ</a>
                                                                          </li>
                                                                      </ul>
                                                                    </span-->
                                                                            <span class="testBtn  input-group-append">
                                                                              <button class="btn btn-primary" type="button">
                                                                                <i class="fa fa-fw fa-check-square-o"></i>編集</button>
                                                                            </span>
                                                                            <span class="testBtn input-group-append">
                                                                              <button class="btn btn-primary" type="button">
                                                                                <i class="fa fa-fw fa-trash"></i>削除</button>
                                                                            </span>
                                                                            <span class="testBtn input-group-append">
                                                                              <button class="btn btn-default" type="button">録音番号一覧へ</button>
                                                                            </span>
                                                                            <span class="testBtn  input-group-append">
                                                                              <button class="btn btn-default" type="button">検索ユーザー一覧へ</button>
                                                                            </span>
                                                                        </div>
                                                              <!--処理選択ここまで-->
                                                             </div>

                                                          </li>
                                                      </ol>
                                                  </li>

                                            </ol>
                                      </div>
                                   </div>
                              </div>
                        </div>
<!------------------------------------------------------------------------------------->
        </div>

      </content>
    </div>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
    <!-- global js -->
    <script src="js/app.js" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
    <script src="vendors/nestable-list/jquery.nestable.js"></script>
    <script src="js/custom_js/nestable_list.js" type="text/javascript"></script>
    <script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="js/custom_js/simple-table.js"></script>
    <script src="vendors/iCheck/js/icheck.js" type="text/javascript"></script>
    　<script src="js/custom_js/form_layouts.js" type="text/javascript"></script>
    <!-- end of page level js -->
 </body>
</html>
