<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->


{!! Form::model($get_group, ['url' => 'group/'.$get_group->id, 'method' => 'get']) !!}
<h3>グループID{{$get_group->id}}</h3> 
<?= Form::label('group_name', 'グループ名'); ?>
<?= Form::text('group_name', null, ['class' => 'form-control', 'placeholder' => 'group_name']); ?>
<?= Form::submit('更新', ['class' => 'btn btn-primary']); ?>  
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>
