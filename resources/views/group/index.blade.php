<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->

<h1>GROUP PAGE</h1>
<h2>グループ一覧</h2>

<table class="table">
	@foreach ($get_groups as $get_group)
	<tr>
		<td>{{$get_group->group_name}}</td>
		<!--{{$node = $loop->count}} 必要ですが、消すのはダメ。--> 
		<td><a href="{{ url('/group/'.$get_group->id.'/edit')}}">編集</a></td>
		<td><a href="{{ url('/group/destroy/'.$get_group->id)}}">削除</a></td>
		<td><a href="{{ url('/user_record/bygroupid/'.$get_group->id)}}">録音番号一覧へ</a></td>
		<td><a href="{{ url('/user_search/bygroupid/'.$get_group->id)}}">検索ユーザー一覧へ</a></td>
	</tr>
	@endforeach


</table>

<!-- Create from start 2018/08/13 end 2018/08/13 -->
<hr>
<h2>グループ登録</h2>
{!! Form::open(['url' => '/group/create', 'method' => 'get']) !!} 
<?= Form::label('group_name', 'グループ名'); ?>
<?= Form::text('group_name', null, ['class' => 'form-control', 'placeholder' => 'group_name']); ?>
<!-- {{$new_node = $node + 1}} 必要ですが、消すのはダメ。-->
<?= Form::hidden('group_node', $new_node, ['class' => 'form-control', 'placeholder' => 'group_name']); ?>
<?= Form::submit('新規登録'); ?>   
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>