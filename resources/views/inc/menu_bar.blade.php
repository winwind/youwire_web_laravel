<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">{!! trans('app.togglenavigation') !!}</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!--<a class="navbar-brand" href="#">Top</a>-->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active_page"><a href="/">Top</a></li>
        <!--<li><a href="#">Link</a></li>-->
        @if($data['authority_edit_tag_category'] == 1 || $data['authority_edit_security_group'] == 1 || $data['authority_edit_group'] == 1 || $data['authority_edit_user'] == 1 || $data['authority_edit_alertmail'] == 1)
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">{!! trans('app.adminmenu') !!} <b class="caret"></b></a>
          <ul class="dropdown-menu">
            @if($data['authority_edit_tag_category'] == 1)
              <li><a href="/tagCategoryView">{!! trans('app.createtagcat') !!}</a></li>
            @endif
            @if($data['authority_edit_group'] == 1)
              <li><a href="#">{!! trans('app.groupregistration') !!}</a></li>
            @endif
            @if($data['authority_edit_user'] == 1)
              <li><a href="#">{!! trans('app.registerrecordingnumber') !!}</a></li>
              <li><a href="#">{!! trans('app.searchuserregistration') !!}</a></li>
            @endif
            @if($data['authority_edit_alertmail'] == 1)
              <li><a href="/alertmailView">{!! trans('app.alertemailsettings') !!}</a></li>
            @endif
            @if($data['authority_edit_security_group'] == 1)
              <li><a href="#">{!! trans('app.registeroperationauthority') !!}</a></li>
            @endif
            @if($data['authority_edit_tag_category'] == 1 && $data['authority_edit_security_group'] == 1 && $data['authority_edit_group'] == 1 && $data['authority_edit_user'] == 1 && $data['authority_edit_alertmail'] == 1)
              <li><a href="/operationlog">{!! trans('app.operationlog') !!}</a></li>
            @endif
          </ul>
        </li>
        @endif
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="/admin">{!! trans('app.managementscreen') !!}</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              @if (Auth::check())
                {{ Auth::user()->uname }} <span class="caret"></span>
              @endif
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        {!! trans('app.logout') !!}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                <li><a href="#">{!! trans('app.changepw') !!}</a></li>
            </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
</nav>

<script src="{{ asset('js/app.js') }}"></script>
