<!--left panel start -->
  <div class="panel panel-primary">
    <div class="panel-heading">{!! trans('app.search') !!}</div>
    <div class="panel-body">
        <form id="serch_form">
          {{ csrf_field() }}
          <div class="form-group row">
            <label for="startdatetime" class="col-xs-4 col-form-label">{!! trans('app.callstart') !!}</label>
            <div class="col-xs-8">
              <div class='input-group date' id='datetimepicker1'>
                  <input type="text" class="form-control" id="startdatetime" name="startdatetime" value="@if (isset($old_req)) {{$old_req->startdatetime}} @endif">
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="enddatetime" class="col-xs-4 col-form-label">~</label>
            <div class="col-xs-8">
              <div class='input-group date' id='datetimepicker2'>
                  <input type="text" class="form-control" id="enddatetime" name="enddatetime" value="@if (isset($old_req)) {{$old_req->enddatetime}} @endif">
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="startduration" class="col-xs-4 col-form-label">{!! trans('app.duration') !!}</label>
            <div class="col-xs-8">
              <div class='input-group date' id='datetimepicker3'>
                  <input type="text" class="form-control" id="startduration" name="startduration" value="@if (isset($old_req->startduration)) {{$old_req->startduration}} @endif"> {{-- @if (isset($old_req)) {{$old_req->startduration}} @endif --}}
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-time"></span>
                  </span>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="endduration" class="col-xs-4 col-form-label">~</label>
            <div class="col-xs-8">
              <div class='input-group date' id='datetimepicker4'>
                  <input type="text" class="form-control" id="endduration" name="endduration" value="@if (isset($old_req->endduration)) {{$old_req->endduration}} @endif"> {{--@if (isset($old_req)) {{$old_req->endduration}} @endif--}}
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-time"></span>
                  </span>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="localparty" class="col-xs-4 col-form-label">{!! trans('app.localparty') !!}</label>
            <div class="col-xs-8">
              <input type="text" class="form-control" id="localparty" name="localparty" value="@if (isset($old_req)) {{$old_req->localparty}} @endif">
            </div>
          </div>
          <div class="form-group row">
            <label for="remoteparty" class="col-xs-4 col-form-label">{!! trans('app.remoteparty') !!}</label>
            <div class="col-xs-8">
              <input type="text" class="form-control" id="remoteparty" name="remoteparty" value="@if (isset($old_req)) {{$old_req->remoteparty}} @endif">
            </div>
          </div>
          <fieldset class="form-group">
            <div class="row">
              <div class="col-form-label col-xs-4 pt-0">{!! trans('app.direction') !!}</div>
              <div class="col-xs-8">
                <div class="form-check" style="display:inline;">
                  <input class="form-check-input" type="checkbox" name="direction_in" id="direction_in" value="in" @if (isset($old_req)) {{$old_req->direction_in=="in"?"checked":""}} @endif>
                  <label class="form-check-label" for="direction_in">
                    {!! trans('app.incoming') !!}
                  </label>
                </div>
                <div class="form-check" style="display:inline;">
                  <input class="form-check-input" type="checkbox" name="direction_out" id="direction_out" value="out" @if (isset($old_req)) {{$old_req->direction_out=="out"?"checked":""}} @endif>
                  <label class="form-check-label" for="direction_out">
                    {!! trans('app.outgoing') !!}
                  </label>
                </div>
              </div>
            </div>
          </fieldset>
            @if ($config["player_value"]==0 && ($config["app_type"]==1 || $config["app_type"]==2))
              <div class="form-group row">
                <!--<div class="col-xs-2">Checkbox</div>-->
                <div class="col-xs-8">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="direction_vr" value="vr" @if (isset($old_req)) {{$old_req->direction_vr=="vr"?"checked":""}} @endif>
                    <label class="form-check-label" for="direction_vr">
                      {!! trans('app.voicerecorder') !!}
                    </label>
                  </div>
                </div>
                <div class="col-xs-8">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="direction_all" value="all"  @if (isset($old_req)) {{$old_req->direction_all=="all"?"checked":""}} @endif>
                    <label class="form-check-label" for="direction_all">
                      {!! trans('app.all') !!}
                    </label>
                  </div>
                </div>
              </div>
            @endif
            <div class="form-group row">
              <div class="col-xs-4">{!! trans('app.download') !!}</div>
              <div class="col-xs-8">
                <div class="form-check" style='display:inline;'>
                  <input class="form-check-input" type="checkbox" id="download_act" name="download_act" value="act" @if (isset($old_req)) {{$old_req->download_act=="act"?"checked":""}} @endif>
                  <label class="form-check-label" for="direction_act">
                    {!! trans('app.ok') !!}
                  </label>
                </div>
                <div class="form-check" style='display:inline;'>
                  <input class="form-check-input" type="checkbox" id="download_non" name="download_non" value="non" @if (isset($old_req)) {{$old_req->download_non=="non"?"checked":""}} @endif>
                  <label class="form-check-label" for="direction_non">
                  {!! trans('app.non') !!}
                  </label>
                </div>
              </div>
            </div>
          <div class="form-group row">
            <div class="col-xs-4">{!! trans('app.tag') !!}</div>
            <div class="col-xs-8">
              <select name="selected_tag" style="border:1px solid lightgray;width:100%;border-radius:4px;">
                  <option value="0">----</option>
                @foreach ($tag_cat as $cat)
                  <option value="{{$cat->id}}" @if (isset($old_req)) {{$old_req->selected_tag==$cat->id?"selected":""}} @endif>{{$cat->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-xs-4">{!! trans('app.group') !!}</div>
            <div class="col-xs-8">
              <select name="selected_group" style="border:1px solid lightgray;width:100%;border-radius:4px;">
                  <option value="-1">----</option>
                @foreach ($tag_group as $group)
                  <option value="{{$group->id}}" @if (isset($old_req)) {{$old_req->selected_group==$group->id?"selected":""}} @endif>{{$group->group_name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-xs-4">{!! trans('app.searchpage') !!}</div>
            <div class="col-xs-8">
              <select name="selected_page" style="border:1px solid lightgray;width:100%;border-radius:4px;">
                  <option value="20" @if (isset($old_req)) {{$old_req->selected_page=="20"?"selected":""}} @endif>20</option>
                  <option value="50" @if (isset($old_req)) {{$old_req->selected_page=="50"?"selected":""}} @endif>50</option>
                  <option value="100" @if (isset($old_req)) {{$old_req->selected_page=="100"?"selected":""}} @endif>100</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-xs-4">{!! trans('app.text') !!}</div>
            <div class="col-xs-8">
                  <input type="text" class="form-control" id="freeword" name="freeword" value="@if (isset($old_req)) {{$old_req->freeword}} @endif">
                  <input class="form-check-input" type="radio" id="and_or1" name="and_or" value="and" @if (isset($old_req)) {{$old_req->and_or=="and"?"checked":""}} @endif>
                  <label class="form-check-label" for="and_or1">
                    AND
                  </label>
                  <input class="form-check-input" type="radio" id="and_or2" name="and_or" value="or" @if (isset($old_req)) {{$old_req->and_or=="or"?"checked":""}} @endif>
                  <label class="form-check-label" for="and_or2">
                    OR
                  </label>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-xs-6">
              <a href="#" name="btn_search" id="btn_search" class="btn btn-primary pull-right" ><img src="{!! asset('images/search_16.png') !!}" title="Search Button" /><span class="form_btn">{!! trans('app.search') !!}</span></a>
            </div>
            <div class="col-xs-6">
              <a href="#" name="btn_clear" id="btn_clear" class="btn btn-primary"><img src="{!! asset('images/clear_16.png') !!}" title="Clear Button" /><span class="form_btn">{!! trans('app.clear') !!}</span></a>
            </div>
          </div>
        </form>
      </div> <!--endof panel-body css class -->
  </div> <!-- end of panel css class -->
    @if ($config["bulk_dl_limit"] > 0 && $data["authority_bulk_download"] == 1) <!--$authority_bulk_download is received from the "youwire_security_group" DBtable -->
        <h3>{!! trans('app.bulkdownloadlimit') !!}</h3>
        <form id="sum" name="sum">
          <div class="form-group row">
            <div class="col-xs-12">
              <div id="body">
                  残り選択可能通話時間<div id="duration_sum"></div>
              </div>
            </div>
          </div>
        </form>
    @endif

    @yield('status_panel')

    @if ($config["status_count"])
      <div class="panel panel-primary">
        <div class="panel-heading">{!! trans('app.resultstatus') !!}</div>
        <div class="panel-body">
            {!! trans('app.deletetarget') !!}{{$config["status_count"][0]}} 件
            <ul>
                @foreach ($config["status_count"] as $key => $value)
                    @if ($key == Config::get('youwireConfig.delete_success'))
                      <li>{!! trans('app.deletesuccess') !!}{{$value}}件</li>
                    @endif
                    @if ($key == Config::get('youwireConfig.delete_file_fail'))
                      <li style="color:red;">{!! trans('app.deletefilefailure') !!}{{$value}}件</li>
                    @endif
                    @if ($key == Config::get('youwireConfig.delete_db_failed'))
                      <li style="color:red;">{!! trans('app.deletedbfailure') !!}{{$value}}件</li>
                    @endif
                    @if ($key == Config::get('youwireConfig.delete_protected'))
                      <li style="color:blue;">{!! trans('app.protect') !!}{{$value}}件</li>
                    @endif
                @endforeach
            </ul>
        </div>
      </div>
    @endif

    <!--jQuery Script and reqiered jQuery CDN for date picker-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                  viewMode: 'days',
                  format: 'YYYY-MM-DD HH:mm'
                });
                $('#datetimepicker2').datetimepicker({
                  viewMode: 'days',
                  format: 'YYYY-MM-DD HH:mm'
                });
                $('#datetimepicker3').datetimepicker({
                  format: 'HH:mm:ss'
                });
                $('#datetimepicker4').datetimepicker({
                  format: 'HH:mm:ss'
                });
                $('#btn_search').click(function() {
                  $('#serch_form').attr('action', '/search');
                  $('#serch_form').attr('method', 'GET');
                  $('#serch_form').submit();
                  return false;
                });
                $('#btn_clear').click(function() {
                  $(this).closest("form").find("textarea,:text,select").val("").end().find(":checked").not('[name="and_or"]').prop("checked",false);
                  $('#selected_tag').val("");
                  $('#selected_group').val("");
                  $('#startduration').val("00:00:00");
                  $('#endduration').val("00:00:00");
                  return false;
                });
            });
    </script>
<!--left panel end-->
