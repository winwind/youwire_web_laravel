<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <img src="{{ asset('images/login_logo.png') }}" alt="Youwire"/>
                </div>

                <div class="links">
                  <div class="panel panel-default">

                      <div class="panel-body">
                          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                              {{ csrf_field() }}

                              <div class="form-group{{ $errors->has('uname') ? ' has-error' : '' }}">
                                  <label for="uname" class="col-md-4 control-label">ユーザー名</label>

                                  <div class="col-md-6">
                                      <input id="uname" type="text" class="form-control" name="uname" value="{{ old('uname') }}" required autofocus>

                                      @if ($errors->has('uname'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('uname') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                  <label for="password" class="col-md-4 control-label">パスワード</label>

                                  <div class="col-md-6">
                                      <input id="password" type="password" class="form-control" name="password" required>

                                      @if ($errors->has('password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <div class="checkbox">
                                          <label>
                                              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 覚えている
                                          </label>
                                      </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <div class="col-md-8 col-md-offset-4">
                                      <button type="submit" class="btn btn-primary">
                                          ログイン
                                      </button>

                                      <a class="btn btn-link" href="{{ route('password.request') }}">
                                          パスワードをお忘れですか
                                      </a>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </body>
</html>
