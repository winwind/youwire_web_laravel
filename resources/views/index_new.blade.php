<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <title>Login</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
    <link rel="stylesheet" href="stylesheet.css">
    <link type="text/css" href="css/app.css" rel="stylesheet"/>
    <!-- end of global css -->
    <!--page level css -->
    <link href="vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="vendors/gridforms/css/gridforms.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <link rel="stylesheet" type="text/css" href="css/form_layouts.css">
    <!--end of page level css-->
  </head>

  <body>
     <div style="margin:60px auto; width:600px">
        <div>
          <img class="youwire-logo" src="images/logo.png" style="width:600px">
        </div>
         <div class="card-body" style="width:300px; margin:0 auto;">
             <form method="POST" action="{{ route('login') }}">
               {{ csrf_field() }}
                 <div class="form-group">
                     <label for="uname">ユーザー名</label>
                     <input type="text" class="form-control" name="uname" value="{{ old('uname') }}" id="uname" required autofocus>
                     <!--ここにエラーチェック入れる-->
                 </div>
                 <div class="form-group">
                     <label for="password">パスワード</label>
                     <input type="password" class="form-control" id="password" name="password" required>
                     <!--ここにエラーチェック入れる-->
                 </div>

                 <div class="form-group">
                   <div class="checkbox">
                     <label>
                         <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}　style="margin-right: 7px;">
                          ログイン情報を記憶する
                     </label>
                  </div>
                </div>

                <div class="form-group">
                     <button type="submit" class="btn btn-primary m-t-10">ログイン
                     </button>
                     <!--a class="btn btn-link" href="{{ route('password.request') }}">
                                          パスワードをお忘れですか
                                      </a-->
                 </div>
             </form>
         </div>
      </div>
  </body>

  <footer class="footer text-white" style="background-color:#428BCA;">
      <div class="row">
          <div class="" style="margin: 0 auto; margin-top:10px;">
              <p class="">Copyright © 2012-2018 GeekFeed Co.,Ltd. All rights reserved</p>
          </div>
  </footer>
  <!-- global js -->
  <script src="js/app.js" type="text/javascript"></script>
  <!-- end of global js -->
  <!-- begining of page level js -->
  <!-- page level js-->
  <script src="vendors/iCheck/js/icheck.js" type="text/javascript"></script>
  <script src="js/custom_js/form_layouts.js" type="text/javascript"></script>
  <!-- end of page level js -->
</html>
