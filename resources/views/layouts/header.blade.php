<!DOCTYPE html>
<html>
  <head>
    @yield('head')
  </head>

 <body class="skin-coreplus">
  <!--ヘッダー-->
  <header>
    <div style="margin:20px">
      <img class="youwire-logo" src="images/logo.png" style="width:350px;">
      <div id="language_switcher" class="col-xs-2 pull-right" style="margin-top:25px;"> <!--language switch form -->
                    <form action="/language" method="post">
                        <select name="locale" style="border:10px;" class="">
                        <option value="en" {{App::getLocale()=='en'?'selected':''}}>English</option>
                        <option value="jp" {{App::getLocale()=='jp'?'selected':''}}>日本語</option>
                        </select>
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-primary btn-xs" value="change" />
                    </form>
        </div>
    </div>

    <div class="text-white" style="float:right; padding:20px;">
      {!! trans('app.connectionip') !!} : @foreach ($ips as $ip) {{$ip}} @endforeach||
      @if (!Auth::guest())
        {!! trans('app.loginuser') !!} : {{Auth::user()->name}}
      || {!! trans('app.lastlogin') !!} : {{Session::get('last_login')}}
      @endif
    </div>
    <section class="content-header">
      <div class="card-header text-white bg-primary">
           <div class="input-group-append dropdown">
               <a href="/welcome" class="btn menu-button btn-primary card-title"><i class="fa fa-fw fa-home"></i>TOP</a>

                <button class="btn menu-button btn-primary dropdown-toggle dropdown" type="button" data-toggle="dropdown">
                  {!! trans('app.adminmenu') !!}
                </button>
                @if($data['authority_edit_tag_category'] == 1 || $data['authority_edit_security_group'] == 1 || $data['authority_edit_group'] == 1 || $data['authority_edit_user'] == 1 || $data['authority_edit_alertmail'] == 1)
                  <ul class="dropdown-menu">
                    @if($data['authority_edit_tag_category'] == 1)
                      <li class="dropdown-item"><a href="/tagCategoryView">{!! trans('app.createtagcat') !!}</a></li>
                    @endif
                    @if($data['authority_edit_group'] == 1)
                      <li class="dropdown-item"><a href="/group">{!! trans('app.groupregistration') !!}</a></li>
                    @endif
                    @if($data['authority_edit_user'] == 1)
                      <li class="dropdown-item"><a href="/user_record">{!! trans('app.registerrecordingnumber') !!}</a></li>
                      <li class="dropdown-item"><a href="/user_search">{!! trans('app.searchuserregistration') !!}</a></li>
                    @endif
                    @if($data['authority_edit_security_group'] == 1)
                      <li class="dropdown-item"><a href="/security_group">{!! trans('app.registeroperationauthority') !!}</a></li>
                    @endif
                    <!--アラートメール-->
                    @if($data['authority_edit_alertmail'] == 1)
                      <li class="dropdown-item"><a href="/alertmailView">{!! trans('app.alertemailsettings') !!}</a></li>
                    @endif
                    <!--操作ログ-->
                    @if($data['authority_edit_tag_category'] == 1 && $data['authority_edit_security_group'] == 1 && $data['authority_edit_group'] == 1 && $data['authority_edit_user'] == 1 && $data['authority_edit_alertmail'] == 1)
                      <li class="dropdown-item"><a href="/operationlog">{!! trans('app.operationlog') !!}</a></li>
                     @endif
                  @endif
                </ul>

                <a href="/user_setting" class="btn menu-button btn-primary card-title">{!! trans('app.changepw') !!}</a>
                <a href="/" class="btn menu-button btn-primary card-title">{!! trans('app.logout') !!}</a>
                <a href="/admin" class="btn menu-button btn-primary card-title">{!! trans('app.managementscreen') !!}</a>

            </div>
      </div>
      </section>
    </header>
    <div class="wrapper" style="margin-top:25px;margin-left:30px;margin-left:30px;">
      @yield('contents')
    </div>
  </body>
</html>
