<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{!! trans('app.youwire') !!}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!--CSS for datetime picker-->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

        <!-- main bootstro css (app.css) and custom css (style.css) -->
        <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />-->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />

        <!-- menu bar custom stylesheet -->
        <link href="{{ asset('css/menu_style.css') }}" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <div class="container-fluid">
          <div id="top" class="row"> <!--top most hrizontal row -->
            <div id="top_row_1" class="row"> <!-- login user details row -->
                <div id="login_user" class="col-xs-10">
                    @if (!Auth::guest())
                      {!! trans('app.loginuser') !!} : {{Auth::user()->name}} | {!! trans('app.connectionip') !!}: @yield('ip') | {!! trans('app.lastlogin') !!} : {{Session::get('last_login')}}
                    @endif
                </div>
                <div id="language_switcher" class="col-xs-2"> <!--language switch form -->
                  <form action="/language" method="post">
                      <select name="locale" style="border:0px;">
                      <option value="en" {{App::getLocale()=='en'?'selected':''}}>English</option>
                      <option value="jp" {{App::getLocale()=='jp'?'selected':''}}>日本語</option>
                      </select>
                      {{ csrf_field() }}
                      <input type="submit" class="btn btn-default btn-xs" value="change" />
                  </form>
                </div>
            </div>
            <div id="top_row_2" class="row"> <!-- Top logo and menu -->
                <div id="logo" class="col-xs-4"> <!--logo-->
                  <img src="{{ asset('images/youwire_logo.gif') }}" alt="Youwire Logo"/>
                </div>
                <div id="menu" class="col-xs-8 bg-default"> <!-- menu bar -->
                   @include('inc.menu_bar')
                </div>
            </div>
          </div>
          <div id="bottom" class="row">
            <div id="left" class="col-xs-3">
                <!--left panel start -->
                @yield('leftpanel')
                <!--left panel end-->
            </div>
            <div id="content" class="col-xs-9">
                @yield('content')
            </div>
          </div>
        </div>
    </body>
</html>
