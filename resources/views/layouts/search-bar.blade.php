<!DOCTYPE html>
@extends('view.welcome')

@section('search-bar')
<!--section id="sidebar-search" style="width:300px;"-->
<div id="sidebar-search" style="width:300px;">
  <!--検索タブ-->
    <div class="col-12">
        <div class="card border-primary ">
          <div class="card-header bg-primary text-white">
              <h3 class="card-title d-inline">
                  <i class="fa fa-fw fa-search"></i>{!! trans('app.search') !!}
              </h3>
              <span class="pull-right hidden-xs">
                <i class="fa fa-fw fa-chevron-up clickable"></i>
                <i class="fa fa-fw fa-times removepanel"></i>
               <!--button class="btn btn-primary pull-right"  id="removetab">
                <i class="fa fa-fw fa-times"></i>
              </button-->
              </span>
          </div>
          <div class="card card-body">
              <form action="#"id="serch_form" class="">
                {{ csrf_field() }}
                  <!--div class="form-body box-body"-->
                  <div class="box-body" style="margin-left:5px;">
                    <!--div class="form-group row">
                           <label class="col-12 col-form-label control-label text-left">
                               通話時間：
                           </label>
                         <div class="col-12">
                           <div class="input-group input-group-prepend">
                               <div class="input-group-text border-right-0 rounded-0">
                                   <i class="fa fa-fw fa-clock-o"></i>
                               </div>
                               <input type="text" class="form-control" id="reservationtime"
                                      placeholder="MM/DD/YYYY HH:MM-MM/DD/YYYY HH:MM">
                           </div>
                       </div>
                     </div-->
                     <!--通話日-->
                     <div class="form-group row">
                        <label for="startdatetime" class="col-xs-4 col-12 col-form-label">{!! trans('app.callstart') !!}</label>
                        <div class="col-xs-8 col-11">
                          <div class='input-group date' id='datetimepicker1'>
                              <input type="text" class="form-control" id="startdatetime" name="startdatetime" value="@if (isset($old_req)) {{$old_req->startdatetime}} @endif">
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="enddatetime" class="col-xs-4 col-1 col-form-label">~</label>
                        <div class="col-xs-8 col-10">
                          <div class='input-group date' id='datetimepicker2'>
                              <input type="text" class="form-control" id="enddatetime" name="enddatetime" value="@if (isset($old_req)) {{$old_req->enddatetime}} @endif">
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                      <!--通話開始/終わり-->
                      <div class="form-group row">
                        <label for="startduration" class="col-xs-4 col-form-label">{!! trans('app.duration') !!}</label>
                        <div class="col-xs-8">
                          <div class='input-group date' id='datetimepicker3'>
                              <input type="text" class="form-control" id="startduration" name="startduration" value="@if (isset($old_req->startduration)) {{$old_req->startduration}} @endif"> {{-- @if (isset($old_req)) {{$old_req->startduration}} @endif --}}
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-time"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="endduration" class="col-xs-4 col-form-label">~</label>
                        <div class="col-xs-8">
                          <div class='input-group date' id='datetimepicker4'>
                              <input type="text" class="form-control" id="endduration" name="endduration" value="@if (isset($old_req->endduration)) {{$old_req->endduration}} @endif"> {{--@if (isset($old_req)) {{$old_req->endduration}} @endif--}}
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-time"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                    <!--通話時間ここまで-->

                     <div class="form-group row">
                         <label for=rightinputnumber class="col-12 col-form-label text-left">
                             {!! trans('app.localparty') !!}
                         </label>
                         <div class="col-12">
                             <div class="input-group input-group-prepend" >
                                 <span class="input-group-text border-right-0 rounded-0">
                                 <i class="fa fa-fw fa-phone"></i>
                             </span>
                                 <input type="text"  name="localparty" value="@if (isset($old_req)) {{$old_req->localparty}} @endif"
                                         id="localparty" class="form-control"/>
                             </div>
                         </div>
                     </div>
                     <div class="form-group row">
                         <label for="inputnumber" class="col-12 col-form-label text-left">
                           {!! trans('app.remoteparty') !!}
                         </label>
                           <div class="col-12">
                             <div class="input-group input-group-prepend">
                                 <span class="input-group-text border-right-0 rounded-0">
                                 <i class="fa fa-fw fa-phone"></i>
                             </span>
                                 <input type="text" class="form-control" id="remoteparty"
                                 name="remoteparty" value="@if (isset($old_req)) {{$old_req->remoteparty}} @endif"/>
                             </div>
                           </div>

                        </div>
                        <div class="form-group row">
                              <label style="padding-right:100px">
                                　{!! trans('app.direction') !!}
                              </label>
                                   <div class="col-6 py-1">

                                          <div class="form-check abc-checkbox form-check-inline">
                                              <input class="form-check-input" type="checkbox" name="direction_in"
                                               id="direction_in" value="in" @if (isset($old_req)) {{$old_req->direction_in=="in"?"checked":""}} @endif>
                                              <label for="inlineCheckbox1">{!! trans('app.incoming') !!} </label>
                                          </div>
                                      </div>
                                    <div class="col-6 py-1">

                                          <div class="form-check abc-checkbox form-check-inline">
                                              <input class="form-check-input" type="checkbox" name="direction_out"
                                              id="direction_out" value="out" @if (isset($old_req)) {{$old_req->direction_out=="out"?"checked":""}} @endif>
                                              <label for="inlineCheckbox2">{!! trans('app.outgoing') !!} </label>
                                          </div>
                                      </div>
                         </div>
                         <!--ここにボイスレコーダ項目を追加する-->
                         @if ($config["player_value"]==0 && ($config["app_type"]==1 || $config["app_type"]==2))
                          <div class="form-group row">
                            <!--<div class="col-xs-2">Checkbox</div>-->
                            <div class="col-xs-8">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="direction_vr" value="vr" @if (isset($old_req)) {{$old_req->direction_vr=="vr"?"checked":""}} @endif>
                                <label class="form-check-label" for="direction_vr">
                                  {!! trans('app.voicerecorder') !!}
                                </label>
                              </div>
                            </div>
                            <div class="col-xs-8">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="direction_all" value="all"  @if (isset($old_req)) {{$old_req->direction_all=="all"?"checked":""}} @endif>
                                <label class="form-check-label" for="direction_all">
                                  {!! trans('app.all') !!}
                                </label>
                              </div>
                            </div>
                          </div>
                        @endif
                        <div class="form-group row">
                          <label style="padding-right:40px">
                            　{!! trans('app.download') !!}
                          </label>
                             <div class="col-6 py-1">
                               <div class="form-check abc-checkbox form-check-inline">
                                   <input class="form-check-input" type="checkbox" id="download_act"
                                   name="download_act" value="act" @if (isset($old_req)) {{$old_req->download_act=="act"?"checked":""}} @endif>
                                   <label for="inlineCheckbox1">{!! trans('app.ok') !!}</label>
                               </div>
                             </div>
                             <div class="col-6 py-1">
                                <div class="form-check abc-checkbox form-check-inline">
                                   <input class="form-check-input" type="checkbox" id="download_non"
                                   name="download_non" value="non" @if (isset($old_req)) {{$old_req->download_non=="non"?"checked":""}} @endif>
                                   <label for="inlineCheckbox2">{!! trans('app.non') !!}</label>
                                </div>
                               </div>
                         </div>

                         <div class="form-group row">
                                <label class="col-12 col-form-label text-left">
                                  {!! trans('app.tag') !!}
                                </label>
                                <div class="col-12 ">
                                  <select name="selected_tag" class="form-control">
                                      <option value="0">----</option>
                                      @foreach ($tag_cat as $cat)
                                        <option value="{{$cat->id}}" @if (isset($old_req)) {{$old_req->selected_tag==$cat->id?"selected":""}} @endif>{{$cat->name}}</option>
                                      @endforeach
                                  </select>
                                </div>
                          </div>
                          <div class="form-group row">
                                 <label class="col-12 col-form-label text-left">
                                   {!! trans('app.group') !!}
                                 </label>
                                 <div class="col-12">
                                   <select name="selected_group" class="form-control">
                                       <option value="-1">----</option>
                                       @foreach ($tag_group as $group)
                                          <option value="{{$group->id}}" @if (isset($old_req)) {{$old_req->selected_group==$group->id?"selected":""}} @endif>{{$group->group_name}}</option>
                                        @endforeach
                                   </select>
                                 </div>
                           </div>
                           <div class="form-group row">
                                  <label class="col-12 col-form-label text-left">
                                    {!! trans('app.searchpage') !!}
                                  </label>
                                  <div class="col-12 ">
                                    <select name="selected_page" class="form-control">
                                      <option value="20" @if (isset($old_req)) {{$old_req->selected_page=="20"?"selected":""}} @endif>20</option>
                                      <option value="50" @if (isset($old_req)) {{$old_req->selected_page=="50"?"selected":""}} @endif>50</option>
                                      <option value="100" @if (isset($old_req)) {{$old_req->selected_page=="100"?"selected":""}} @endif>100</option>
                                    </select>
                                  </div>
                            </div>
                            <!--フリーワード検索-->
                            <div class="form-group row">
                              <div class="col-xs-4">{!! trans('app.text') !!}</div>
                              <div class="col-xs-8">
                                    <input type="text" class="form-control" id="freeword" name="freeword" value="@if (isset($old_req)) {{$old_req->freeword}} @endif">
                                    <input class="form-check-input" type="radio" id="and_or1" name="and_or" value="and" @if (isset($old_req)) {{$old_req->and_or=="and"?"checked":""}} @endif>
                                    <label class="form-check-label" for="and_or1">
                                      AND
                                    </label>
                                    <input class="form-check-input" type="radio" id="and_or2" name="and_or" value="or" @if (isset($old_req)) {{$old_req->and_or=="or"?"checked":""}} @endif>
                                    <label class="form-check-label" for="and_or2">
                                      OR
                                    </label>
                              </div>
                            </div>

                        <div>
                            <div class="row">
                                <div class="col-12">

                                    <button type="button" name="btn_search"  id="btn_search" class="btn btn-primary">
                                      {!! trans('app.search') !!}
                                    </button>
                                    &nbsp;
                                    <button type="button"　name="btn_clear" id="btn_clear" class="btn btn-default bttn_reset">
                                      {!! trans('app.clear') !!}
                                    </button>
                                </div>
                            </div>
                        </div>
                     </div>
                 </form>
              </div>
             </div>
         </div>
        <!--検索タブ終わり-->

      <!--/section-->
      </div>
    @endsection
