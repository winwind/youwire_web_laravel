<!DOCTYPE html>
@extends('layouts.header')

@section('ip')
    @foreach ($ips as $ip)
      {{$ip}}
    @endforeach
@endsection

  @section('head')
    <meta charset="UTF-8">
    <title>Operation Log</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="stylesheet" type="text/css" href="vendors/awesomebootstrapcheckbox/css/build.css">
    <link type="text/css" href="css/app.css" rel="stylesheet">
  　<link rel="stylesheet" type="text/css" href="css/formelements.css">
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" type="text/css" href="css/form_layouts.css">
    <!--datatables&timepicker-->
    <link href="css/datatable.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/jquery.datetimepicker.css"/ >
    <!--end of page level css-->
  @endsection

 @section('contents')
    <section class="content">
      <div class="row">
        <div class="col-lg-4  col-12">
              <div class="card border-primary">
                      <div class="card-header text-white bg-primary" style="position:relative;">
                          <h3 class="card-title d-inline">
                              {!! trans('app.operationsearch') !!}
                          </h3>
                      </div>
                      <div class="card-body">
                          <form name="serch_form" id="serch_form">
                            {{ csrf_field() }}
                              <div class="form-group row">
                                  <label for="inputEmail3" class="col-5">{!! trans('app.logsearchuser') !!} : </label>
                                  <input type="text" class="col-6" iname="uname" id="uname" value="@if (isset($old_req)) {{$old_req->uname}} @endif">
                              </div>
                              <div class="form-group row">
                                  <label for="inputPassword1" class="col-5">{!! trans('app.logsearchdate') !!} : </label>
                                  <input type="text" class="col-6" name="startdatetime" id="startdatetime" value="@if (isset($old_req)) {{$old_req->startdatetime}} @endif">
                              </div>
                              <div class="form-group row">
                                <div class="col-5"></div>
                                <div class="col-6">～</div>
                              </div>
                              <div class="form-group row">
                                  <div class="col-5"></div>
                                  <input type="text" class="col-6" name="enddatetime" id="enddatetime" value="@if (isset($old_req)) {{$old_req->enddatetime}} @endif">
                              </div>
                                <div class="col-12 pull-right">
                                    <div>
                                        <button type="button" name="btn_search" id="btn_search" class="btn btn-primary">
                                          {!! trans('app.search') !!}
                                        </button>
                                        &nbsp;
                                        <button type="button"　name="btn_clear" id="btn_clear" class="btn btn-default bttn_reset">
                                          {!! trans('app.clear') !!}
                                        </button>
                                    </div>
                               </div>
                          </form>
                      </div>
                  </div>
            </div>
            <div class="col-lg-8 col-12">
              <div class="card border-primary">
                <div class="card-header text-white bg-primary">
                    <h3 class="card-title d-inline">
                        <i class="fa fa-fw fa-list-alt"></i>{!! trans('app.searchresult') !!}
                    </h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <form name="list" id="list">
                      {{ csrf_field() }}
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                              <tr>
                                <th>{!! trans('app.logtimestamp') !!}</th>
                                <th>{!! trans('app.logsearchuser') !!}</th>
                                <th>{!! trans('app.logaccessurl') !!}</th>
                                <th>{!! trans('app.logdisplay') !!}</th>
                                <th>{!! trans('app.logaction') !!}</th>
                                <th>{!! trans('app.loginfo') !!}</th>
                              </tr>
                            </thead>
                            <tbody>
                              @if(isset($logListData))
                                @foreach ($logListData as $logData)
                                  <tr id="logid_{{$logData->log_id}}">
                                		<td>{{$logData->timestamp_date}}</br>{{$logData->timestamp_time}}</td>
                                		<td>{{$logData->uname}}</td>
                                		<td>{{$logData->access_url}}</td>
                                		<td>{{$logData->display}}</td>
                                		<td>{{$logData->action}}</td>
                                		<td>{{$logData->operation_info}}</td>
                                	</tr>
                                @endforeach
                                <div class="table-pages">
                                {{ $logListData->appends(request()->input())->links() }}
                              </div>
                              @endif
                          </tbody>
                        </table>
                      </form>
                    </div>
                </div>
            </div>
          </div>

      </div>

    </section>
@endsection

     <!-- global js -->
     <script src="js/app.js" type="text/javascript"></script>
     <!-- end of global js -->
     <!-- begining of page level js -->
     <!--datatables-->
     <script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
     <script type="text/javascript" src="js/custom_js/simple-table.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script src="/jquery.js"></script>
     <script src="/build/jquery.datetimepicker.full.min.js"></script>
     <script>
     $(function(){
      $('#startdatetime').datetimepicker();
      $('#enddatetime').datetimepicker();
     	$('#btn_search').click(function() {
     		$('#serch_form').attr('action', '/logsearch');
     		$('#serch_form').submit();
     		return false;
     	});
     	$('#btn_clear').click(function() {
     		$(this).closest("form").find("textarea,:text,select").val("").end().find(":checked").prop("checked",false);
     		return false;
     	});
     });
     </script>
     <!-- end of page level js -->
