@extends('layouts.app')

@section('ip')
    @foreach ($ips as $ip)
      {{$ip}}
    @endforeach
@endsection

@section('leftpanel')
  <div class="panel panel-primary">
    <div class="panel-heading">{!! trans('app.operationsearch') !!}</div>
    <div class="panel-body">
      <form name="serch_form" id="serch_form">
        {{ csrf_field() }}
          <table class="table">
            <tr>
      				<td>{!! trans('app.logsearchuser') !!} : </td>
      				<td><input type="text" name="uname" id="uname" value="@if (isset($old_req)) {{$old_req->uname}} @endif"></td>
      			</tr>
      			<tr>
      				<td>{!! trans('app.logsearchdate') !!}</td>
      				<td>
      					<input type="text" name="startdatetime" id="startdatetime" value="@if (isset($old_req)) {{$old_req->startdatetime}} @endif">
      				</td>
      			</tr>
      			<tr>
      				<td>&nbsp;</td>
      				<td>
      					<h1>~</h1>
      				</td>
      			</tr>
      			<tr>
      				<td>&nbsp;</td>
      				<td>
      					<input type="text" name="enddatetime" id="enddatetime" value="@if (isset($old_req)) {{$old_req->enddatetime}} @endif">
      				</td>
      			</tr>
          </table>
          <div class="form-group row">
            <div class="col-xs-6">
              <a name="btn_search" id="btn_search" class="btn btn-primary pull-right" ><img src="{!! asset('images/search_16.png') !!}" title="Search Button" /><span class="form_btn">{!! trans('app.search') !!}</span></a>
            </div>
            <div class="col-xs-6">
              <a name="btn_clear" id="btn_clear" class="btn btn-primary"><img src="{!! asset('images/clear_16.png') !!}" title="Clear Button" /><span class="form_btn">{!! trans('app.clear') !!}</span></a>
            </div>
          </div>
      </form>
    </div><!--end panel body-->
  </div><!--end panel-->
@endsection

@section('content')
  <div class="panel panel-primary">
    <div class="panel-heading">{!! trans('app.searchresult') !!}</div>
    <div class="panel-body">
      <form name="list" id="list">
        {{ csrf_field() }}
          <table class="table">
            <tr>
          		<th>{!! trans('app.logtimestamp') !!}</th>
          		<th>{!! trans('app.logsearchuser') !!}</th>
          		<th>{!! trans('app.logaccessurl') !!}</th>
          		<th>{!! trans('app.logdisplay') !!}</th>
          		<th>{!! trans('app.logaction') !!}</th>
          		<th>{!! trans('app.loginfo') !!}</th>
          	</tr>
            @if(isset($logListData))
              @foreach ($logListData as $logData)
                <tr id="logid_{{$logData->log_id}}">
              		<td>{{$logData->timestamp_date}}</br>{{$logData->timestamp_time}}</td>
              		<td>{{$logData->uname}}</td>
              		<td>{{$logData->access_url}}</td>
              		<td>{{$logData->display}}</td>
              		<td>{{$logData->action}}</td>
              		<td>{{$logData->operation_info}}</td>
              	</tr>
              @endforeach
              {{ $logListData->appends(request()->input())->links() }}
            @endif
          </table>

      </form>
    </div><!--end panel-body-->
  </div><!--end panel-->
@endsection


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(function(){
	$('#btn_search').click(function() {
		$('#serch_form').attr('action', '/logsearch');
		$('#serch_form').submit();
		return false;
	});
	$('#btn_clear').click(function() {
		$(this).closest("form").find("textarea,:text,select").val("").end().find(":checked").prop("checked",false);
		return false;
	});
});
</script>
