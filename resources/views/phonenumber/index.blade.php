<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->

<h1>phonenumber PAGE</h1>
<h2>識別番号一覧</h2>
<table class="table">
	<tr>
		<td>ID</td>
		<td>識別番号表示名</td>
		<td>識別番号</td>
		<td>操作</td>
		<td>操作</td>
	</tr>
	@foreach ($get_phones as $get_phone)
	<tr>
		<td>{{$get_phone->phone_number_id }}</td>
		<td>{{$get_phone->phone_number_name }}</td>
		<td>{{$get_phone->phone_number }}</td>
		<td><a href="{{ url('/Phonenumber/'.$get_phone->phone_number_id .'/edit')}}">編集</a></td>
		<td><a href="{{ url('/Phonenumber/destroy/'.$get_phone->phone_number_id )}}">削除</a></td>
	</tr>
	@endforeach
<!-- {{$uid = $get_phone->uid}} 必要ですが、消すのはダメ。 -->
</table>
<h3>録音制御設定</h3>
<table class="table">
	@foreach ($get_profiles as $get_profile)
		<tr><td>録音番号{{$get_profile->uname }}</td></tr>
		<tr><td>録音番号表示名{{$get_profile->name }}</td></tr>
		<tr><td>録音番号表示名 : {{$get_profile->record_control }}</td></tr>
	@endforeach
	<!-- {{$test = $get_profile->record_control}} 必要ですが、消すのはダメ。 -->
</table>

{!! Form::open(['url' => '/Phonenumber/edituser', 'method' => 'get']) !!} 
<?= Form::hidden('uid', $uid); ?>
<?= Form::select('record_control', [0 => '録音制御なし', 1 => '識別番号を録音拒否', 2 => '識別番号を録音許可'], $test); ?>
<?= Form::submit('更新', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!}


<!-- Create form start 2018/09/10 -->
<hr>
<h2>識別番号設定</h2>
{!! Form::open(['url' => '/Phonenumber/create', 'method' => 'get']) !!} 
<?= Form::hidden('uid', $uid); ?>
<?= Form::label('phone_number_name', '識別番号表示名'); ?>
<?= Form::text('phone_number_name', null, ['class' => 'form-control', 'placeholder' => '識別番号表示名']); ?>
<?= Form::label('phone_number', '識別番号'); ?>
<?= Form::number('phone_number', null, ['class' => 'form-control', 'placeholder' => '識別番号']); ?>
<?= Form::submit('新規登録', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>