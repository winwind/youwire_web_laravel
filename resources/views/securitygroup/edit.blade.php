<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->


<h1>EDIT securitygroup PAGE</h1>
{!! Form::model($get_securitygroup, ['url' => 'security_group/'.$get_securitygroup->security_group_id, 'method' => 'get']) !!}
<?= Form::label('security_group_id', 'security_group_id'); ?>
<h3>{{$get_securitygroup->security_group_id}}</h3>
<?= Form::label('security_group_name', 'security_group_name'); ?>
<?= Form::text('security_group_name', null, ['class' => 'form-control', 'placeholder' => 'group_name','maxlength' => 32]); ?><br>
<!-- operating_range -->
<?= Form::label('operating_range', 'operating_range'); ?>
<?= Form::select('operating_range', [ 1 => '自分のみ',
									  2 => 'グループ内のみ',
									  3 =>  'グループ配下',
									  4 =>  '全体',
									  5 =>  'なし']); ?><br>
<!-- authority items -->
<?= Form::label('authority_search', 'authority_search'); ?>
<?= Form::checkbox('authority_search', true); ?><br>
<?= Form::label('authority_delete', 'authority_delete'); ?> 
<?= Form::checkbox('authority_delete', true); ?><br>
<?= Form::label('authority_download', 'authority_download'); ?> 
<?= Form::checkbox('authority_download', true); ?><br>
@if ($bulk_dl_limit > 0 )
<?= Form::label('authority_bulk_download', 'authority_bulk_download'); ?> 
<?= Form::checkbox('authority_bulk_download', true); ?><br>
@endif
<?= Form::label('authority_protect', 'authority_protect'); ?>
<?= Form::checkbox('authority_protect', true); ?><br>
<?= Form::label('authority_add_tag', 'authority_add_tag'); ?> 
<?= Form::checkbox('authority_add_tag', true); ?><br>
<?= Form::label('authority_edit_tag_category', 'authority_edit_tag_category'); ?> 
<?= Form::checkbox('authority_edit_tag_category', true); ?><br>
<?= Form::label('authority_edit_security_group', 'authority_edit_security_group'); ?> 
<?= Form::checkbox('authority_edit_security_group', true); ?><br>
<?= Form::label('authority_edit_group', 'authority_edit_group'); ?> 
<?= Form::checkbox('authority_edit_group', true); ?><br>
<?= Form::label('authority_edit_user', 'authority_edit_user'); ?> 
<?= Form::checkbox('authority_edit_user', true); ?><br>
<?= Form::label('authority_edit_alertmail', 'authority_edit_alertmail'); ?>  
<?= Form::checkbox('authority_edit_alertmail', true); ?>

<?= Form::submit('更新', ['class' => 'btn btn-primary']); ?>  
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>