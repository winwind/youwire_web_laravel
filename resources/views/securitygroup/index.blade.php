<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->


<h1>SECURITY GROUP PAGE</h1>
<table class="table">
	<tr>
		<th>ID</th>
		<th>操作権限名</th>
		<th>操作範囲</th>
		<th>検索</th>
		<th>削除</th>
		<th>ダウンロード</th>
		@if ($bulk_dl_limit > 0 )
		<th>一括ダウンロード</th>
		@endif
		<th>保護</th>
		<th>タグ付与</th>
		<th>タグカテゴリ編集</th>
		<th>操作権限編集</th>
		<th>グループ編集</th>
		<th>ユーザー編集</th>
		<th>アラートメール設定</th>
		<th colspan="2">操作</th>
	</tr>
	@foreach ($get_securitygroups as $get_securitygroup)
	<tr>
		<td>{{$get_securitygroup->security_group_id}}</td>
		<td>{{$get_securitygroup->security_group_name}}</td>
		<td>{{$get_securitygroup->operating_range}}</td>
		<td>{{$get_securitygroup->authority_search}}</td>
		<td>{{$get_securitygroup->authority_delete}}</td>
		<td>{{$get_securitygroup->authority_download}}</td>
		@if ($bulk_dl_limit > 0 )
		<td>{{$get_securitygroup->authority_bulk_download}}</td>
		@endif
		<td>{{$get_securitygroup->authority_protect}}</td>
		<td>{{$get_securitygroup->authority_add_tag}}</td>
		<td>{{$get_securitygroup->authority_edit_tag_category}}</td>
		<td>{{$get_securitygroup->authority_edit_security_group}}</td>
		<td>{{$get_securitygroup->authority_edit_group}}</td>
		<td>{{$get_securitygroup->authority_edit_user}}</td>
		<td>{{$get_securitygroup->authority_edit_alertmail}}</td>
		<td><a href="{{ url('/security_group/'.$get_securitygroup->security_group_id.'/edit')}}">編集</a></td>
		<td><a href="{{ url('/security_group/destroy/'.$get_securitygroup->security_group_id)}}">削除</a></td> 
	</tr>
	@endforeach
</table>

{!! Form::open(['url' => '/security_group/create', 'method' => 'get']) !!} 
<?= Form::label('security_group_name', 'security_group_name'); ?>
<?= Form::text('security_group_name', null, ['class' => 'form-control', 'placeholder' => 'group_name','maxlength' => 32]); ?><br>
<!-- operating_range -->
<?= Form::label('operating_range', 'operating_range'); ?>
<?= Form::select('operating_range', [ 1 => '自分のみ',
									  2 => 'グループ内のみ',
									  3 =>  'グループ配下',
									  4 =>  '全体',
									  5 =>  'なし']); ?><br>
<!-- authority items -->
<?= Form::label('authority_search', 'authority_search'); ?>
<?= Form::checkbox('authority_search', true); ?><br>
<?= Form::label('authority_delete', 'authority_delete'); ?> 
<?= Form::checkbox('authority_delete', true); ?><br>
<?= Form::label('authority_download', 'authority_download'); ?> 
<?= Form::checkbox('authority_download', true); ?><br>
@if ($bulk_dl_limit > 0 )
<?= Form::label('authority_bulk_download', 'authority_bulk_download'); ?> 
<?= Form::checkbox('authority_bulk_download', true); ?><br>
@endif
<?= Form::label('authority_protect', 'authority_protect'); ?>
<?= Form::checkbox('authority_protect', true); ?><br>
<?= Form::label('authority_add_tag', 'authority_add_tag'); ?> 
<?= Form::checkbox('authority_add_tag', true); ?><br>
<?= Form::label('authority_edit_tag_category', 'authority_edit_tag_category'); ?> 
<?= Form::checkbox('authority_edit_tag_category', true); ?><br>
<?= Form::label('authority_edit_security_group', 'authority_edit_security_group'); ?> 
<?= Form::checkbox('authority_edit_security_group', true); ?><br>
<?= Form::label('authority_edit_group', 'authority_edit_group'); ?> 
<?= Form::checkbox('authority_edit_group', true); ?><br>
<?= Form::label('authority_edit_user', 'authority_edit_user'); ?> 
<?= Form::checkbox('authority_edit_user', true); ?><br>
<?= Form::label('authority_edit_alertmail', 'authority_edit_alertmail'); ?>  
<?= Form::checkbox('authority_edit_alertmail', true); ?>

<?= Form::submit('新規登録', ['class' => 'btn btn-primary']); ?>  
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>