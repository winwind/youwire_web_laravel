<!DOCTYPE html>
@extends('layouts.header')
<html>
  <!--head-->
    @section('head')
    <meta charset="UTF-8">
    <title>Tag Category</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
    <link rel="stylesheet" href="stylesheet.css">
    <link type="text/css" href="css/app.css" rel="stylesheet">
    <!-- end of global css -->
  　<link rel="stylesheet" type="text/css" href="css/formelements.css">
    <link href="css/buttons_sass.css" rel="stylesheet">
    <link rel="stylesheet" href="stylesheet.css">
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" type="text/css" href="css/form_layouts.css">
    <!--datatables-->
    <link href="css/datatable.css" rel="stylesheet">
    <!--end of page level css-->
  @endsection

  @section('contents')
     <section class="content">
       <div class="row">
         <div class="col-lg-4  col-12">
              <div class="card border-primary">
                      <div class="card-header text-white bg-primary" style="position:relative;">
                          <h3 class="card-title d-inline">
                              {!! trans('app.createtag') !!}
                          </h3>
                          <!--span class="pull-right"-->
                                <button id="btn_add" class="btn btn-primary pull-right d-sm-block submit-group btn-submit">
                                  <i class="fa fa-fw fa-upload"></i>{!! trans('app.new') !!}
                                </button>
                                <button id="btn_update" class="btn btn-primary pull-right d-sm-block submit-group btn-submit hide">
                                  <i class="fa fa-fw fa-refresh"></i>{!! trans('app.edit') !!}
                                </button>
                          <!--/span-->
                      </div>
                      <div class="card-body">
                        <div id="message">
                         <!-- to display error and messages from TagCategoryController -->
                         @if(Session::has('msg'))
                             @if(Session::get('msg') == 4)
                               <div style="color:green">{!! trans('app.insertsuccess') !!}</div>
                             @endif
                             @if(Session::get('msg') == 5)
                               <div style="color:green">{!! trans('app.updatesuccess') !!}</div>
                             @endif
                             @if(Session::get('msg') == 1)
                               <div style="color:red">{!! trans('app.notagcatname') !!}</div>
                             @endif
                             @if(Session::get('msg') == 2)
                               <div style="color:red">{!! trans('app.japcharerror') !!}</div>
                             @endif
                             @if(Session::get('msg') == 3)
                               <div style="color:red">{!! trans('app.weighterror') !!}</div>
                             @endif
                             @if(Session::get('msg') == 6)
                               <div style="color:red">{!! trans('app.insertfail') !!}</div>
                             @endif
                             @if(Session::get('msg') == 7)
                               <div style="color:red">{!! trans('app.updatefail') !!}</div>
                             @endif
                             <div style="display:none;">{{Session::pull('msg')}}</div>
                           @endif
                         </div>
                          <form class="" id="list" name="list">
                            {{ csrf_field() }}
                              <div class="col-12 form-group row">
                                  <label class="col-6 text-right">{!! trans('app.catid') !!}:</label>
                                  <div class="col-6" id="tag_cat_edit_id" value=""></div>
                              </div>
                              <div class="col-12 form-group row">
                                  <label class="col-6 text-right">{!! trans('app.catname') !!}:</label>
                                  <input type="text" class="col-6" id="tag_category_name"
                                      name="tag_category_name">
                              </div>
                              <div class="col-12 form-group row">
                                  <label class="col-6 text-right">{!! trans('app.catsort') !!}:</label>
                                  <input type="text" class="col-6" id="tag_category_weight"
                                    name="tag_category_weight">
                              </div>
                              <input type="hidden" name="tag_category_id" id="tag_category_id">
                              <div>
                                  <div class="row">
                                      <div class="col-12">
                                          <button type="button"　name="tagcat_clear" id="tagcat_clear" class="btn pull-right btn-default">
                                            {!! trans('app.clear') !!}
                                          </button>
                                       </div>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
            </div>

            <div class="col-lg-8 col-12">
              <div class="card border-primary">
                <div class="card-header text-white bg-primary">
                    <h3 class="card-title d-inline">
                        <i class="fa fa-fw fa-list-alt"></i>{!! trans('app.catlist') !!}
                    </h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>{!! trans('app.catname') !!}</th>
                                <th>{!! trans('app.catsort') !!}</th>
                                <th>{!! trans('app.tagedit') !!}</th>
                                <th>{!! trans('app.tagdelete') !!}</th>
                            </tr>
                            </thead>
                            <tbody>
                              @foreach ($tagCatListData as $tagCatData)
                                <tr id="tagcatid_{{$tagCatData['id']}}">
                                  <td>{{$tagCatData['id']}}</td>
                                  <td id="tagcatname{{$tagCatData['id']}}">{{$tagCatData['name']}}</td>
                                  <td id="tagcatweight{{$tagCatData['id']}}">{{$tagCatData['weight']}}</td>
                                  <td>
                                       <a href="{{$tagCatData['id']}}" name="btn_edit{{$tagCatData['id']}}" class="btn btn-primary btn-xs btn_edit" id="btn_edit{{$tagCatData['id']}}" value="{{$tagCatData['id']}}"
                                            data-target="#edit" data-placement="top"><span
                                            class="fa fa-pencil"></span></a>
                                  </td>
                                  <td>
                                       <a href="{{$tagCatData['id']}}" name="btn_del{{$tagCatData['id']}}" id="btn_del{{$tagCatData['id']}}" value="{{$tagCatData['id']}}" class="btn btn-danger btn-xs btn_del"
                                            data-target="#delete" data-placement="top"><span
                                            class="fa fa-trash"></span></a>
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>

      </div>

    </section>
    @endsection

  <!--/div-->
      <style type="text/css">
         #message{
            text-align:center;
            padding-bottom:10px;
         }
         .btn-submit{
           position:absolute;
           top:3px;
           right:5px;
         }
         .hide{
           visibility:hidden;
         }
      </style>


     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script>
     $(function(){
       $('#tagcat_clear').click(function(){
         $('#tag_cat_edit_id').html("");
         $('#tag_category_weight').attr('value',"");
         $('#tag_category_name').attr('value',"");
         //$('#tag_category_weight').val("");
         //$('#tag_category_name').val("");
         $('#btn_update').addClass('hide');
         $('#btn_add').removeClass('hide');
       });
     	$('.btn_edit').click(function() {
         var tagcatid = $(this).attr("href");
     		$('#tag_cat_edit_id').html(tagcatid);
         $('#tag_category_name').attr('value',$('#tagcatname'+tagcatid).html());
         $('#tag_category_weight').attr('value',$('#tagcatweight'+tagcatid).html());
         $('#btn_update').removeClass('hide');
         $('#btn_add').addClass('hide');
     		return false;
     	});
     	$('#btn_add').click(function() {
     		if( confirm('{!! trans('app.tagcataddconfirm') !!}') ){
     			$('#list').attr('action', '/control_add');
     			$('#list').submit();
     		}
     		return false;
     	});
     	$('#btn_update').click(function() {
     		if( isNaN($('#tag_cat_edit_id').html()) ){
     			alert( '{!! trans('app.updatetagcatcheck') !!}' );
     		} else {
     			if( confirm('{!! trans('app.updatetagcatconfirm') !!}') ){
            $('#tag_category_id').attr('value',$('#tag_cat_edit_id').html());
     				$('#list').attr('action', '/control_update');
     				$('#list').submit();
     			}
     		}
     		return false;
     	});
     	$('.btn_del').click(function() {
     		if( confirm('{!! trans('app.tagcatdeleteconfirm') !!}') ){
     			$('#tag_category_id').attr('value',$(this).attr("href"));
     			$('#list').attr('action', '/control_delete');
     			$('#list').submit();
     		}
     		return false;
     	});

     });
     </script>
     <script type="text/javascript" src="script.js"></script>
     <!-- global js -->
     <script src="js/app.js" type="text/javascript"></script>
     <!-- end of global js -->
     <!-- begining of page level js -->
     <script src="js/custom_js/form_layouts.js" type="text/javascript"></script>
     <!--datatables-->
     <script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
     <script type="text/javascript" src="js/custom_js/simple-table.js"></script>
     <!-- end of page level js -->
  <!--/body>
 </html-->
