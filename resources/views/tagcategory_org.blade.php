@extends('layouts.headlayout')

@section('ip')
    @foreach ($ips as $ip)
      {{$ip}}
    @endforeach
@endsection

@section('leftpanel')
  <!-- to display error and messages from TagCategoryController -->
  @if(Session::has('msg'))
    @if(Session::get('msg') == 4)
      <div style="color:green">{!! trans('app.insertsuccess') !!}</div>
    @endif
    @if(Session::get('msg') == 5)
      <div style="color:green">{!! trans('app.updatesuccess') !!}</div>
    @endif
    @if(Session::get('msg') == 1)
      <div style="color:red">{!! trans('app.notagcatname') !!}</div>
    @endif
    @if(Session::get('msg') == 2)
      <div style="color:red">{!! trans('app.japcharerror') !!}</div>
    @endif
    @if(Session::get('msg') == 3)
      <div style="color:red">{!! trans('app.weighterror') !!}</div>
    @endif
    @if(Session::get('msg') == 6)
      <div style="color:red">{!! trans('app.insertfail') !!}</div>
    @endif
    @if(Session::get('msg') == 7)
      <div style="color:red">{!! trans('app.updatefail') !!}</div>
    @endif
  @endif

  <!-- new tag cat register and update panel -->
  <div class="panel panel-primary">
    <div class="panel-heading">{!! trans('app.createtag') !!}</div>
    <div class="btn-group">
      <button type="button" id="btn_add" name="btn_add" class="btn btn-primary">{!! trans('app.new') !!} <span class="glyphicon glyphicon-plus"></span></button>
      <button type="button" id="btn_update" name="btn_update" class="btn btn-primary">{!! trans('app.edit') !!} <span class="glyphicon glyphicon-refresh"></span></button>
    </div>
    <div class="panel-body">
      <form id="list" name="list">
      {{ csrf_field() }}
          <table class="table">
            <tr>
              <th width="20%">
                {!! trans('app.catid') !!}
              </th>
              <td style="text-align:left" width="80%" id="tag_cat_edit_id">
                {!! trans('app.newtagidmsg') !!}
              </td>
            </tr>
            <tr>
              <th  width="20%">
                {!! trans('app.catname') !!}
              </th>
              <td style="text-align:left" width="80%">
                <input type="text" name="tag_category_name" id="tag_category_name" value="">
              </td>
            </tr>
            <tr>
              <th  width="20%">
                {!! trans('app.catsort') !!}
              </th>
              <td style="text-align:left" width="80%">
                <input type="text" name="tag_category_weight" id="tag_category_weight" value="">
              </td>
            </tr>
          </table>

          <input type="hidden" name="tag_category_id" id="tag_category_id" value="">
      </form>
    </div>
  </div>
@endsection


@section('content')
  <div class="panel panel-primary">
    <div class="panel-heading">{!! trans('app.catlist') !!}</div>
    <div class="panel-body">
      <div id="tagcatlist">
        <table class="table" style="text-align:center">
          <tr>
            <th width="10%">ID</th>
            <th width="55%">{!! trans('app.catname') !!}</th>
            <th width="10%">{!! trans('app.catsort') !!}</th>
            <th width="25%">{!! trans('app.operation') !!}</th>
          </tr>
          @foreach ($tagCatListData as $tagCatData)
            <tr id="tagcatid_{{$tagCatData['id']}}">
              <td style="text-align:center" >{{$tagCatData['id']}}</td>
              <td style="text-align:left" id="tagcatname{{$tagCatData['id']}}">{{$tagCatData['name']}}</td>
              <td style="text-align:left" id="tagcatweight{{$tagCatData['id']}}">{{$tagCatData['weight']}}</td>
              <td style="text-align:center">
                   <a href="{{$tagCatData['id']}}" name="btn_edit{{$tagCatData['id']}}" id="btn_edit{{$tagCatData['id']}}" value="{{$tagCatData['id']}}" class="btn btn-primary btn_edit">
                      <span class="glyphicon glyphicon-edit"></span> {!! trans('app.tagedit') !!}
                   </a>
                   <a href="{{$tagCatData['id']}}" name="btn_del{{$tagCatData['id']}}" id="btn_del{{$tagCatData['id']}}" value="{{$tagCatData['id']}}" class="btn btn-primary btn_del">
                      <span class="glyphicon glyphicon-trash"></span> {!! trans('app.tagdelete') !!}
                   </a>
              </td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(function(){
	$('.btn_edit').click(function() {
    var tagcatid = $(this).attr("href");
		$('#tag_cat_edit_id').html(tagcatid);
    $('#tag_category_name').attr('value',$('#tagcatname'+tagcatid).html());
    $('#tag_category_weight').attr('value',$('#tagcatweight'+tagcatid).html());
		return false;
	});
	$('#btn_add').click(function() {
		if( confirm('{!! trans('app.tagcataddconfirm') !!}') ){
			$('#list').attr('action', '/control_add');
			$('#list').submit();
		}
		return false;
	});
	$('#btn_update').click(function() {
		if( isNaN($('#tag_cat_edit_id').html()) ){
			alert( '{!! trans('app.updatetagcatcheck') !!}' );
		} else {
			if( confirm('{!! trans('app.updatetagcatconfirm') !!}') ){
        $('#tag_category_id').attr('value',$('#tag_cat_edit_id').html());
				$('#list').attr('action', '/control_update');
				$('#list').submit();
			}
		}
		return false;
	});
	$('.btn_del').click(function() {
		if( confirm('{!! trans('app.tagcatdeleteconfirm') !!}') ){
			$('#tag_category_id').attr('value',$(this).attr("href"));
			$('#list').attr('action', '/control_delete');
			$('#list').submit();
		}
		return false;
	});
});
</script>
