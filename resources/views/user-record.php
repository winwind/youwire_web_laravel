<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>User Record</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="stylesheet" href="vendors/chartist/css/chartist.min.css">
    <link href="vendors/nvd3/css/nv.d3.min.css" rel="stylesheet" type="text/css">
    <link href="vendors/morrisjs/morris.css" rel="stylesheet" type="text/css">
    <link href="vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="vendors/awesomebootstrapcheckbox/css/build.css">
        <!-- global css -->
    <link type="text/css" href="css/app.css" rel="stylesheet">
        <!-- end of global css -->
  　<link rel="stylesheet" type="text/css" href="css/formelements.css">
    <link href="css/buttons_sass.css" rel="stylesheet">
    <link rel="stylesheet" href="stylesheet.css">
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link href="vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
  　<link rel="stylesheet" type="text/css" href="vendors/gridforms/css/gridforms.css">
    <link rel="stylesheet" type="text/css" href="css/form_layouts.css">
    <!--datatables-->
    <link href="css/datatable.css" rel="stylesheet">
    <!--end of page level css-->
  </head>

 <body class="skin-coreplus">
  <!--ヘッダー-->
  <header>
    <div style="margin:20px">
      <img class="youwire-logo" src="images/logo.png" style="width:350px;">
    </div>
    <div class="text-white" style="float:right; padding:20px;">
      ログインユーザー：<?php echo 'ユーザ名'?> || 前回ログイン日時：<?php echo '2018/9/4 12:00'?>
    </div>
    <section class="content-header">
      <div class="card-header text-white bg-primary">
           <div class="input-group-append dropdown">
               <a href="welcome.php" class="btn menu-button btn-primary card-title"><i class="fa fa-fw fa-home"></i>TOP</a>

                <button class="btn menu-button btn-primary dropdown-toggle dropdown" type="button" data-toggle="dropdown">
                  管理者メニュー
                </button>
                  <ul class="dropdown-menu">
                    <li class="dropdown-item">
                        <a href="tag-category.php">タグカテゴリ登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="group.php">グループ登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="user-record.php">録音番号登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="user-search.php">検索ユーザー登録</a>
                    </li>
                    <li class="dropdown-item">
                        <a href="securty-group.php">操作権限登録</a>
                    </li>
                </ul>

                <a href="userinfo.php" class="btn menu-button btn-primary card-title">パスワード変更</a>
                <a href="login.php" class="btn menu-button btn-primary card-title">ログアウト</a>
                <a href="welcome.php" class="btn menu-button btn-primary card-title">管理画面</a>

            </div>
      </div>
    </section>
  </header>
 <!--ヘッダーここまで-->
  <div class="wrapper">
    <section class="content">
      <div class="row">
        <div class="col-lg-4  col-12">
              <div class="card border-primary">
                      <div class="card-header text-white bg-primary" style="position:relative;">
                          <h3 class="card-title d-inline">
                              録音番号登録
                          </h3>
                          <!--span class="pull-right"-->
                                <button class="btn btn-primary pull-right d-sm-block submit-group" style="position:absolute; top:3px; right:5px;">
                                  <i class="fa fa-fw fa-upload"></i>登録
                                </button>
                          <!--/span-->
                      </div>
                      <div class="card-body">
                          <form>
                            <div class="col-12 form-group row">
                              <label class="col-6">検索/再生機能の付与:</label>
                               <input type="checkbox" class="" name="c1" id="c1" value="">
                              </div>
                              <div class="form-group row col-12">
                                  <label for="inputEmail3" class="col-6">タグカテゴリID:</label>
                                  <div class="col-6" id="inputGroupId"
                                         placeholder="ID" disabled="disabled">1</div>
                              </div>
                              <div class="form-group row col-12">
                                  <label for="inputPassword1" class="col-6">録音番号:</label>
                                  <input type="text" class="col-6" id="inputRecordnumber"
                                         placeholder="録音番号">
                              </div>
                              <div class="form-group row col-12">
                                  <label for="inputPassword1" class="col-6">録音番号表示名:</label>
                                  <input type="text" class="col-6" id="inputRecordName"
                                         placeholder="録音番号表示名">
                              </div>
                              <div class="form-group row col-12">
                                  <label for="inputPassword1" class="col-6">パスワード:</label>
                                  <input type="password" class="col-6" id="inputRecordName"
                                         placeholder="">
                              </div>
                              <div class="form-group row col-12">
                                  <label for="inputPassword1" class="col-6">確認用パスワード:</label>
                                  <input type="password" class="col-6" id="inputRecordName"
                                         placeholder="">
                              </div>
                              <div class="form-group row col-12">
                                     <label class="col-6">
                                       グループ:
                                     </label>
                                     <div class="">
                                       <select id="selectize1" class="form-control">
                                           <option value="">--選択--</option>
                                           <option value="claim">デフォルト</option>
                                           <option value="contact">東京本社</option>
                                           <option value="others">その他</option>
                                       </select>
                                     </div>
                               </div>
                          </form>
                      </div>
                  </div>
            </div>
            <div class="col-lg-8 col-12">
              <div class="card border-primary">
                <div class="card-header text-white bg-primary">
                    <h3 class="card-title d-inline">
                        <i class="fa fa-fw fa-list-alt"></i>録音番号一覧
                    </h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>録音番号</th>
                                <th>録音番号表示名</th>
                                <th>操作権限</th>
                                <th>グループ</th>
                                <th>編集</th>
                                <th>削除</th>
                                <th>録音制御設定</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>08012345678</td>
                                <td>デフォルトユーザー</td>
                                <td>ユーザー</td>
                                <td>デフォルト</td>
                                <td>

                                    <a href="#" class="btn btn-primary btn-xs" data-placement="top">
                                      <span class="fa fa-pencil"></span></a>

                                </td>
                                <td>

                                    <a href="#" class="btn btn-danger btn-xs" data-placement="top">
                                      <span class="fa fa-trash"></span></a>

                                </td>
                                <td>

                                    <a href="phone-number.php" class="btn btn-danger btn-xs"
                                            data-target="#" data-placement="top"><span
                                            class="fa fa-cog"></span></a>

                                </td>
                            </tr>

                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>

      </div>

    </section>
  </div>


     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
     <script type="text/javascript" src="script.js"></script>
     <!-- global js -->
     <script src="js/app.js" type="text/javascript"></script>
     <!-- end of global js -->
     <!-- begining of page level js -->
     <script src="vendors/iCheck/js/icheck.js" type="text/javascript"></script>
     <script src="js/custom_js/form_layouts.js" type="text/javascript"></script>
     <!--datatables-->
     <script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
     <script type="text/javascript" src="js/custom_js/simple-table.js"></script>
     <!-- end of page level js -->
  </body>
 </html>
