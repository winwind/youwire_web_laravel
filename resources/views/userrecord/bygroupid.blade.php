<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->


<h1>by group id get_record PAGE</h1>
<h2>録音番号一覧 by group_id</h2>
<table class="table">
	<tr>
		<td>ID</td>
		<td>録音番号</td>
		<td>録音番号表示名</td>
		<td>操作権限	</td>
		<td>グループ</td>
		<td>操作</td>
		<td>操作</td>
	</tr>
	@foreach ($get_records as $get_record)
	<tr>
		<td>{{$get_record->id}}</td>
		<td>{{$get_record->uname}}</td>
		<td>{{$get_record->name}}</td>
		<td>{{$get_record->security_group_name}}</td>
		<td>{{$get_record->group_name}}</td>
		<td><a href="{{ url('/user_search/'.$get_record->id.'/edit')}}">編集</a></td>
		<td><a href="{{ url('/user_search/destroy/'.$get_record->id)}}">削除</a></td>
	</tr>
	@endforeach

	
</table>

<hr>
<h2>録音番号登録</h2>
{!! Form::open(['url' => '/user_search/create', 'method' => 'get']) !!} 
<?= Form::label('uname', '録音番号'); ?>
<?= Form::number('uname', null, ['class' => 'form-control', 'placeholder' => 'uname']); ?>
<?= Form::label('name', '検索ユーザー表示名'); ?>
<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name']); ?>
<?= Form::label('password', 'パスワード'); ?>
<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
<?= Form::label('vpassword', '確認用パスワード'); ?>
<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
<?= Form::label('group_id', 'グループ'); ?>
<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), null, ['class' => 'form-control',  'placeholder' => 'なし']); ?>
<?= Form::label('security_group_id', '操作権限'); ?>
<?= Form::select('security_group_id', App\Security_group::all()->pluck('security_group_name','security_group_id'), null, ['class' => 'form-control']); ?>
<?= Form::submit('新規登録', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>