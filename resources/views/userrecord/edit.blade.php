<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->

<h1>EDIT user_record PAGE</h1>
{!! Form::model($get_records, ['url' => '/user_record/'.$get_records->id, 'method' => 'get']) !!} 
<?= Form::label('uname', '録音番号'); ?>
<?= Form::number('uname', null, ['class' => 'form-control', 'placeholder' => 'uname']); ?>
<?= Form::label('name', '検索ユーザー表示名'); ?>
<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name']); ?>
@if($is_user_rec_limit === 1)

<?= Form::hidden('is_user_rec_limit', $is_user_rec_limit); ?>
<?= Form::label('file_limit_size', 'file_limit_size'); ?>
<?= Form::number('file_limit_size', null, ['class' => 'form-control', 'placeholder' => 'file_limit_size']); ?>
@endif
<?= Form::label('user_type', '検索/再生機能の付与'); ?>
<?= Form::checkbox('user_type', true); ?><br>
if($user_type)//javascript{<br>
<?= Form::label('password', 'パスワード'); ?>
<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
<?= Form::label('vpassword', '確認用パスワード'); ?>
<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
}
@if($is_user_rec_limit === 0 )
<?= Form::label('group_id', 'グループ'); ?>
<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), null, ['class' => 'form-control']); ?>
<?= Form::label('security_group_id', '操作権限'); ?>
<?= Form::select('security_group_id', App\Security_group::all()->pluck('security_group_name','security_group_id'), null, ['class' => 'form-control']); ?>
@endif
<?= Form::submit('更新', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!} 
<!-- ////////////////////////// -->
</body>
</html>
