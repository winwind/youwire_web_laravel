<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->

<h1>user_record PAGE</h1>
<h2>録音番号一覧</h2>
<table class="table">
	<tr>
		<td>ID</td>
		<td>録音番号</td>
		<td>録音番号表示名</td>
		<td>操作権限</td>
		<td>グループ</td>
		<td>操作</td>
		<td>操作</td>
		<td>操作</td>
	</tr>
	@foreach ($get_records as $get_record)
	<tr>
		<td>{{$get_record->id}}</td>
		<td>{{$get_record->uname}}</td>
		<td>{{$get_record->name}}</td>
		<td>{{$get_record->security_group_name}}</td>
		<td>{{$get_record->group_name}}</td>
		<td><a href="{{ url('/user_record/'.$get_record->id.'/edit')}}">編集</a></td>
		<td><a href="{{ url('/user_record/destroy/'.$get_record->id)}}">削除</a></td>
		<td><a href="{{ url('/userrecord/Phonenumber/'.$get_record->id)}}">
		録音制御設定</a></td>
	</tr>
	@endforeach


</table>

<!-- Create from start 2018/09/07 end 2018/09/07 -->
<hr>
<h2>録音番号登録</h2>
{!! Form::open(['url' => '/user_record/create', 'method' => 'get']) !!} 
<?= Form::label('uname', '録音番号'); ?>
<?= Form::number('uname', null, ['class' => 'form-control', 'placeholder' => 'uname']); ?>
<?= Form::label('name', '検索ユーザー表示名'); ?>
<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name']); ?>
@if($is_user_rec_limit === 1)
<?= Form::hidden('is_user_rec_limit', $is_user_rec_limit); ?>
<?= Form::label('file_limit_size', 'file_limit_size'); ?>
<?= Form::number('file_limit_size', null, ['class' => 'form-control', 'placeholder' => 'file_limit_size']); ?>
@endif

<?= Form::label('user_type', '検索/再生機能の付与'); ?>
<?= Form::checkbox('user_type', true); ?><br>
if($user_type)//javascript{<br>
<?= Form::label('password', 'パスワード'); ?>
<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
<?= Form::label('vpassword', '確認用パスワード'); ?>
<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
}

@if($is_user_rec_limit === 0 )
<?= Form::label('group_id', 'グループ'); ?>
<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), null, ['class' => 'form-control']); ?>
<?= Form::label('security_group_id', '操作権限'); ?>
<?= Form::select('security_group_id', App\Security_group::all()->pluck('security_group_name','security_group_id'), null, ['class' => 'form-control']); ?>
@endif
<?= Form::submit('新規登録', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!} 

</body>
</html>