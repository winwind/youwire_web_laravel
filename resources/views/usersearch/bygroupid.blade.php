<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->

<h1>Usersearch PAGE</h1>
<table class="table">
	@foreach ($get_users as $get_user)
	<tr>
		<td>{{$get_user->id}}</td>
		<td>{{$get_user->uname}}</td>
		<td>{{$get_user->name}}</td>
		<td>{{$get_user->security_group_name}}</td>
		<td>{{$get_user->group_name}}</td>
		<td><a href="{{ url('/user_search/'.$get_user->id.'/edit')}}">編集</a></td>
		<td><a href="{{ url('/user_search/destroy/'.$get_user->id)}}">削除</a></td>
	</tr>
	@endforeach
</table>

{!! Form::open(['url' => '/user_search/create', 'method' => 'get']) !!} 
<?= Form::label('uname', 'uname,検索ユーザーID'); ?>
<?= Form::text('uname', null, ['class' => 'form-control', 'placeholder' => 'uname','maxlength' => 25]); ?>
<?= Form::label('name', 'name,検索ユーザー表示名'); ?>
<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name','maxlength' => 60]); ?>
<?= Form::label('password', 'password,パスワード'); ?>
<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
<?= Form::label('vpassword', 'vpassword,確認用パスワード'); ?>
<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
<?= Form::label('group_id', 'group_id,グループ'); ?>
<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), null, ['class' => 'form-control',  'placeholder' => 'なし']); ?>
<?= Form::label('security_group_id', 'security_group_id,操作権限'); ?>
<?= Form::select('security_group_id', App\Security_group::all()->pluck('security_group_name','security_group_id'), null, ['class' => 'form-control']); ?>
<?= Form::submit('新規登録', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>