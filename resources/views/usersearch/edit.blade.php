<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->


<h1>EDIT Usersearch PAGE</h1>

{!! Form::model($get_users, ['url' => '/user_search/'.$get_users->id, 'method' => 'get']) !!} 
<h4><?= Form::label('id', 'ID'); ?>
{{$get_users->id}}</h4>
<?= Form::label('uname', '検索ユーザーID'); ?>
<?= Form::text('uname', null, ['class' => 'form-control', 'placeholder' => 'uname','maxlength' => 25]); ?>
<?= Form::label('name', '検索ユーザー表示名'); ?>
<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name','maxlength' => 60]); ?>
<?= Form::label('password', 'パスワード'); ?>
<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
<?= Form::label('vpassword', '確認用パスワード'); ?>
<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
<?= Form::label('group_id', 'グループ	'); ?>
<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), 0, ['class' => 'form-control',  'placeholder' => 'なし']); ?>
<?= Form::label('security_group_id', '操作権限'); ?>
<?= Form::select('security_group_id', App\Security_group::all()->pluck('security_group_name','security_group_id'), null, ['class' => 'form-control']); ?>
<?= Form::submit('更新', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>
