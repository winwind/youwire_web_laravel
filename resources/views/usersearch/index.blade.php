<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<!-- ////////////////////////// -->

<h1>Usersearch PAGE</h1>
<h2>検索ユーザー一覧</h2>
@if ($errors > 0)
	@foreach ($errors as $error)
		<h5>{{ $error }}</h5><br>
	@endforeach
@endif 

<table class="table">
	<tr>
		<td>ID</td>
		<td>検索ユーザーID</td>
		<td>検索ユーザー表示名</td>
		<td>操作権限</td>
		<td>グループ</td>
		<td>操作</td>
		<td>操作</td>
	</tr>
	@foreach ($get_users as $get_user)
	<tr>
		<td>{{$get_user->id}}</td>
		<td>{{$get_user->uname}}</td>
		<td>{{$get_user->name}}</td>
		<td>{{$get_user->security_group_name}}</td>
		<td>{{$get_user->group_name}}</td>
		<td><a href="{{ url('/user_search/'.$get_user->id.'/edit')}}">編集</a></td>
		<td><a href="{{ url('/user_search/destroy/'.$get_user->id)}}">削除</a></td>
	</tr>
	@endforeach
</table>

<hr>
<h2>検索ユーザー登録</h2>
{!! Form::open(['url' => '/user_search/create', 'method' => 'get']) !!} 
<?= Form::label('uname', '検索ユーザーID'); ?>
<?= Form::text('uname', null, ['class' => 'form-control', 'placeholder' => 'uname','maxlength' => 25]); ?>
<?= Form::label('name', '検索ユーザー表示名'); ?>
<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name','maxlength' => 60]); ?>
<?= Form::label('password', 'パスワード'); ?>
<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
<?= Form::label('vpassword', '確認用パスワード'); ?>
<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
<?= Form::label('group_id', 'グループ'); ?>
<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), null, ['class' => 'form-control',  'placeholder' => 'なし']); ?>
<?= Form::label('security_group_id', '操作権限'); ?>
<?= Form::select('security_group_id', App\Security_group::all()->pluck('security_group_name','security_group_id'), null, ['class' => 'form-control']); ?>
<?= Form::submit('新規登録', ['class' => 'btn btn-primary']); ?>   
{!! Form::close() !!} 

<!-- ////////////////////////// -->
</body>
</html>
