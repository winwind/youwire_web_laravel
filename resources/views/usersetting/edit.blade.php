<!DOCTYPE html>
<html lang="en">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<style>
	body{
		background-color: #f7f7f7;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
	<p class="navbar-brand">UserSetting</p>
</nav>
<br>
<div class="container">
	<div class="row">
		<div class="col-2">.col-2</div>
			<div class="col-10 shadow p-3 mb-5 bg-white rounded">.col-10
				{!! Form::model($get_users, ['url' => '/user_setting/'.$get_users->id, 'method' => 'get']) !!} 
				<form>
				  <div class="form-row">
				    <div class="form-group col-md-6">
				    <?= Form::label('uname', 'ユーザー名'); ?>
					<?= Form::text('uname', null, ['class' => 'form-control', 'placeholder' => 'uname']); ?>
				    </div>
				    <div class="form-group col-md-6">
					<?= Form::label('name', 'ユーザ名'); ?>
					<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name']); ?>
				    </div>
				  </div>
				  <div class="form-row">
				    <div class="form-group col-md-6">
					<?= Form::label('password', 'パスワード'); ?>
					<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
				    </div>
				    <div class="form-group col-md-6">
					<?= Form::label('vpassword', '確認用パスワード'); ?>
					<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
				    </div>
				  </div>
				  <div class="form-row">
				    <div class="form-group col-md-6">
					<?= Form::label('email', 'メールアドレス'); ?>
					<?= Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'email']); ?>
				    </div>
				    <div class="form-group col-md-3">
					<?= Form::label('group_id', 'グループ'); ?>
					<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), null, ['class' => 'form-control']); ?>
				    </div>
				    <div class="form-group col-md-3">
					<?= Form::label('file_limit_size', '容量制限サイズ	'); ?>
					<?= Form::select('file_limit_size', ['1200000000' => '1200000000', '2400000000' => '2400000000', '3600000000' => '3600000000', '4800000000' => '4800000000', '0' => '0'], null, ['class' => 'form-control']); ?>
				    </div>
				  </div>
				  <button type="submit" class="btn btn-secondary">Sign in</button>
				  {!! Form::close() !!} 
				</form>
				  <br>
			</div>
	</div>
</body>
</html>