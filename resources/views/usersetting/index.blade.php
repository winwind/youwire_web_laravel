<!DOCTYPE html>
<html lang="en">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<style>
	body{
		background-color: #f7f7f7;
	}
</style>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
	<p class="navbar-brand">UserSetting</p>
</nav>
<br>
<div class="container">
	<div class="row">
		<div class="col-2">.col-2</div>
			<div class="col-10 shadow p-3 mb-5 bg-white rounded">.col-10
				{!! Form::open(['url' => '/user_setting/create', 'method' => 'get']) !!} 
				<form>
				  <div class="form-row">
				    <div class="form-group col-md-6">
				    <?= Form::label('uname', 'ユーザー名'); ?>
					<?= Form::text('uname', null, ['class' => 'form-control', 'placeholder' => 'uname','maxlength' => 25]); ?>
				    </div>
				    <div class="form-group col-md-6">
					<?= Form::label('name', 'ユーザ名'); ?>
					<?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name','maxlength' => 60]); ?>
				    </div>
				  </div>
				  <div class="form-row">
				    <div class="form-group col-md-6">
					<?= Form::label('password', 'パスワード'); ?>
					<?= Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password']); ?>
				    </div>
				    <div class="form-group col-md-6">
					<?= Form::label('vpassword', '確認用パスワード'); ?>
					<?= Form::text('vpassword', null, ['class' => 'form-control', 'placeholder' => 'vpassword']); ?>
				    </div>
				  </div>
				  <div class="form-row">
				    <div class="form-group col-md-6">
					<?= Form::label('email', 'メールアドレス'); ?>
					<?= Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'email']); ?>
				    </div>
				    <div class="form-group col-md-3">
					<?= Form::label('group_id', 'グループ'); ?>
					<?= Form::select('group_id', App\Group::all()->pluck('group_name','id'), null, ['class' => 'form-control']); ?>
				    </div>
				    <div class="form-group col-md-3">
					<?= Form::label('file_limit_size', '容量制限サイズ	'); ?>
					<?= Form::select('file_limit_size', ['1200000000' => '1200000000', '2400000000' => '2400000000', '3600000000' => '3600000000', '4800000000' => '4800000000', '0' => '0'], null, ['class' => 'form-control']); ?>
				    </div>
				  </div>
				  <button type="submit" class="btn btn-secondary">Sign in</button>
				</form>
				  <br>
			</div>
	</div>
	<hr><br>
	<div class="row">
		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col">UID</th>
		      <th scope="col">ユーザー名</th>
		      <th scope="col">ユーザ名</th>
		      <th scope="col">登録日</th>
		      <th scope="col">最終ログイン</th>
		      <th scope="col">容量制限サイズ</th>
		      <th colspan="2">操作</th>
		    </tr>
		  </thead>
		  <tbody>
		    @foreach ($get_users as $get_user)
			<tr>
				<th scope="row">{{$get_user->id}}</th>
				<td>{{$get_user->uname}}</td>
				<td>{{$get_user->name}}</td>
				<td>{{$get_user->created_at}}</td>
				<td>{{$get_user->last_login}}</td>
				<td>{{$get_user->file_limit_size}}</td>
				<td><a href="{{ url('/user_setting/'.$get_user->id.'/edit')}}">編集</a></td>
				<td><a href="{{ url('/user_setting/destroy/'.$get_user->id)}}">削除</a></td>
			</tr>
			@endforeach
		  </tbody>
		</table>
	</div>
</div>



</body>
</html>