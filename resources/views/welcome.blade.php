<!DOCTYPE html>
@extends('layouts.header')

  @section('head')
    <meta charset="UTF-8">
    <title>Top</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
   <!-- global css -->
   <link type="text/css" href="css/app.css" rel="stylesheet"/>
   <!-- end of global css -->
   <!-- page level css -->
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
   <link rel="stylesheet" href="stylesheet.css">
   <link rel="stylesheet" type="text/css" href="css/custom.css">
   <link rel="stylesheet" type="text/css" href="css/form_layouts.css">
<!--datatables-->
    <link rel="stylesheet" type="text/css" href="vendors/datatables/css/dataTables.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="vendors/datatables/css/buttons.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <!--link rel="stylesheet" type="text/css" href="css/custom_css/datatables_custom.css"-->
    <!--CSS for datetime picker -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!--link rel="stylesheet" href="vendors/datetime/css/jquery.datetimepicker.css"-->
    <link rel="stylesheet" type="text/css" href="/jquery.datetimepicker.css"/ >
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <!--link rel="stylesheet" type="text/css" href="css/datepicker.css"-->
   <!--checkbox&radio-->
    <link href="vendors/prettycheckable/css/prettyCheckable.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="css/custom_css/radio_checkbox.css">
    <link rel="stylesheet" type="text/css" href="vendors/awesomebootstrapcheckbox/css/awesome-bootstrap-checkbox.css">
   <!-- end of page level css -->
  @endsection

  @section('contents')
    <div class="row">
      <div id="sidebar-search">
      <!--検索タブ-->
          <div class="col-12">
              <div class="card border-primary">
                <div class="card-header bg-primary text-white">
                  <h3 class="card-title d-inline">
                      <i class="fa fa-fw fa-search"></i>{!! trans('app.search') !!}
                  </h3>
                  <span class="pull-right hidden-xs">
                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                    <i class="fa fa-fw fa-times removepanel"></i>
                   <!--button class="btn btn-primary pull-right"  id="removetab">
                    <i class="fa fa-fw fa-times"></i>
                  </button-->
                  </span>
                </div>
              <div class="card card-body">
                  <form action="#" id="serch_form" class="">
                    {{ csrf_field() }}
                      <div class="box-body" style="margin-left:5px;">
                        <!--通話日-->
                        <div class="form-group row" style=margin-bottom:3px;>
                          <label for="startdatetime" class="col-4 col-form-label text-left">
                            {!! trans('app.callstart') !!}</label>
                            <div class="col-8">
                                <div class="input-group">
                                  <span class="input-group-text border-right-0 rounded-0">
                                      <i class="fa fa-fw fa-calendar"></i>
                                  </span>
                                  <input data-format="yyyy/MM/dd hh:mm" type="text" id="datetimepicker1"
                                    class="form-control" name="startdatetime"value="@if (isset($old_req)) {{$old_req->startdatetime}} @endif"/>
                                </div>
                            </div>
                          </div>
                         <div class="form-group row">
                            <label for="enddatetime" class="col-4 col-form-label">~</label>
                            <div class="col-8">
                              <div class='input-group date'>
                                <span class="input-group-text border-right-0 rounded-0">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </span>
                                  <input type="text" data-format="yyyy/MM/dd" class="form-control"
                                  id="datetimepicker2" name="enddatetime" value="@if (isset($old_req)) {{$old_req->enddatetime}} @endif"/>
                              </div>
                            </div>
                         </div>
                          <!--通話開始/終わり-->
                         <div class="form-group row" style=margin-bottom:3px;>
                            <label for="startduration" class="col-4 col-form-label">{!! trans('app.duration') !!}</label>
                            <div class="col-8">
                              <div class='input-group date' id='datetimepicker3'>
                                  <span class="input-group-text border-right-0 rounded-0">
                                      <i class="fa fa-fw fa-clock-o"></i>
                                  </span>
                                  <input type="text" class="form-control" id="startduration" name="startduration" value="@if (isset($old_req->startduration)) {{$old_req->startduration}} @endif"> {{-- @if (isset($old_req)) {{$old_req->startduration}} @endif --}}
                              </div>
                            </div>
                         </div>
                         <div class="form-group row">
                            <label for="endduration" class="col-4 col-form-label">~</label>
                            <div class="col-8">
                              <div class='input-group date' id='datetimepicker4'>
                                  <span class="input-group-text border-right-0 rounded-0">
                                      <i class="fa fa-fw fa-clock-o"></i>
                                  </span>
                                  <input type="text" class="form-control" id="endduration" name="endduration" value="@if (isset($old_req->endduration)) {{$old_req->endduration}} @endif"> {{--@if (isset($old_req)) {{$old_req->endduration}} @endif--}}
                              </div>
                            </div>
                         </div>

                        <!--自番号-->
                         <div class="form-group row" style="margin-top:25px;">
                             <label for=rightinputnumber class="col-4 col-form-label text-left">{!! trans('app.localparty') !!}</label>
                             <div class="col-8">
                                 <div class="input-group  input-group-prepend" >
                                   <span class="input-group-text border-right-0 rounded-0">
                                       <i class="fa fa-fw fa-phone"></i>
                                   </span>
                                     <input type="text"  name="localparty" value="@if (isset($old_req)) {{$old_req->localparty}} @endif"
                                             id="localparty" class="form-control"/>
                                 </div>
                             </div>
                         </div>
                         <!--相手番号-->
                         <div class="form-group row">
                             <label for="inputnumber" class="col-4 col-form-label text-left">{!! trans('app.remoteparty') !!}</label>
                               <div class="col-8">
                                 <div class="input-group  input-group-prepend">
                                     <span class="input-group-text border-right-0 rounded-0">
                                         <i class="fa fa-fw fa-phone"></i>
                                     </span>
                                     <input type="text" class="form-control" id="remoteparty"
                                     name="remoteparty" value="@if (isset($old_req)) {{$old_req->remoteparty}} @endif"/>
                                 </div>
                               </div>
                          </div>
                          <!--方向-->
                          <div class="form-group row" style="margin-top:30px;">
                                <label class="col-12">{!! trans('app.direction') !!}</label>
                                      <div class="col-6 py-1"  style="display:block;">
                                          <div class="form-check abc-checkbox form-check-inline">
                                              <input class="form-check-input" type="checkbox" name="direction_in"
                                               id="direction_in" value="in" @if (isset($old_req)) {{$old_req->direction_in=="in"?"checked":""}} @endif/>
                                              <label class="form-check-label" for="inlineCheckbox1">{!! trans('app.incoming') !!} </label>
                                          </div>
                                      </div>
                                      <div class="col-6 py-1">
                                          <div class="form-check abc-checkbox form-check-inline">
                                              <input class="form-check-input" type="checkbox" name="direction_out"
                                              id="direction_out" value="out" @if (isset($old_req)) {{$old_req->direction_out=="out"?"checked":""}} @endif/>
                                              <label class="form-check-label" for="inlineCheckbox2">{!! trans('app.outgoing') !!} </label>
                                          </div>
                                      </div>
                             </div>
                            <!--ボイスレコーダ項目-->
                             @if ($config["player_value"]==0 && ($config["app_type"]==1 || $config["app_type"]==2))
                              <div class="form-group row">
                              <!--<div class="col-xs-2">Checkbox</div>-->
                                <div class="col-xs-8">
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="direction_vr" value="vr" @if (isset($old_req)) {{$old_req->direction_vr=="vr"?"checked":""}} @endif>
                                    <label class="form-check-label" for="direction_vr">
                                      {!! trans('app.voicerecorder') !!}
                                    </label>
                                  </div>
                                </div>
                              <!--方向：全て？-->
                                <div class="col-xs-8">
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="direction_all" value="all"  @if (isset($old_req)) {{$old_req->direction_all=="all"?"checked":""}} @endif>
                                    <label class="form-check-label" for="direction_all">
                                      {!! trans('app.all') !!}
                                    </label>
                                  </div>
                                </div>
                              </div>
                            　@endif
                            <!--ダウンロード-->
                            <div class="form-group row" style="margin-bottom:30px;">
                              <label class="col-12">{!! trans('app.download') !!}</label>
                                 <div class="col-6 py-1">
                                   <div class="form-check abc-checkbox form-check-inline">
                                       <input class="form-check-input" type="checkbox" id="download_act"
                                       name="download_act" value="act" @if (isset($old_req)) {{$old_req->download_act=="act"?"checked":""}} @endif/>
                                       <label for="inlineCheckbox1">{!! trans('app.ok') !!}</label>
                                   </div>
                                 </div>
                                 <div class="col-6 py-1">
                                    <div class="form-check abc-checkbox form-check-inline">
                                       <input class="form-check-input" type="checkbox" id="download_non"
                                       name="download_non" value="non" @if (isset($old_req)) {{$old_req->download_non=="non"?"checked":""}} @endif/>
                                       <label for="inlineCheckbox2">{!! trans('app.non') !!}</label>
                                    </div>
                                </div>
                            </div>
                            <!--タグ-->
                            <div class="form-group row">
                                  <label class="col-4 col-form-label text-left">{!! trans('app.tag') !!}</label>
                                  <div class="col-8">
                                      <select name="selected_tag" id="selected_tag" class="form-control">
                                          <option value="0">----</option>
                                          @foreach ($tag_cat as $cat)
                                            <option value="{{$cat->id}}" @if (isset($old_req)) {{$old_req->selected_tag==$cat->id?"selected":""}} @endif>{{$cat->name}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              <!--グループ-->
                              <div class="form-group row">
                                     <label class="col-4 col-form-label text-left">{!! trans('app.group') !!}</label>
                                     <div class="col-8">
                                       <select name="selected_group" id="selected_group" class="form-control">
                                           <option value="-1">----</option>
                                           @foreach ($tag_group as $group)
                                              <option value="{{$group->id}}" @if (isset($old_req)) {{$old_req->selected_group==$group->id?"selected":""}} @endif>{{$group->group_name}}</option>
                                            @endforeach
                                       </select>
                                     </div>
                               </div>
                               <!--表示件数-->
                               <div class="form-group row">
                                      <label class="col-4 col-form-label text-left">{!! trans('app.searchpage') !!}</label>
                                      <div class="col-8">
                                        <select name="selected_page" class="form-control">
                                          <option value="20" @if (isset($old_req)) {{$old_req->selected_page=="20"?"selected":""}} @endif>20</option>
                                          <option value="50" @if (isset($old_req)) {{$old_req->selected_page=="50"?"selected":""}} @endif>50</option>
                                          <option value="100" @if (isset($old_req)) {{$old_req->selected_page=="100"?"selected":""}} @endif>100</option>
                                        </select>
                                      </div>
                                </div>
                              <!--フリーワード検索-->
                              <div class="form-group row" style="margin-top:25px;">
                                  <label class="col-4">{!! trans('app.text') !!}</label>
                                  <div class="col-8">
                                      <input type="text" class="form-control" id="freeword" name="freeword" value="@if (isset($old_req)) {{$old_req->freeword}} @endif">
                                 </div>
                              </div>
                              <div class="form-group row">
                                 <div class="col-5"></div>
                                 <div class="col-7 row">
                                        <div class="py-1">
                                          <div class="form-check abc-radio form-check-inline">
                                            <input class="form-check-input" type="radio" id="and_or1" name="and_or" value="and" @if (isset($old_req)) {{$old_req->and_or=="and"?"checked":""}} @endif>
                                            <label class="form-check-label" for="and_or1">AND</label>
                                          </div>
                                        </div>
                                        <div class="py-1">
                                          <div class="form-check abc-radio form-check-inline">
                                            <input class="form-check-input" type="radio" id="and_or2" name="and_or" value="or" @if (isset($old_req)) {{$old_req->and_or=="or"?"checked":""}} @endif>
                                            <label class="form-check-label" for="and_or2">OR</label>
                                          </div>
                                        </div>
                                  </div>
                              </div>
                            <!--検索/クリアボタン-->
                            <div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="button" name="btn_search"  id="btn_search" class="btn btn-primary">
                                          {!! trans('app.search') !!}
                                        </button>
                                        &nbsp;
                                        <button type="button"　name="btn_clear" id="btn_clear" class="btn btn-default bttn_reset">
                                          {!! trans('app.clear') !!}
                                        </button>
                                     </div>
                                </div>
                            </div>
                         </div>
                     </form>
                  </div>
               </div>
            </div>
        </div>
            <!--検索タブ終わり-->

          <section id="main-content" class="col-8">
           <!--音声-->
           <form>
               <div class="col-12">
                     <div class="card border-primary" style="height:100px;">
                         <div class="card-header text-white bg-primary" style="position:relative;">
                             <h3 class="card-title d-inline" >
                               <i class="fa fa-fw fa-play-circle"></i>{!! trans('app.play') !!}
                             </h3>
                          </div>
                     <div class="card-body">
                              @if ($config["player_value"] == 1) <!-- 1 => Windows Media Player -->
                                   <object
                                     id="wmp2"
                                     width="96%" height="45"
                                     classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
                                     codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715"
                                     standby="Loading MicrosoftR WindowsR Media Player components..."
                                     type="application/x-oleobject">
                                     <!-- <param name="FileName" value="./media/meta_sample.asx" /> --><!-- Filename属性は絶対・相対パスどちらでもOK -->
                                     <param name="ShowControls" value="true" /><!-- コントロール表示 -->
                                     <param name="AutoStart" value="true" /><!-- 自動再生しない -->
                                     <!-- コントロール表示（showcontrols="1"）、自動再生しない（autostart="0"） -->
                                     <embed
                                       name="wmp2"
                                       type="application/x-mplayer2"
                                       pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"
                                       width="96%" height="45"
                                       showcontrols="1"><!-- src属性は絶対パス指定 -->
                                     </embed>
                               </object>
                             @else
                               <div id="player_container" style="margin:0 auto;"></div>
                               <p id="message"></p>
                               ​<script src="{!! asset('js/jwplayer.js') !!}" ></script>
                               <script type="text/javascript">
                                     jwplayer("player_container").setup({
                                     flashplayer: "{!! asset('player/player.swf') !!}",
                                     controlbar: "bottom",
                                     //width: 800,
                                     width:"96%",height: 24,
                                     stretching: 'exactfit'
                                     });
                             </script>
                           @endif
                    </div>
                 </div>
               </div>


             <!--data table-->
            <div class="col-12">
                <div class="card border-primary">
                   <div class="card-header text-white bg-primary">
                     <h3 class="card-title d-inline"  style="position:relative;">
                      <i class="fa fa-fw fa-search"></i>{!! trans('app.searchresult') !!}
                      @if(!empty($res->total()))
                       @if ($res->total() >= 0)
                            @if ($res->total() > 0)
                                <!-- display the number of items in the page.. Here we can use laravel "pagination" module -->
                                {{$res->firstItem()}} - {{$res->lastItem()}} of {{$res->total()}}
                            @else
                              0 件
                            @endif
                         @endif
                        @endif
                      </h3>
                      <span class="pull-right" style="position:absolute; top:3px; right:5px;">
                        <button class="btn menu-button btn-primary dropdown-toggle dropdown" type="button" data-toggle="dropdown">
                          {!! trans('app.select') !!}
                        </button>
                          <ul class="dropdown-menu">
                          @if ($data['authority_download'] == 1 || $data['authority_protect'] == 1 || $data['authority_delete'] == 1)
                            @if ($config['bulk_dl_limit'] >0 && $data['authority_bulk_download'] == 1)
                              <li class="dropdown-item">
                                  <a href="#" id="button_bulk_dl" name="button_bulk_dl">{!! trans('app.bulkdl') !!} </a>
                              </li>
                            @endif
                            @if ($data['authority_download'] == 1)
                              <li class="dropdown-item">
                                  <a href="#" name="csv_out_all" id="csv_out_all">{!! trans('app.allcheck') !!}</a>
                              </li>
                              <li class="dropdown-item">
                                  <a href="#" id="button_csv_out" name="button_csv_out">{!! trans('app.csvall') !!} </a>
                              </li>
                            @endif
                            @if ($data['authority_protect'] == 1)
                              <li class="dropdown-item">
                                  <a href="#" id="button_lock" name="button_lock">{!! trans('app.lock') !!}</a>
                              </li>
                              <li class="dropdown-item">
                                  <a href="#" id="button_unlock" name="button_unlock">{!! trans('app.unlock') !!}</a>
                              </li>
                            @endif
                            @if ($data['authority_delete'] == 1)
                              <li class="dropdown-item">
                                  <a href="#" id="button_del" name="button_del">{!! trans('app.delete') !!}</a>
                              </li>
                            @endif
                          @endif
                          </ul>
                       </span>
                   </div>
                        <div class="card-body">
                          <div class="table-pages">
                            {{ $res->appends(request()->input())->links() }}
                          </div>
                            <div class="table-responsive">
                                <table id="mytable" class="table table-bordred table-striped">
                                  <thead>
                                    <tr>
                                      <th><input type="checkbox" name="ckeck_ctrl" id="check_ctrl"/></th>
                                        <th scope="col">ID</th>
                                        <th scope="col">{!! trans('app.play') !!}</th>
                                        <th scope="col">{!! trans('app.callstart') !!}</th>
                                        <th scope="col">{!! trans('app.duration') !!}</th>
                                        <th scope="col">{!! trans('app.group') !!}</th>
                                        <th scope="col">{!! trans('app.localparty') !!}</th>
                                        <th scope="col">{!! trans('app.direction') !!}</th>
                                        <th scope="col">{!! trans('app.remoteparty') !!}</th>
                                        @if ($config['customer'] == 1)
                                            <th scope="col">{!! trans('app.rivision') !!}</th>
                                        @endif
                                        @if ($config['app_type'] == 1 || $config['app_type'] == 3)
                                            <th scope="col">{!! trans('app.place') !!}</th>
                                        @endif
                                        <th scope="col">{!! trans('app.lastdldate') !!}</th>
                                        @if ($data['tag_use'] == 1 && $data['authority_add_tag'] == 1)
                                            <th scope="col">{{$data['qtag_name']}}</th>
                                        @endif
                                        @if ($data['authority_download'] == 1 && $data['authority_add_tag'] == 1)
                                            <th scope="col">{!! trans('app.operation') !!}</th>
                                        @endif
                                    </tr>
                                  </thead>
                                  <tbody>
                                   @foreach ($res as $items)
                                    <tr id="recording_id_{{$items->id}}">
                                        <td>
                                          <input type="checkbox" name="chkselect[]" id="chkselect" value="{{ $items->id }}" onclick="sumDlFileDuration({{ $config['bulk_dl_limit'] }});" />
                                          <input type="hidden" name="duration_sec[]" id="duration_sec" value="{{ $items->duration_sec }}" />
                                        </td>
                                        <td>
                                          <a href="{{ $items->id }}" name="link_view_detail_{{$items->id}}" value="{{$items->id}}" id="link_view_detail_{{$items->id}}" class="link_view_detail">{{$items->id}}</a>
                                          @if ($items->protect === 1)
                                            <span class="glyphicon glyphicon-remove-circle"></span>
                                          @endif
                                        </td>
                                        <td>
                                          <a href="#" class="btn btn-primary btn-xs"  name="button_play{{$items->id}}" id="button_play{{$items->id}}"
                                              onclick="@if ($config['player_value'] == 1) wmp_play('{!! asset('recordings') !!}/{{$items->filename_org}}'
                                              @else loadjwplayer('{!! asset('recordings') !!}/{{$items->localparty}}/{{$items->filename}}'@endif,'recording_id_{{$items->id}}');">
                                              <span class="fa fa-fw fa-play-circle"></span>
                                          </a>
                                        </td>
                                        <td>{{$items->timestamp_date}}</br>{{$items->timestamp_time}}</td>
                                        <td>{{$items->duration}}</td>
                                        <td>{{$items->group_name}}</td>
                                        <td  id="localparty_{{$items->id}}">{{$items->localparty}}</td>
                                        <td>
                                            @if ($items->direction === 0)
                                              <i class="fa fa-fw fa-arrow-left"></i>
                                            @elseif ($items->direction === 1)
                                              <i class="fa fa-fw fa-arrow-right"></i>
                                            @elseif ($items->direction === 2)
                                              <img src="{!! asset('images/voice_recorder_26.png') !!}" />
                                            @endif
                                          </td>
                                        <td id="remoteparty_{{$items->id}}">{{$items->remoteparty}}</td>
                                        @if ($config['customer'] == 1)
                                            <td>{{$items->revision}}</td>
                                        @endif

                                        @if ($config['app_type'] == 1 || $config['app_type'] == 3)
                                            @if ($items->location)
                                              <td>
                                                  <a href="http://maps.google.com/maps?q={{$items->latitude}},{{$items->longitude}}" target="_blank">
                                                      <img class="status" src="{!! asset('images/map_16.png') !!}" title="{{$items->location}}" />
                                                  </a>
                                              </td>
                                            @else
                                              <td>-</td>
                                            @endif
                                        @endif

                                        <td>{{$items->download_datetime_date}}</br>{{$items->download_datetime_time}}</td>

                                        @if ($data['tag_use'] && $data['authority_add_tag'] == 1)
                                          <td>
                                              @if ($items->qtag_exist)
                                                  <img src="{!! asset('images/voice_record_tag2_16.png') !!}" title="{{$data['qtag_name']}}" />
                                              @endif
                                          </td>
                                        @endif

                                        @if ($data['authority_download'] == 1 || $data['authority_add_tag'] == 1)
                                         <td>
                                            @if ($data['authority_download'] == 1)
                                                    <a href="{{$items->id}}" class="btn btn-primary btn-xs" name="button_dl{{$items->id}}" id="button_dl{{$items->id}}" value="{{$items->id}}" class="btn btn-primary btn_dl">
                                                      <span class="glyphicon glyphicon-music"></span> DL
                                                    </a>
                                            @endif

                                            @if ($data['tag_use'] && $data['authority_add_tag'] == 1)
                                                <li>
                                                    <a href="{{$items->id}}" style="height:24px;line-height:26px;" name="button_qtag{{$items->id}}" id="button_qtag{{$items->id}}" value="{{$items->id}}">
                                                      <img src="{!! asset('images/voice_record_tag_16.png') !!}" title="QuickTag">{{$data['qtag_name']}}
                                                      </a>
                                                </li>
                                            @endif
                                         </td>
                                           @endif
                                        <!--/td-->
                                    </tr>

                                    @endforeach
                                  </tbody>
                                </table>
                                <div class="table-pages">
                                  {{ $res->appends(request()->input())->links() }}
                                </div>
                            </div>
                        </div>
                   </div>
                </div>

          <input type="hidden" name="oldPlayingID1" id="oldPlayingID1" value="0"/>
          <input type="hidden" name="id" id="id" value=""/>
          <input type="hidden" name="search_sql" id="search_sql" value="{{$data['search_sql']}}"/>
          @if(isset($data['selected_page']))
            <input type="hidden" name="selected_page" id="selected_page" value="{{$data['selected_page']}}"/>
          @endif
          <input type="hidden" name="protect" id="protect" value="0"/>
          <input type="hidden" name="recid" id="recid" value="0"/>
          <input type="hidden" name="page" id="page" value="0"/>
        </form>
      </section>
    </div>
  <!--maincontent-->
 </div>
  <!--/div-->
  @endsection
<!-- ./wrapper -->

  <!-- global js -->
  <script src="js/app.js" type="text/javascript"></script>
  <!-- end of global js-->
  <!-- page level js-->
  <script src="js/custom_js/form_layouts.js" type="text/javascript"></script>
  <script type="text/javascript" src="vendors/selectric/js/jquery.selectric.min.js"></script>
  <script type="text/javascript" src="js/custom_js/custom_elements.js"></script>
  <script src="js/custom_js/radio_checkbox.js"></script>
  <!--datatables-->
  <script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="vendors/datatables/js/dataTables.bootstrap4.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="/jquery.js"></script>
  <script src="/build/jquery.datetimepicker.full.min.js"></script>
  <script type="text/javascript" src="dist/jquery.mask.js"></script>
  <script type="text/javascript" src="dist/jquery.mask.min.js"></script>
<script type="text/javascript">

function submitStop(e){
    if(!e) var e = window.event;

    if(e.keyCode == 13)
        return false;
}

$(function(){
  $('#datetimepicker1').datetimepicker();
  $('#datetimepicker2').datetimepicker();
  $('#startduration').mask('00:00:00',{placeholder: "__:__:__"});
  $('#endduration').mask('00:00:00',{placeholder: "__:__:__"});
  /*  $('input[name="startdatetime"]').daterangepicker({
      timePicker: true,
      singleDatePicker: true,
      showDropdowns: true,
      autoApply: true,
      autoUpdateInput: true,
      locale: {
          format: 'YYYY/MM/DD hh:mm',
        }
    });
    $('input[name="startdatetime"]').daterangepicker({
    "singleDatePicker": true,
    "timePicker": true,
    "showDropdowns": true,
    "locale": {
        "format": "YYYY/MM/DD hh:mm",
        "firstDay": 1
      },
    },function(start, end, label) {
      console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });

    /*$('#startduration').datetimepicker({
      format: 'HH:mm:ss'
    });
    $('#endduration').datetimepicker({
      format: 'HH:mm:ss'
    });*/
  /*ーーー検索タブの処理ーーー*/
      /*$('#datetimepicker1').datetimepicker({
        viewMode: 'days',
        format: 'YYYY-MM-DD HH:mm'
      });
      $('#datetimepicker2').datetimepicker({
        viewMode: 'days',
        format: 'YYYY-MM-DD HH:mm'
      });
      $('#datetimepicker3').datetimepicker({
        format: 'HH:mm:ss'
      });
      $('#datetimepicker4').datetimepicker({
        format: 'HH:mm:ss'
      });*/
      $('#btn_search').click(function() {
        $('#serch_form').attr('action', '/search');
        $('#serch_form').attr('method', 'GET');
        $('#serch_form').submit();
        $('input[name="enddatetime"]').val("");
        return false;
      });
      $('#btn_clear').click(function() {
        //$(this).closest("form").find("textarea,:text,select").val("").end().find(":checked").not('[name="and_or"]').prop("checked",false);
        $('#datetimepicker1').val("");
        $('#datetimepicker2').val("");
        $('#startduration').val("00:00:00");
        $('#endduration').val("00:00:00");
        $('#localparty').val("");
        $('#remoteparty').val("");
        $('#direction_in').prop("checked", false);
        $('#direction_out').prop("checked", false);
        //$('#direction_vr').val("");
        //$('#direction_all').val("");
        $('#download_act').prop("checked", false);
        $('#download_non').prop("checked", false);
        $('#selected_tag').val("");
        $('#selected_group').val("");
        $('#freeword').val("");
        $('#and_or1').prop("checked", false);
        $('#and_or2').prop("checked", false);
        return false;
      });
    /*ーー検索タブの処理ここまでーー*/
	/*$('#button_lock').click(function() {
		return false;
	});
	$('#btn_search').click(function() {
		$('#serch_form').attr('action', '/search');
    $('#serch_form').attr('method', 'GET');
		$('#serch_form').submit();
		return false;
	});
	$('#btn_clear').click(function() {
		$(this).closest("form").find("textarea,:text,select").val("").end().find(":checked").not('[name="and_or"]').prop("checked",false);
		$('#selected_tag').val("");
		$('#selected_group').val("");
		$('#startduration').val("00:00:00");
		$('#endduration').val("00:00:00");
		return false;
	});*/
	$('#button_lock').click(function() {
		//var url_param = '<{youwire_pagenavi_get_params pagenavi=$pageNavi}>';
    var n = $(this).closest("form").find("input:checkbox[name^='chkselect']:checked").length;
    if(n>0)
    {
		  $('#list').attr('action', '/control_update');
      $('#protect').attr('value', '1');
		  $('#list').submit();
		  return false;
    }
    else {
      alert("No record is selected");
    }
	});
	$('#button_unlock').click(function() {
		//var url_param = '<{youwire_pagenavi_get_params pagenavi=$pageNavi}>';
    var n = $(this).closest("form").find("input:checkbox[name^='chkselect']:checked").length;
    if(n>0)
    {
  		$('#list').attr('action', '/control_update');
      $('#protect').attr('value', '0');
  		$('#list').submit();
  		return false;
    }
    else {
      alert("No record is selected");
    }
	});
	$('.link_view_detail').click(function() {
		var $id = $(this).attr("href");
    $('#page').attr('value',{{$res->currentPage()}});
    $('#recid').attr('value',$id);
		//var url_param = '<{youwire_pagenavi_get_params pagenavi=$pageNavi}>';
		$('#list').attr('action', '/controlviewdetail');
		$('#list').submit();
		return false;
	});
	/*
	$('#btn_detail_download_text').click(function() {
		$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_download_text');
		$('#list').submit();
		return false;
	});
	*/
	$('.btn_dl').click(function() {
		$('#id').val($(this).attr("href"));
		$('#list').attr('action', '/download');
		$('#list').submit();
		return false;
	});
	/*$('.btn_qtag').click(function() {
		$('#id').val($(this).attr("href"));
		$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_quicktag');
		$('#list').submit();
		return false;
	});
	$('.btn_dl-text').click(function() {
		$id = $(this).attr("href");
		$('#id').val($id);
		var $voiceHtml = $('#voicetext_'+$id+'_html').val()
		$voiceHtml = $voiceHtml.split(' ');
		var $html = '';
		for (var i = 0; i < $voiceHtml.length; i++) {
			if ( $voiceHtml[i].indexOf(':0') != -1 ) {
				var $str = $voiceHtml[i].replace( /:0/g , '' );
				var $telnum = $('#localparty_'+$id).text();
				$html += $telnum + '：' + $str + "\r\n";
			} else if ( $voiceHtml[i].indexOf(':1') != -1 ) {
				var $str = $voiceHtml[i].replace( /:1/g , '' );
				var $telnum = $('#remoteparty_'+$id).text();
				$html += $telnum + '：' + $str + "\r\n";
			}
		}
		$('#voicetext').val($html);
		// $('#voicetext').val($('#voicetext_'+$id+'_html').val());
		$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_download_text');
		$('#list').submit();
		return false;
	});*/
	$('#button_del').click(function() {
		var n = $(this).closest("form").find("input:checked").length;
		if( n == 0 ){
			alert('{!! trans('app.norecordslected') !!}');
			return false;
		}
		//if(confirm("<{$smarty.const._MD_YOUWIRE_CONF_DEL_MSG1}>"+$(this).attr("href")+"<{$smarty.const._MD_YOUWIRE_CONF_DEL_MSG2}>")){
		if( confirm('{!! trans('app.deleteconfirm') !!}') ){
			//$('#id').val($(this).attr("href"));
			$('#list').attr('action', '/controldelete');
			$('#list').submit();
		}
		return false;
	});
	/*$('#button_bulk_dl').click(function() {

		var download_file_size = 0;

		if(document.list.chkselect){
			var chk_count = 0;
			for (var i = 0; i < document.list.chkselect.length; i++) {
				if (document.list.chkselect[i].checked) {
					chk_count++;
				}
			}
			if ( chk_count == 0 ) {
				alert("１件も選択されていません。\n一括ダウンロード対象をチェックしてください");
				return false;
			}
			if (!sumDlFileDuration('')) {
				return false;
			}
		} else {
			alert('<{$smarty.const._MD_YOUWIRE_BULK_DOWNLOAD_REQUIRE_CHECKED_MSG}>');
			return false;
		}

		var pass = window.prompt('<{$smarty.const._MD_YOUWIRE_BULK_DOUNLOAD_CONFIRM_MSG}>', "");
		if ( pass != null ) {
			$('#id').val($(this).attr("href"));
			$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_bulk_download?pass='+pass);
			$('#list').submit();
		}

		return false;
	}); */
	$('#button_csv_out').click(function() {

		var res = false;
		var out_all_chk = $("#csv_out_all:checked").val();

		if( out_all_chk == 1 ){
			if( confirm('Do you want to download CSV of all search results?') ){
				res = true;
			}
		} else {
			var n = $(this).closest("form").find("input:checked").length;
			if( n == 0 ){
				alert('No record is selected');
				return false;
			}
			if( confirm('Do you wans download CSV of selected record') ){
				res = true;
			}
		}
		if( res ){
			//$('#id').val($(this).attr("href"));
			$('#list').attr('action', '/csvoutput');
			$('#list').submit();
		}
		return false;
	});
	$('#check_ctrl').click(function() {
    if( $(this).is(':checked')){
			$(this).closest("form").find("input:checkbox[name^='chkselect']").prop('checked', true);//.attr("checked",true);
		}else{
			$(this).closest("form").find("input:checkbox[name^='chkselect']").prop('checked', false);//.attr("checked",false);
		}
		sumDlFileDuration('{{$config['bulk_dl_limit']}}');
	});
	/*$('#btn_search_complaint_time').click(function() {
		$('#serch_form').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/search_complaint_time');
		$('#serch_form').submit();
		return false;
	});
	$('#btn_search_complaint_count').click(function() {
		$('#serch_form').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/search_complaint_count');
		$('#serch_form').submit();
		return false;
	});*/
});



@if ($config['player_value'] == 1)
    function wmp_play(mediapath,reco_id,id){
        highlightRecord(reco_id);
        //alert(mediapath);
        document.wmp2.filename = mediapath;
        document.wmp2.play();
              if ( id ) {
                      var voiceText = document.getElementById('voicetext_'+id).innerHTML;
                      var dispArea = document.getElementById('youwire_recognition');
                      dispArea.value = voiceText;
              }
    }
@else
    function loadjwplayer(mediapath,reco_id,id){
        highlightRecord(reco_id);
        //alert(mediapath);
        jwplayer().load({file: mediapath});
        jwplayer().play();
        if ( id ) {
        var voiceText1 = document.getElementById('voicetext_'+id).innerHTML;
        var voiceText2 = document.getElementById('voicetext_'+id+'_html').value;
        var dispArea = document.getElementById('youwire_recognition');
        voiceHtml = voiceText2.split(' ');
        var html = '';
        for (var i = 0; i < voiceHtml.length; i++) {
          if ( voiceHtml[i].indexOf(':0') != -1 ) {
            var str = voiceHtml[i].replace( /:0/g , '' );
            var telnum = document.getElementById('localparty_'+id).innerHTML;
            html += '<div class="dir_0"><span class="telnum">' + telnum + '：</span><span class="text">' + str + '</span></div>';
          } else if ( voiceHtml[i].indexOf(':1') != -1 ) {
            var str = voiceHtml[i].replace( /:1/g , '' );
            var telnum = document.getElementById('remoteparty_'+id).innerHTML;
            html += '<div class="dir_1"><span class="telnum">' + telnum + '：</span><span class="text">' + str + '</span></div>';
          }
        }
        dispArea.value = voiceText1;
        dispArea.innerHTML = html;
      }
    }

@endif


function highlightRecord(id){

  var SesVar = '<%= Session("show_details").ToString() %>';
  if (SesVar != 1) {
      //document.getElementById(oldID).classList.remove("highlight");
      $("#"+oldID).removeClass("highlight");
      //alert(oldID);
      var oldID = document.list.oldPlayingID1.value;

      document.list.oldPlayingID1.value = id;
      $("#"+id).addClass("highlight");
          //document.getElementById(id).add("highlight");
  }
}

function sumDlFileDuration(limit){

	// 一括ダウンロード権限の有無
	var authority_bulk_download = '{{$data['authority_bulk_download']}}';

	// 一括ダウンロード許可容量が0、もしくは、権限がない場合は以下の処理を行わない
	if ( limit <= 0 || authority_bulk_download == 0 ) {
		return false;
	}

	// チェックされている通話時間を加算する
	var sum = 0;

	if ( document.list.chkselect ) {
		var len = document.list.chkselect.length;
		for(var i=0;i<len;i++){
			if ( document.list.chkselect[i].checked ) {
				sum = sum + parseInt(document.list.duration_sec[i].value);
			}
		}
	}

	// 残DL可能時間
	var alive = limit - sum;

	var alive_abs = Math.abs(alive);

	var h = Math.floor(alive_abs/60/60);
	alive_abs = alive_abs - (h*60*60);
	var m = Math.floor(alive_abs/60);
	var s = alive_abs - (m*60);

	var alive_abs_text = '';
	if ( h > 0 ) {
		alive_abs_text = alive_abs_text + h + ' 時間 ';
	}
	if ( m > 0 ) {
		alive_abs_text = alive_abs_text + m + ' 分 ';
	}
	alive_abs_text = alive_abs_text + s + ' 秒 ';

	if ( alive < 0 ) {
		var limit_hour = limit/60/60;
		alert("ダウンロード可能な通話時間(容量)を超えています。\n最大ダウンロード可能時間は合計で" + limit_hour + "時間です。\n" + alive_abs_text + "ぶんの選択を解除してください。");
		alive_abs_text = "<font color='red'>- " + alive_abs_text + "<b>";
		// 画面にダウンロード可能時間を表示
		document.getElementById("duration_sum").innerHTML = "<b>" + alive_abs_text + "</b>";
		return false;
	}

	// 画面にダウンロード可能時間を表示
	document.getElementById("duration_sum").innerHTML = "<b>" + alive_abs_text + "</b>";
	return true;

}
</script>


 <!-- end of page level js-->
