@extends('layouts.app')

@section('ip')
    @foreach ($ips as $ip)
      {{$ip}}
    @endforeach
@endsection

@section('content')
  @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif
  @if (session('error'))
      <div class="alert alert-danger">
          {{ session('error') }}
      </div>
  @endif
  <div id="mainContent">
        <form id="list" name="list">
          {{ csrf_field() }}
            <div class="panel panel-primary">
              <div class="panel-heading">{!! trans('app.play') !!}</div>
                <div class="panel-body">
                   @if ($config["player_value"] == 1) <!-- 1 => Windows Media Player -->
                      <object
                            id="wmp2"
                            width="98%" height="45"
                            classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
                            codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715"
                            standby="Loading MicrosoftR WindowsR Media Player components..."
                            type="application/x-oleobject">

                            <!-- <param name="FileName" value="./media/meta_sample.asx" /> --><!-- Filename属性は絶対・相対パスどちらでもOK -->
                            <param name="ShowControls" value="true" /><!-- コントロール表示 -->
                            <param name="AutoStart" value="true" /><!-- 自動再生しない -->

                            <!-- コントロール表示（showcontrols="1"）、自動再生しない（autostart="0"） -->
                            <embed
                              name="wmp2"
                              type="application/x-mplayer2"
                              pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"
                              width="98%" height="45"
                              showcontrols="1"><!-- src属性は絶対パス指定 -->
                            </embed>
                      </object>
                    @else
                      <div id="player_container" style="margin:0 auto;"></div>
			                <p id="message"></p>
                      ​<script src="{!! asset('js/jwplayer.js') !!}" ></script>
                      <script type="text/javascript">
                  				  jwplayer("player_container").setup({
                  					flashplayer: "{!! asset('player/player.swf') !!}",
                  					controlbar: "bottom",
                  					//width: 800,
                  					width:"99%",height: 24,
                  					stretching: 'exactfit'
                  					});
              			</script>
                  @endif
              </div>
            </div>

            <div class="panel panel-primary">
              <div class="panel-heading">{!! trans('app.speechrecognitiondata') !!}</div>
              <div class="panel-body">
                <div id="recognition">
                  <div name="youwire_recognition" id="youwire_recognition" style="text-align: left;"></div>
                </div>
              </div>
            </div>

            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="btn-group">
                    @if ($data['authority_download'] == 1 || $data['authority_protect'] == 1 || $data['authority_delete'] == 1)
                        @if ($config['bulk_dl_limit'] >0 && $data['authority_bulk_download'] == 1)
                            <button type="button" id="button_bulk_dl" name="button_bulk_dl" class="btn btn-primary">{!! trans('app.bulkdl') !!} <span class="glyphicon glyphicon-download-alt"></span></button>
                        @endif
                        @if ($data['authority_download'] == 1)
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <input type="checkbox" class="btn btn-primary" name="csv_out_all" id="csv_out_all" value="1"><span>{!! trans('app.allcheck') !!}</span>
                                </div>
                              </div>
                            <button type="button" id="button_csv_out" name="button_csv_out" class="btn btn-primary">{!! trans('app.csvall') !!} <span class="glyphicon glyphicon-floppy-save"></span></button>
                        @endif
                        @if ($data['authority_protect'] == 1)
                            <button type="button" id="button_lock" name="button_lock" class="btn btn-primary">{!! trans('app.lock') !!} <span class="glyphicon glyphicon-lock"></span></button>
                            <button type="button" id="button_unlock" name="button_unlock" class="btn btn-primary">{!! trans('app.unlock') !!} <span class="glyphicon glyphicon-remove"></span></button>
                        @endif
                        @if ($data['authority_delete'] == 1)
                            <button type="button" id="button_del" name="button_del" class="btn btn-primary">{!! trans('app.delete') !!} <span class="glyphicon glyphicon-trash"></span></button>
                        @endif
                    @endif
                </div>
              </div>
              <div class="panel-body">
                <table class="table" id="dataTable">
                    <thead>
                        <tr>
                          <th scope="col"><input type="checkbox" name="ckeck_ctrl" id="check_ctrl"/></th>
                          <th scope="col">ID</th>
                          <th scope="col">{!! trans('app.play') !!}</th>
                          <th scope="col">{!! trans('app.callstart') !!}</th>
                          <th scope="col">{!! trans('app.duration') !!}</th>
                          <th scope="col">{!! trans('app.group') !!}</th>
                          <th scope="col">{!! trans('app.localparty') !!}</th>
                          <th scope="col">{!! trans('app.direction') !!}</th>
                          <th scope="col">{!! trans('app.remoteparty') !!}</th>
                          @if ($config['customer'] == 1)
                              <th scope="col">{!! trans('app.rivision') !!}</th>
                          @endif
                          @if ($config['app_type'] == 1 || $config['app_type'] == 3)
                              <th scope="col">{!! trans('app.place') !!}</th>
                          @endif
                          <th scope="col">{!! trans('app.lastdldate') !!}</th>
                          @if ($data['tag_use'] == 1 && $data['authority_add_tag'] == 1)
                              <th scope="col">{{$data['qtag_name']}}</th>
                          @endif
                          @if ($data['authority_download'] == 1 && $data['authority_add_tag'] == 1)
                              <th scope="col">{!! trans('app.operation') !!}</th>
                          @endif
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($res as $items)
                        <tr id="recording_id_{{$items->id}}">
                          <td>
                              <input type="checkbox" name="chkselect[]" id="chkselect" value="{{ $items->id }}" onclick="sumDlFileDuration({{ $config['bulk_dl_limit'] }});" />
                              <input type="hidden" name="duration_sec[]" id="duration_sec" value="{{ $items->duration_sec }}" />
                          </td>
                          <td>
                              <a href="{{ $items->id }}" name="link_view_detail_{{$items->id}}" value="{{$items->id}}" id="link_view_detail_{{$items->id}}" class="link_view_detail">{{$items->id}}</a>
                              @if ($items->protect === 1)
                                  <span class="glyphicon glyphicon-remove-circle"></span>
                              @endif
                          </td>
                          <td>
                              <a href="#" style="height:24px;line-height:26px;" name="button_play{{$items->id}}" id="button_play{{$items->id}}"
                                  onclick="@if ($config['player_value'] == 1) wmp_play('{!! asset('recordings') !!}/{{$items->filename_org}}'
                                  @else loadjwplayer('{!! asset('recordings') !!}/{{$items->localparty}}/{{$items->filename}}'@endif,'recording_id_{{$items->id}}');">
                                    <img src="{!! asset('images/button_blue_play_26.png') !!}" class="btn btn-primary"/>
                              </a>
                          </td>
                          <td>
                              {{$items->timestamp_date}}</br>{{$items->timestamp_time}}
                          </td>
                          <td>
                              {{$items->duration}}
                          </td>
                          <td>
                              {{$items->group_name}}
                          </td>
                          <td id="localparty_{{$items->id}}">
                              {{$items->localparty}}
                          </td>
                          <td>
                              @if ($items->direction === 0)
                                <img src="{!! asset('images/arrow_left_26.png') !!}" />
                              @elseif ($items->direction === 1)
                                <img src="{!! asset('images/arrow_right_26.png') !!}" />
                              @elseif ($items->direction === 2)
                                <img src="{!! asset('images/voice_recorder_26.png') !!}" />
                              @endif
                          </td>
                          <td id="remoteparty_{{$items->id}}">
                              {{$items->remoteparty}}
                          </td>
                          @if ($config['customer'] == 1)
                              <td>{{$items->revision}}</td>
                          @endif

                          @if ($config['app_type'] == 1 || $config['app_type'] == 3)
                              @if ($items->location)
                                <td>
                                    <a href="http://maps.google.com/maps?q={{$items->latitude}},{{$items->longitude}}" target="_blank">
                                        <img class="status" src="{!! asset('images/map_16.png') !!}" title="{{$items->location}}" />
                                    </a>
                                </td>
                              @else
                                <td>-</td>
                              @endif
                          @endif

                          <td>
                              {{$items->download_datetime_date}}</br>{{$items->download_datetime_time}}
                          </td>

                          @if ($data['tag_use'] && $data['authority_add_tag'] == 1)
                            <td>
                                @if ($items->qtag_exist)
                                    <img src="{!! asset('images/voice_record_tag2_16.png') !!}" title="{{$data['qtag_name']}}" />
                                @endif
                            </td>
                          @endif

                          @if ($data['authority_download'] == 1 || $data['authority_add_tag'] == 1)
                              <td>
{{--
                                  <button type="button" id="button_dl{{$items['id']}}" name="button_dl{{$items['id']}}" value="{{$items['id']}}" class="btn btn-primary">DL<span class="glyphicon glyphicon-download-alt"></span></button>
                                  @if ($items['protect'] === 0)
                                      <button type="button" id="button_del{{$items['id']}}" name="button_del{{$items['id']}}" value="{{$items['id']}}" class="btn btn-primary">DEL<span class="glyphicon glyphicon-erase"></span></button>
                                  @endif

                                  <a href="#cancel" title="operation"> <img src="{!! asset('images/voice_record_download_16.png') !!}" /> Operation </a>
                                  <ul>
                                    <li>
                                      <a href="{{$items['id']}}" style="height:24px;line-height:26px;" name="button_dl{{$items['id']}}" id="button_dl{{$items['id']}}" value="{{$items['id']}}" >
                                        <img src="{!! asset('images/voice_record_download_16.png') !!}" title="Download" />Download
                                      </a>
                                    </li>
                                    @if ($items['protect'] === 0)
                                        <li>
                                            <a href="{{$items['id']}}" style="height:24px;line-height:26px;" name="button_del{{$items['id']}}" id="button_del{{$items['id']}}" value="{{$items['id']}}">
                                              <img src="{!! asset('images/voice_record_delete_16.png') !!}" title="Delete" />Delete
                                            </a>
                                        </li>
                                    @endif
                                  </ul>
--}}

                                      @if ($data['authority_download'] == 1)
                                              <a href="{{$items->id}}" name="button_dl{{$items->id}}" id="button_dl{{$items->id}}" value="{{$items->id}}" class="btn btn-primary btn_dl">
                                                <span class="glyphicon glyphicon-music"></span> DL
                                              </a>
                                      @endif
                                      @if ($data['tag_use'] && $data['authority_add_tag'] == 1)
                                          <li>
                                              <a href="{{$items->id}}" style="height:24px;line-height:26px;" name="button_qtag{{$items->id}}" id="button_qtag{{$items->id}}" value="{{$items->id}}">
                                                <img src="{!! asset('images/voice_record_tag_16.png') !!}" title="QuickTag">{{$data['qtag_name']}}
                                                </a>
                                          </li>
                                      @endif
                              </td>
                          @endif
                        </tr>
                        <tr id="recording_id{{$items->id}}_2">
                            <td colspan="3"></td>
                            <td colspan="7" style="border-top:1px dashed blue;">
                                <p id="voicetext_{{$items->id}}">{{$items->voicetext2}}</p>
                                <input type="hidden" id="voicetext_{{$items->id}}_html" value="{{$items->voicetext1}}" />
                            </td>
                            <td>
                                <a href="{{$items->id}}" name="button_dltxt{{$items->id}}" id="button_dltxt{{$items->id}}" value="{{$items->id}}" class="btn btn-primary">
                                  <span class="glyphicon glyphicon-pencil"></span> DL
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                  {{ $res->appends(request()->input())->links() }}
              </div>
            </div>

          <input type="hidden" name="oldPlayingID1" id="oldPlayingID1" value="0"/>
          <input type="hidden" name="id" id="id" value=""/>
          <input type="hidden" name="search_sql" id="search_sql" value="{{$data['search_sql']}}"/>
          @if(isset($data['selected_page']))
            <input type="hidden" name="selected_page" id="selected_page" value="{{$data['selected_page']}}"/>
          @endif
          <input type="hidden" name="protect" id="protect" value="0"/>
          <input type="hidden" name="recid" id="recid" value="0"/>
          <input type="hidden" name="current_page" id="current_page" value="0"/>
        </form>
  </div>
@endsection

@section('status_panel')
  @if(!empty($res->total()))
    @if ($res->total() >= 0)
      <div class="panel panel-primary">
        <div class="panel-heading">{!! trans('app.searchresult') !!}</div>
        <div class="panel-body">
            @if ($res->total() > 0)
                <!-- display the number of items in the page.. Here we can use laravel "pagination" module -->
                {{$res->firstItem()}} - {{$res->lastItem()}} of {{$res->total()}}
            @else
              0 件
            @endif
        </div>
      </div>
    @endif
  @endif
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
function submitStop(e){
    if(!e) var e = window.event;

    if(e.keyCode == 13)
        return false;
}

$(function(){
	/*$('#button_lock').click(function() {
		return false;
	});
	$('#btn_search').click(function() {
		$('#serch_form').attr('action', '/search');
    $('#serch_form').attr('method', 'GET');
		$('#serch_form').submit();
		return false;
	});
	$('#btn_clear').click(function() {
		$(this).closest("form").find("textarea,:text,select").val("").end().find(":checked").not('[name="and_or"]').prop("checked",false);
		$('#selected_tag').val("");
		$('#selected_group').val("");
		$('#startduration').val("00:00:00");
		$('#endduration').val("00:00:00");
		return false;
	});*/
	$('#button_lock').click(function() {
		//var url_param = '<{youwire_pagenavi_get_params pagenavi=$pageNavi}>';
    var n = $(this).closest("form").find("input:checkbox[name^='chkselect']:checked").length;
    if(n>0)
    {
		  $('#list').attr('action', '/control_update');
      $('#protect').attr('value', '1');
		  $('#list').submit();
		  return false;
    }
    else {
      alert("No record is selected");
    }
	});
	$('#button_unlock').click(function() {
		//var url_param = '<{youwire_pagenavi_get_params pagenavi=$pageNavi}>';
    var n = $(this).closest("form").find("input:checkbox[name^='chkselect']:checked").length;
    if(n>0)
    {
  		$('#list').attr('action', '/control_update');
      $('#protect').attr('value', '0');
  		$('#list').submit();
  		return false;
    }
    else {
      alert("No record is selected");
    }
	});
	$('.link_view_detail').click(function() {
		var $id = $(this).attr("href");
    $('#current_page').attr('value',{{$res->currentPage()}});
    $('#recid').attr('value',$id);
		//var url_param = '<{youwire_pagenavi_get_params pagenavi=$pageNavi}>';
		$('#list').attr('action', '/controlviewdetail');
		$('#list').submit();
		return false;
	});
	/*
	$('#btn_detail_download_text').click(function() {
		$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_download_text');
		$('#list').submit();
		return false;
	});
	*/
	$('.btn_dl').click(function() {
		$('#id').val($(this).attr("href"));
		$('#list').attr('action', '/download');
		$('#list').submit();
		return false;
	});
	/*$('.btn_qtag').click(function() {
		$('#id').val($(this).attr("href"));
		$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_quicktag');
		$('#list').submit();
		return false;
	});
	$('.btn_dl-text').click(function() {
		$id = $(this).attr("href");
		$('#id').val($id);
		var $voiceHtml = $('#voicetext_'+$id+'_html').val()
		$voiceHtml = $voiceHtml.split(' ');
		var $html = '';
		for (var i = 0; i < $voiceHtml.length; i++) {
			if ( $voiceHtml[i].indexOf(':0') != -1 ) {
				var $str = $voiceHtml[i].replace( /:0/g , '' );
				var $telnum = $('#localparty_'+$id).text();
				$html += $telnum + '：' + $str + "\r\n";
			} else if ( $voiceHtml[i].indexOf(':1') != -1 ) {
				var $str = $voiceHtml[i].replace( /:1/g , '' );
				var $telnum = $('#remoteparty_'+$id).text();
				$html += $telnum + '：' + $str + "\r\n";
			}
		}
		$('#voicetext').val($html);
		// $('#voicetext').val($('#voicetext_'+$id+'_html').val());
		$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_download_text');
		$('#list').submit();
		return false;
	});*/
	$('#button_del').click(function() {
		var n = $(this).closest("form").find("input:checked").length;
		if( n == 0 ){
			alert('{!! trans('app.norecordslected') !!}');
			return false;
		}
		//if(confirm("<{$smarty.const._MD_YOUWIRE_CONF_DEL_MSG1}>"+$(this).attr("href")+"<{$smarty.const._MD_YOUWIRE_CONF_DEL_MSG2}>")){
		if( confirm('{!! trans('app.deleteconfirm') !!}') ){
			//$('#id').val($(this).attr("href"));
			$('#list').attr('action', '/controldelete');
			$('#list').submit();
		}
		return false;
	});
	/*$('#button_bulk_dl').click(function() {

		var download_file_size = 0;

		if(document.list.chkselect){
			var chk_count = 0;
			for (var i = 0; i < document.list.chkselect.length; i++) {
				if (document.list.chkselect[i].checked) {
					chk_count++;
				}
			}
			if ( chk_count == 0 ) {
				alert("１件も選択されていません。\n一括ダウンロード対象をチェックしてください");
				return false;
			}
			if (!sumDlFileDuration('')) {
				return false;
			}
		} else {
			alert('<{$smarty.const._MD_YOUWIRE_BULK_DOWNLOAD_REQUIRE_CHECKED_MSG}>');
			return false;
		}

		var pass = window.prompt('<{$smarty.const._MD_YOUWIRE_BULK_DOUNLOAD_CONFIRM_MSG}>', "");
		if ( pass != null ) {
			$('#id').val($(this).attr("href"));
			$('#list').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/control_bulk_download?pass='+pass);
			$('#list').submit();
		}

		return false;
	}); */
	$('#button_csv_out').click(function() {

		var res = false;
		var out_all_chk = $("#csv_out_all:checked").val();

		if( out_all_chk == 1 ){
			if( confirm('Do you want to download CSV of all search results?') ){
				res = true;
			}
		} else {
			var n = $(this).closest("form").find("input:checked").length;
			if( n == 0 ){
				alert('No record is selected');
				return false;
			}
			if( confirm('Do you wans download CSV of selected record') ){
				res = true;
			}
		}
		if( res ){
			//$('#id').val($(this).attr("href"));
			$('#list').attr('action', '/csvoutput');
			$('#list').submit();
		}
		return false;
	});
	$('#check_ctrl').click(function() {
    if( $(this).is(':checked')){
			$(this).closest("form").find("input:checkbox[name^='chkselect']").prop('checked', true);//.attr("checked",true);
		}else{
			$(this).closest("form").find("input:checkbox[name^='chkselect']").prop('checked', false);//.attr("checked",false);
		}
		sumDlFileDuration('{{$config['bulk_dl_limit']}}');
	});
	/*$('#btn_search_complaint_time').click(function() {
		$('#serch_form').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/search_complaint_time');
		$('#serch_form').submit();
		return false;
	});
	$('#btn_search_complaint_count').click(function() {
		$('#serch_form').attr('action', '<{$xoops_url}>/modules/<{$xoops_dirname}>/default/search_complaint_count');
		$('#serch_form').submit();
		return false;
	});*/
});



@if ($config['player_value'] == 1)
    function wmp_play(mediapath,reco_id,id){
        highlightRecord(reco_id);
        //alert(mediapath);
        document.wmp2.filename = mediapath;
        document.wmp2.play();
              if ( id ) {
                      var voiceText = document.getElementById('voicetext_'+id).innerHTML;
                      var dispArea = document.getElementById('youwire_recognition');
                      dispArea.value = voiceText;
              }
    }
@else
    function loadjwplayer(mediapath,reco_id,id){
        highlightRecord(reco_id);
        //alert(mediapath);
        jwplayer().load({file: mediapath});
        jwplayer().play();
        if ( id ) {
        var voiceText1 = document.getElementById('voicetext_'+id).innerHTML;
        var voiceText2 = document.getElementById('voicetext_'+id+'_html').value;
        var dispArea = document.getElementById('youwire_recognition');
        voiceHtml = voiceText2.split(' ');
        var html = '';
        for (var i = 0; i < voiceHtml.length; i++) {
          if ( voiceHtml[i].indexOf(':0') != -1 ) {
            var str = voiceHtml[i].replace( /:0/g , '' );
            var telnum = document.getElementById('localparty_'+id).innerHTML;
            html += '<div class="dir_0"><span class="telnum">' + telnum + '：</span><span class="text">' + str + '</span></div>';
          } else if ( voiceHtml[i].indexOf(':1') != -1 ) {
            var str = voiceHtml[i].replace( /:1/g , '' );
            var telnum = document.getElementById('remoteparty_'+id).innerHTML;
            html += '<div class="dir_1"><span class="telnum">' + telnum + '：</span><span class="text">' + str + '</span></div>';
          }
        }
        dispArea.value = voiceText1;
        dispArea.innerHTML = html;
      }
    }

@endif


function highlightRecord(id){

  var SesVar = '<%= Session("show_details").ToString() %>';
  if (SesVar != 1) {
      //document.getElementById(oldID).classList.remove("highlight");
      $("#"+oldID).removeClass("highlight");
      //alert(oldID);
      var oldID = document.list.oldPlayingID1.value;

      document.list.oldPlayingID1.value = id;
      $("#"+id).addClass("highlight");
          //document.getElementById(id).add("highlight");
  }
}

function sumDlFileDuration(limit){

	// 一括ダウンロード権限の有無
	var authority_bulk_download = '{{$data['authority_bulk_download']}}';

	// 一括ダウンロード許可容量が0、もしくは、権限がない場合は以下の処理を行わない
	if ( limit <= 0 || authority_bulk_download == 0 ) {
		return false;
	}

	// チェックされている通話時間を加算する
	var sum = 0;

	if ( document.list.chkselect ) {
		var len = document.list.chkselect.length;
		for(var i=0;i<len;i++){
			if ( document.list.chkselect[i].checked ) {
				sum = sum + parseInt(document.list.duration_sec[i].value);
			}
		}
	}

	// 残DL可能時間
	var alive = limit - sum;

	var alive_abs = Math.abs(alive);

	var h = Math.floor(alive_abs/60/60);
	alive_abs = alive_abs - (h*60*60);
	var m = Math.floor(alive_abs/60);
	var s = alive_abs - (m*60);

	var alive_abs_text = '';
	if ( h > 0 ) {
		alive_abs_text = alive_abs_text + h + ' 時間 ';
	}
	if ( m > 0 ) {
		alive_abs_text = alive_abs_text + m + ' 分 ';
	}
	alive_abs_text = alive_abs_text + s + ' 秒 ';

	if ( alive < 0 ) {
		var limit_hour = limit/60/60;
		alert("ダウンロード可能な通話時間(容量)を超えています。\n最大ダウンロード可能時間は合計で" + limit_hour + "時間です。\n" + alive_abs_text + "ぶんの選択を解除してください。");
		alive_abs_text = "<font color='red'>- " + alive_abs_text + "<b>";
		// 画面にダウンロード可能時間を表示
		document.getElementById("duration_sum").innerHTML = "<b>" + alive_abs_text + "</b>";
		return false;
	}

	// 画面にダウンロード可能時間を表示
	document.getElementById("duration_sum").innerHTML = "<b>" + alive_abs_text + "</b>";
	return true;

}
</script>
