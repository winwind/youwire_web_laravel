<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Main routes (login / welcome)
Route::get('/', 'PagesController@index')->name('index');;
Route::get('/welcome', 'PagesController@welcome')->name('welcome');

//Multilanguage translation route
Route::post('/language', array(
  'Middleware' => 'LanguageSwitcher',
  'uses' => 'LanguageController@index'
));

//Authentication routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


//DefaultController Routes
Route::get('/search', 'DefaultController@search')->name('search');
Route::get('/download', 'DefaultController@download_control')->name('download');
Route::get('/csvoutput', 'DefaultController@csvoutput_control')->name('csvoutput');
Route::get('/controldelete', 'DefaultController@control_delete')->name('controldelete');
Route::get('/control_update', 'DefaultController@control_update');


//GroupController Routes　
Route::get('/group', 'GroupController@index');
Route::get('/group/create', 'GroupController@create');
Route::get('/group/destroy/{id}', 'GroupController@destroy');
Route::get('/group/{id}/edit', 'GroupController@edit');
Route::get('/group/{id}', 'GroupController@update');

//SecuritygroupController Routes
Route::get('/security_group', 'SecuritygroupController@index');
Route::get('/security_group/create', 'SecuritygroupController@create');
Route::get('/security_group/destroy/{id}', 'SecuritygroupController@destroy');
Route::get('/security_group/{id}/edit', 'SecuritygroupController@edit');
Route::get('/security_group/{id}', 'SecuritygroupController@update'); 

//UsersearchController Routes
Route::get('/user_search', 'UsersearchController@index');
Route::get('/user_search/create', 'UsersearchController@create');
Route::get('/user_search/bygroupid/{id}', 'UsersearchController@bygroupid');
Route::get('/user_search/destroy/{id}', 'UsersearchController@destroy');
Route::get('/user_search/{id}/edit', 'UsersearchController@edit');
Route::get('/user_search/{id}', 'UsersearchController@update'); 

//UserrecordController Routes
Route::get('/user_record', 'UserrecordController@index');
Route::get('/user_record/create', 'UserrecordController@create');
Route::get('/user_record/bygroupid/{id}', 'UserrecordController@bygroupid');
Route::get('/user_record/destroy/{id}', 'UserrecordController@destroy');
Route::get('/user_record/{id}/edit', 'UserrecordController@edit');
Route::get('/user_record/{id}', 'UserrecordController@update'); 

//PhonenumberController Routes
Route::get('/Phonenumber/create', 'PhonenumberController@create');
Route::get('/Phonenumber/edituser', 'PhonenumberController@updateuser'); 
Route::get('/userrecord/Phonenumber/{id}', 'PhonenumberController@index');
Route::get('/Phonenumber/destroy/{id}', 'PhonenumberController@destroy');
Route::get('/Phonenumber/{id}/edit', 'PhonenumberController@edit');
Route::get('/Phonenumber/{id}', 'PhonenumberController@update'); 

//usersettingController Routes
Route::get('/user_setting', 'UsersettingController@index');
Route::get('/user_setting/create', 'UsersettingController@create');
Route::get('/user_setting/destroy/{id}', 'UsersettingController@destroy');
Route::get('/user_setting/{id}/edit', 'UsersettingController@edit');
Route::get('/user_setting/{id}', 'UsersettingController@update'); 


//DetailController Routes
Route::get('/controlviewdetail', 'DetailController@control_view_detail')->name('controlviewdetail');
Route::get('/control_detail_update', 'DetailController@control_detail_update');
Route::get('/control_detail_delete', 'DetailController@control_detail_delete');
Route::get('/control_detail_download', 'DetailController@control_detail_download');
Route::get('/control_view_tag_edit', 'DetailController@control_view_tag_edit');
Route::get('/control_tag_delete', 'DetailController@control_tag_delete');
Route::get('/control_tag_edit', 'DetailController@control_tag_edit');

//TagCategoryController Routes
Route::get('/tagCategoryView', 'TagCategoryController@tagCategoryView');
Route::get('/control_add', 'TagCategoryController@control_add');
Route::get('/control_update', 'TagCategoryController@control_update');
Route::get('/control_delete', 'TagCategoryController@control_delete');

//LogController Routes
Route::get('/operationlog', 'OperationLogController@index');
Route::get('/logsearch', 'OperationLogController@search');


//alertmail routes
Route::get('/alertmailView', 'AlertmailController@alertView');
Route::get('/control_alert_update', 'AlertmailController@control_update');


//test route
Route::get('/test', 'util\HelperController@testSelf');

//admin route
Route::get('/admin', 'Admin\AdminController@index');
Route::post('/updatePref', 'Admin\AdminController@control_update');
